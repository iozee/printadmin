package com.iozee;

import com.google.common.collect.Lists;
import lombok.extern.java.Log;
import lombok.val;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import com.iozee.printadmin.enums.OrderStatusEnum;
import com.iozee.printadmin.model.Client;
import com.iozee.printadmin.model.Order;
import com.iozee.printadmin.model.Role;
import com.iozee.printadmin.service.ClientService;
import com.iozee.printadmin.service.OrderCriteria;
import com.iozee.printadmin.service.OrderService;
import com.iozee.printadmin.service.ServiceService;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/config/spring/applicationContext.xml", "classpath:/config/spring/security.xml"})
@WebAppConfiguration
@Transactional(readOnly = true)
@Log
public class TestTest {

    @Autowired
    @Lazy
    private AuthenticationManager authenticationManager;

    @Autowired
    @Lazy
    private OrderService orderService;

    @Autowired
    @Lazy
    private ClientService clientService;

    @Autowired
    @Lazy
    private ServiceService serviceService;

    @Autowired
    @Lazy
    private SessionFactory sessionFactory;

    @Before
    public void setUp() {
        login("igor", "qqqq");
        log.info("------- Test setup finished -------");
    }

    @After
    public void tearDown() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void getAllOrdersQDSL() {
        orderService.getAllByCriteria(new OrderCriteria());
        log.info("cache hit count " + sessionFactory.getStatistics().getSecondLevelCacheHitCount());
    }

    @Test
    public void getAllOrdersSession() {
        sessionFactory.getCurrentSession().createCriteria(Order.class).list();
    }

    @Test
    public void testOrderCriteria() {
        List<Order> filtered = orderService.getAllByCriteria(OrderCriteria.builder().status(OrderStatusEnum.IN_WORK).orderId("11").build());
        List<Order> unfiltered = orderService.getAllByCriteria(new OrderCriteria());
        assertTrue(filtered.size() < unfiltered.size());
    }

    @Test
    public void testClient() {
        Client client = clientService.getAll().iterator().next();
        orderService.getOrdersByClient(client);
    }

    @Test
    public void test1LevelCache() {
        Session session = sessionFactory.getCurrentSession();
        Order order1 = session.get(Order.class, 46);
        Order order2 = session.get(Order.class, 46);
        assertTrue(order1 == order2);
    }

    @Test
    public void test2LevelCache() {
        log.info("Testing 2 level cache...");
        sessionFactory.getStatistics().clear();
        Session session = sessionFactory.getCurrentSession();
        session.getTransaction().commit();
        session.close();
        int timesToRead = 3;
        getRole(timesToRead);
        assertEquals(sessionFactory.getStatistics().getSecondLevelCachePutCount(), 1);
        assertEquals(sessionFactory.getStatistics().getSecondLevelCacheHitCount(), 2);
        assertEquals(sessionFactory.getStatistics().getSecondLevelCacheMissCount(), 1);
    }

    @Test
    public void testQueryCacheSpringData() {
        serviceService.getAllServices();
        serviceService.getAllServices();
        serviceService.getAllServices();

        serviceService.getAllMaterials();
        serviceService.getAllMaterials();
        serviceService.getAllMaterials();

        assertEquals(sessionFactory.getStatistics().getQueryCacheHitCount(), 4);
    }

    @Test
    public void testOrdersByServices() {
        val services = Lists.newArrayList(serviceService.getById(7));
        OrderCriteria criteria = OrderCriteria.builder().services(services).build();
        assertEquals(orderService.getCountByCriteria(criteria), 5);
    }

    private void getRole(int times) {
        for (int i = 0; i < times; i++) {
            Session session = sessionFactory.openSession();
            Transaction tr = session.beginTransaction();
            session.get(Role.class, 1); // should hit cache
            tr.commit();
            session.close();
        }
    }

    private void login(String name, String password) {
        Authentication auth = new UsernamePasswordAuthenticationToken(name, password);
        SecurityContextHolder.getContext().setAuthentication(authenticationManager.authenticate(auth));
    }

}
