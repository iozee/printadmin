package com.iozee;

import java.util.Optional;

public class TestJava8 {

    public static void main(String[] args) {
        Runtime.getRuntime().addShutdownHook(new MyShutdownHook());
        Optional<String> s = Optional.of("input");
        s = Optional.empty();
        System.out.println(s.map(TestJava8::getOutput));
        System.out.println(s.flatMap(TestJava8::getOutputOpt));
    }

    static String getOutput(String input) {
        return input == null ? null : "output for " + input;
    }

    static Optional<String> getOutputOpt(String input) {
        return input == null ? Optional.empty() : Optional.of("output for " + input);
    }

    static class MyShutdownHook extends Thread {
        @Override
        public void run() {
            System.out.println("MyShutdownHook executed");
        }
    }

}
