package com.iozee.printadmin.utils;

import org.hibernate.Criteria;

import java.util.Collections;
import java.util.List;

/**
 * @author izerenev
 * Date: 28.05.2014
 */
public class CriteriaUtils {

    @SuppressWarnings("unchecked")
    public static <T> T getAndCast(Criteria criteria) {
        Object result = criteria.uniqueResult();
        return result == null ? null : (T) result;
    }

    @SuppressWarnings("unchecked")
    public static <T> List<T> listAndCast(Criteria criteria) {
        List list = criteria.list();
        return list == null || list.isEmpty() ? Collections.emptyList() : list;
    }

}
