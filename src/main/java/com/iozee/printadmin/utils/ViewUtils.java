package com.iozee.printadmin.utils;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * @author izerenev
 * Date: 28.05.2014
 */
public class ViewUtils {
    public static void showMessage(String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(String.valueOf(message)));
    }

    public static void showError(String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, String.valueOf(message), null));
    }

    public static void changesSavedMessage() {
        showMessage("Изменения сохранены");
        // todo RequestContext.getCurrentInstance().execute("formEdited = false;");
    }

    public static void showError() {
        showError("Произошла ошибка");
    }

    public static void executeJavaScript(String script) {
        // todo RequestContext.getCurrentInstance().execute(script);
    }
}
