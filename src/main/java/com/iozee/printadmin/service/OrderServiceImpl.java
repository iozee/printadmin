package com.iozee.printadmin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.iozee.printadmin.model.CommonEntity;
import com.iozee.printadmin.dao.LaminationDao;
import com.iozee.printadmin.dao.MaterialDao;
import com.iozee.printadmin.dao.OrderDao;
import com.iozee.printadmin.dao.OrderStatusDao;
import com.iozee.printadmin.enums.OrderStatusEnum;
import com.iozee.printadmin.model.*;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

@org.springframework.stereotype.Service
@Transactional
public class OrderServiceImpl implements OrderService, Serializable {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private OrderStatusDao orderStatusDao;

    @Autowired
    private MaterialDao materialDao;

    @Autowired
    private OrderItemService orderItemService;

    @Autowired
    private LaminationDao laminationDao;

    @Autowired
    private ClientService clientService;

    @Override
    public Order create(Order order, List<OrderItem> orderItemList) {
        order.setDateCreated(new Date());
        order.setTimeCreated(new Date());
        order.setOrderId(getNewOrderId());
        order = orderDao.save(order);
        order.setOrderItems(new HashSet<>(orderItemList));
        orderItemList.forEach(orderItemService::createOrderItem);
        return order;
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<Lamination> getAllLaminations() {
        return laminationDao.findAll();
    }

    @Override
    public void update(Order order) {
        orderDao.save(order);
    }

    @Override
    public void delete(Order order) {
        order.getOrderItems().forEach(orderItemService::deleteWithoutOrderUpdate);
        orderDao.delete(order);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> getOrdersByClient(Client client) {
        return orderDao.findByClient(client);
    }

    @Override
    @Transactional(readOnly = true)
    public OrderStatus getStatusById(int id) {
        return orderStatusDao.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    @Transactional(readOnly = true)
    public OrderStatus getStatusByCode(String code) {
        return orderStatusDao.findByCode(code);
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<OrderStatus> getAllStatuses() {
        return orderStatusDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Order getFirstOrder() {
        throw new UnsupportedOperationException();
    }

    @Override
    @Transactional(readOnly = true)
    public int getOrdersCountByStatus(OrderStatusEnum status) {
        return orderDao.getCountByCriteria(OrderCriteria.builder().status(status).build());
    }

    @Override
    @Transactional(readOnly = true)
    public synchronized String getNewOrderId() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyMM");
        Order firstOrder = getFirstOrder();
        return dateFormat.format(new Date()) + ((firstOrder == null ? 0 : firstOrder.getId()) + 1);
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<Material> getAllMaterials() {
        return materialDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public <Entity extends CommonEntity> Entity getEntityById(int id, Class<Entity> cls) {
        if (cls == Material.class) {
            return (Entity) materialDao.findById(id).orElseThrow(IllegalArgumentException::new);
        }
        if (cls == Lamination.class) {
            return (Entity) laminationDao.findById(id).orElseThrow(IllegalArgumentException::new);
        }
        throw new IllegalArgumentException();
    }

    @Override
    @Transactional(readOnly = true)
    public Order getById(int id) {
        return orderDao.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> getAllByCriteria(OrderCriteria criteria) {
        return orderDao.getAllByCriteria(criteria);
    }

    @Override
    @Transactional(readOnly = true)
    public int getCountByCriteria(OrderCriteria criteria) {
        return orderDao.getCountByCriteria(criteria);
    }

}
