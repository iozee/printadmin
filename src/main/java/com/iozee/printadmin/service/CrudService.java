package com.iozee.printadmin.service;

import com.iozee.printadmin.model.CommonEntity;

import java.util.List;

/**
 * @author izerenev
 * Date: 13.05.2014
 */
public interface CrudService<Entity extends CommonEntity> {

    Entity getById(int id);

    List<Entity> getAll();

    Entity create(Entity entity);

    void update(Entity entity);

    void delete(Entity entity);

}
