package com.iozee.printadmin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.iozee.printadmin.dao.MaterialDao;
import com.iozee.printadmin.dao.ServiceDao;
import com.iozee.printadmin.model.Material;
import com.iozee.printadmin.model.Service;

import java.io.Serializable;

@org.springframework.stereotype.Service
@Transactional
public class ServiceServiceImpl implements ServiceService, Serializable {

    @Autowired
    private ServiceDao serviceDao;

    @Autowired
    private MaterialDao materialDao;

    @Override
    @Transactional(readOnly = true)
    public Service getById(int id) {
        return serviceDao.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    public Service create(Service service) {
        return serviceDao.save(service);
    }

    public void update(Service service) {
        serviceDao.save(service);
    }

    public void delete(Service service) {
        serviceDao.delete(service);
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<Material> getAllMaterials() {
        return materialDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<Service> getAllServices() {
        return serviceDao.findAll();
    }
}
