package com.iozee.printadmin.service;

import org.springframework.security.access.prepost.PreAuthorize;
import com.iozee.printadmin.security.SPELSecurityExpressions;
import com.iozee.printadmin.model.Material;
import com.iozee.printadmin.model.Service;

@PreAuthorize(SPELSecurityExpressions.IS_AUTHENTICATED)
public interface ServiceService {

    Iterable<Material> getAllMaterials();

    Iterable<Service> getAllServices();

    Service getById(int id);

}
