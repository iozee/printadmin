package com.iozee.printadmin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.iozee.printadmin.security.RolesEnum;
import com.iozee.printadmin.security.SecurityUtils;
import com.iozee.printadmin.dao.OrderItemDao;
import com.iozee.printadmin.dao.OrderItemStatusDao;
import com.iozee.printadmin.enums.OrderItemStatusEnum;
import com.iozee.printadmin.model.Order;
import com.iozee.printadmin.model.OrderItem;
import com.iozee.printadmin.model.OrderItemStatus;
import com.iozee.printadmin.model.OrderItemStatusLink;
import com.iozee.printadmin.view.beans.TimelineEntry;

import java.io.Serializable;
import java.util.List;

/**
 * @author izerenev
 * Date: 05.06.2014
 */
@Service
@Transactional
public class OrderItemServiceImpl implements OrderItemService, Serializable {

    @Autowired
    private OrderItemDao orderItemDao;

    @Autowired
    private OrderItemStatusDao orderItemStatusDao;

    @Override
    public OrderItem createOrderItem(OrderItem orderItem) {
        orderItem.setStatus(orderItemStatusDao.getStatusByCode(OrderItemStatusEnum.NEW.toString()));
        orderItem = orderItemDao.save(orderItem);
        return orderItem;
    }

    /**
     * Called when the whole Order is deleted
     */
    @Override
    public void deleteWithoutOrderUpdate(OrderItem orderItem) {
        orderItemDao.delete(orderItem);
    }

    @Override
    public void update(OrderItem orderItem) {
        orderItemDao.save(orderItem);
    }

    @Override
    public boolean isStatusChangeAllowed(OrderItemStatusLink statusLink) {
        return SecurityUtils.isSA() || SecurityUtils.hasAnyRole(RolesEnum.toRoleSet(statusLink.getRoles()));
    }

    @Override
    public List<TimelineEntry> getTimelineEntriesByOrderItem(OrderItem orderItem) {
        return orderItemDao.getTimelineEntriesByOrderItem(orderItem);
    }

    @Override
    public List<TimelineEntry> getTimelineEntriesByOrder(Order order) {
        return orderItemDao.getTimelineEntriesByOrder(order);
    }

    @Override
    public void delete(OrderItem orderItem) {
        orderItemDao.delete(orderItem);
    }

    @Override
    @Transactional(readOnly = true)
    public OrderItemStatus getItemStatusById(int id) {
        return orderItemStatusDao.findById(id).orElseThrow(() -> new IllegalArgumentException("Error getting order item status by id: " + id));
    }

    @Override
    public void setStatus(OrderItemStatus status, OrderItem orderItem) {
        orderItem.setStatus(status);
        orderItemDao.save(orderItem);
    }
}
