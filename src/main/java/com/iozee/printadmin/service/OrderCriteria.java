package com.iozee.printadmin.service;

import com.google.common.base.Strings;
import com.iozee.printadmin.dao.MySortOrder;
import com.iozee.printadmin.enums.OrderStatusEnum;
import com.iozee.printadmin.enums.ServiceEnum;
import com.iozee.printadmin.model.Client;
import com.iozee.printadmin.model.Service;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public final class OrderCriteria extends CommonCriteria implements ICriteria {

    private String orderId;

    private Date startDate;

    private Date endDate;

    private BigDecimal costFrom;

    private BigDecimal costTo;

    private Client client;

    private OrderStatusEnum status;

    private List<Service> services;

    @Builder
    private OrderCriteria(int limit, int offset, String sortField, MySortOrder sortOrder, String orderId, Date startDate, Date endDate, BigDecimal costFrom, BigDecimal costTo, Client client, OrderStatusEnum status, List<Service> services) {
        super(limit, offset, sortField, sortOrder);
        this.orderId = orderId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.costFrom = costFrom;
        this.costTo = costTo;
        this.client = client;
        this.status = status;
        this.services = services;
    }

    /**
     * Reset search criteria
     */
    public void clear() {
        orderId = null;
        client = null;
        startDate = null;
        endDate = null;
        costFrom = null;
        costTo = null;
        status = null;
        services = null;
    }

    @Override
    public boolean isEmpty() {
        return Strings.isNullOrEmpty(orderId) &&
                client == null &&
                startDate == null &&
                endDate == null &&
                costFrom == null &&
                costTo == null &&
                status == null &&
                (services == null || services.isEmpty());
    }

    public boolean containsService(ServiceEnum serviceEnum) {
        if (services == null) {
            return false;
        }
        return services.stream().anyMatch(service -> service.getCode().equals(serviceEnum.toString()));
    }

    public void removeService(ServiceEnum serviceEnum) {
        if (services == null) {
            return;
        }
        services.removeIf(service -> service.getCode().equals(serviceEnum.toString()));
    }
}
