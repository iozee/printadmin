package com.iozee.printadmin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.iozee.printadmin.dao.ClientDao;
import com.iozee.printadmin.model.Client;

import java.io.Serializable;
import java.util.List;

@Service
@Transactional
public class ClientServiceImpl implements ClientService, Serializable {

    @Autowired
    private ClientDao dao;

    @Override
    public Client getById(int id) {
        return dao.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<Client> getAll() {
        return dao.findAll();
    }

    @Override
    public List<Client> getAllByCriteria(ClientCriteria criteria) {
        return dao.getAllByCriteria(criteria);
    }

    @Override
    public int getCountByCriteria(ClientCriteria criteria) {
        return dao.getCountByCriteria(criteria);
    }

    @Override
    public Client create(Client client) {
        return dao.save(client);
    }

    @Override
    public void update(Client client) {
        dao.save(client);
    }

    @Override
    public void delete(Client client) {
        dao.delete(client);
    }

}
