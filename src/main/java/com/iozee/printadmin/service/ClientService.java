package com.iozee.printadmin.service;

import org.springframework.security.access.prepost.PreAuthorize;
import com.iozee.printadmin.security.SPELSecurityExpressions;
import com.iozee.printadmin.model.Client;

import java.util.List;

@PreAuthorize(SPELSecurityExpressions.MANAGER)
public interface ClientService {

    Client create(Client entity);

    void update(Client entity);

    void delete(Client entity);

    Client getById(int id);

    Iterable<Client> getAll();

    List<Client> getAllByCriteria(ClientCriteria criteria);

    int getCountByCriteria(ClientCriteria criteria);

}
