package com.iozee.printadmin.service.orders.builders;

import com.iozee.printadmin.model.OrderItem;

/**
 * @author izerenev
 * Date: 09.02.2015
 */
public interface EntityBuilder<T extends OrderItem> {

    T build();

}
