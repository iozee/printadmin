package com.iozee.printadmin.service.orders.builders;

import com.iozee.printadmin.model.OrderPrintWork;
import com.iozee.printadmin.model.orders.OrderSticker;

/**
 * @author izerenev
 * Date: 09.02.2015
 */
public class StickerBuilder implements EntityBuilder<OrderSticker> {
    @Override
    public OrderSticker build() {
        OrderSticker result = new OrderSticker();
        result.setPrint(new OrderPrintWork());
        return result;
    }

}
