package com.iozee.printadmin.service.orders.builders;

import com.iozee.printadmin.model.OrderPrintWork;
import com.iozee.printadmin.model.orders.OrderCalendar;

/**
 * @author izerenev
 * Date: 09.02.2015
 */
public class CalendarBuilder implements EntityBuilder<OrderCalendar> {

    @Override
    public OrderCalendar build() {
        OrderCalendar result = new OrderCalendar();
        result.setCoverFacePrint(new OrderPrintWork());
        result.setCoverBackPrint(new OrderPrintWork());
        result.setCoverBackFacePrint(new OrderPrintWork());
        result.setCoverBackBackPrint(new OrderPrintWork());
        return result;
    }

}
