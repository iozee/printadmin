package com.iozee.printadmin.service.orders.builders;

import com.iozee.printadmin.model.OrderPrintWork;
import com.iozee.printadmin.model.orders.OrderBrochure;

/**
 * @author izerenev
 * Date: 09.02.2015
 */
public class BrochureBuilder implements EntityBuilder<OrderBrochure> {

    @Override
    public OrderBrochure build() {
        OrderBrochure result = new OrderBrochure();
        result.setCoverFacePrint(new OrderPrintWork());
        result.setCoverBackPrint(new OrderPrintWork());
        result.setCoverBackFacePrint(new OrderPrintWork());
        result.setCoverBackBackPrint(new OrderPrintWork());
        return result;
    }

}
