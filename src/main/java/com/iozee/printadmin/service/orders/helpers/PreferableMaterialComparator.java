package com.iozee.printadmin.service.orders.helpers;

import java.util.Comparator;

/**
 * @author izerenev
 * Date: 23.09.2014
 */
public class PreferableMaterialComparator implements Comparator<PreferableMaterial> {
    @Override
    public int compare(PreferableMaterial o1, PreferableMaterial o2) {
        if (o1.getTotalWaste() < o2.getTotalWaste()) {
            return -1;
        }
        if (o1.getTotalWaste() > o2.getTotalWaste()) {
            return 1;
        }
        //return o1.material.getPrice().compareTo(o2.material.getPrice());
        return 0;
    }
}
