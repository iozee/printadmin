package com.iozee.printadmin.service.orders.helpers;

/**
 * @author izerenev
 * Date: 18.09.2014
 */
public class Format {

    private Integer width;

    private Integer height;

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }
}
