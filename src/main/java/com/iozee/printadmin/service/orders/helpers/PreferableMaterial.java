package com.iozee.printadmin.service.orders.helpers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.iozee.printadmin.enums.OrientationEnum;
import com.iozee.printadmin.model.Material;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author izerenev
 * Date: 23.09.2014
 */
public class PreferableMaterial {

    private static final Logger LOG = LoggerFactory.getLogger(PreferableMaterial.class);

    private PreferableMaterial(Material material, int amount, int width, int height, int waste, int itemsCount) {
        this.material = material;
        this.width = width;
        this.height = height;
        this.wastePerList = waste;
        this.itemsPerList = itemsCount;
        if (width > height) {
            itemOrientation = OrientationEnum.LANDSCAPE;
        } else if (height > width) {
            itemOrientation = OrientationEnum.PORTRAIT;
        } else {
            itemOrientation = OrientationEnum.NA;
        }
        setListsToPrintCount(amount);
        setTotalWaste(amount);
    }

    private final Material material;

    private final int width;

    private final int height;

    private final int wastePerList;

    private final int itemsPerList;

    private final OrientationEnum itemOrientation;

    private int totalWaste;

    private int listsToPrintCount;

    public Material getMaterial() {
        return material;
    }

    public OrientationEnum getItemOrientation() {
        return itemOrientation;
    }

    public int getItemsPerList() {
        return itemsPerList;
    }

    int getListsToPrintCount() {
        return listsToPrintCount;
    }

    private void setListsToPrintCount(int amount) {
        double lists = (double) amount / itemsPerList;
        int intLists = (int) lists;
        if (intLists != lists) {
            listsToPrintCount = intLists + 1;
            return;
        }
        listsToPrintCount = intLists;
    }

    public int getTotalWaste() {
        return totalWaste;
    }

    private void setTotalWaste(int amount) {
        int oneListItemsS = itemsPerList * width * height;
        int overallItemsS = oneListItemsS * getListsToPrintCount();
        int overallRequestedItemsS = amount * width * height;
        int notPrintedWaste = overallItemsS - overallRequestedItemsS;
        int overallWaste = wastePerList * getListsToPrintCount() + notPrintedWaste;
        int overallListsS = material.getWidth() * material.getHeight() * getListsToPrintCount();
        totalWaste = overallWaste * 100 / overallListsS;
    }

    public static List<PreferableMaterial> getPreferableMaterials(List<Material> materialList, int amount, int itemWidth, int itemHeight) {
        List<PreferableMaterial> preferableMaterials = new ArrayList<PreferableMaterial>();
        for (Material material : materialList) {
            LOG.warn("========== " + material.getName() + ", " + material.getWidth() + "х" + material.getHeight() + " ============");
            // by item width
            getMaterialWaste(preferableMaterials, material, amount, itemWidth, itemHeight);
            // by item height
            getMaterialWaste(preferableMaterials, material, amount, itemHeight, itemWidth);
        }
        if (!preferableMaterials.isEmpty()) {
            Collections.sort(preferableMaterials, new PreferableMaterialComparator());
            return preferableMaterials;
        }
        return Collections.emptyList();
    }

    private static void getMaterialWaste(List<PreferableMaterial> preferableMaterials, Material material, int amount, int oneSide, int otherSide) {
        LOG.warn("*** " + oneSide + " x " + otherSide);
        int mWidth = material.getWidth();
        if (mWidth < oneSide) {
            LOG.warn("too short");
            return;
        }
        int mHeight = material.getHeight();
        if (mHeight < otherSide) {
            LOG.warn("too short");
            return;
        }

        int wWaste = mWidth % oneSide;
        int hWaste = mHeight % otherSide;

        int wItemsCount = mWidth / oneSide;
        int hItemsCount = mHeight / otherSide;
        int itemsCount = wItemsCount * hItemsCount;

        int waste = (wWaste * otherSide * hItemsCount) +
                (hWaste * oneSide * wItemsCount) +
                (wWaste * hWaste);

        preferableMaterials.add(new PreferableMaterial(material, amount, oneSide, otherSide, waste, itemsCount));
        LOG.warn("Items count: " + itemsCount);
        LOG.warn("Waste: " + waste);
    }
}
