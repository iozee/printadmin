package com.iozee.printadmin.service.orders.builders;

import com.iozee.printadmin.model.OrderPrintWork;
import com.iozee.printadmin.model.orders.OrderBanner;

/**
 * @author izerenev
 * Date: 09.02.2015
 */
public class BannerBuilder implements EntityBuilder<OrderBanner> {
    @Override
    public OrderBanner build() {
        OrderBanner result = new OrderBanner();
        result.setPrint(new OrderPrintWork());
        return result;
    }

}
