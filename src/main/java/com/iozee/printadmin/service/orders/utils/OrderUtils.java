package com.iozee.printadmin.service.orders.utils;

import com.iozee.printadmin.model.OrderItem;
import com.iozee.printadmin.model.comparators.IdComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * @author izerenev
 * Date: 21.08.2014
 */
public final class OrderUtils {

    private OrderUtils() {
    }

    /**
     * Только для отображения списка позиций по порядку.
     */
    public static List<OrderItem> getSortedOrderItemList(Set<OrderItem> orderItems) {
        List<OrderItem> list = new ArrayList<OrderItem>(orderItems);
        Collections.sort(list, new IdComparator());
        return list;
    }

}
