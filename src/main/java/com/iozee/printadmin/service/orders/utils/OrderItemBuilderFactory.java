package com.iozee.printadmin.service.orders.utils;

import com.iozee.printadmin.enums.ServiceEnum;
import com.iozee.printadmin.model.Order;
import com.iozee.printadmin.model.OrderItem;
import com.iozee.printadmin.model.Service;
import com.iozee.printadmin.service.orders.builders.*;

import java.util.Date;

/**
 * @author izerenev
 * Date: 26.01.2015
 */
public final class OrderItemBuilderFactory {

    private OrderItemBuilderFactory() {
    }

    private static EntityBuilder getBuilder(Service service) {
        String serviceCode = service.getCode();
        if (serviceCode.equals(ServiceEnum.NOTEBOOK.name())) {
            return new NotebookBuilder();
        }
        if (serviceCode.equals(ServiceEnum.BANNER.name())) {
            return new BannerBuilder();
        }
        if (serviceCode.equals(ServiceEnum.VISITING_CARD.name())) {
            return new VisitingCardBuilder();
        }
        if (serviceCode.equals(ServiceEnum.BROCHURE.name())) {
            return new BrochureBuilder();
        }
        if (serviceCode.equals(ServiceEnum.LEAFLET.name())) {
            return new LeafletBuilder();
        }
        if (serviceCode.equals(ServiceEnum.BOOKLET.name())) {
            return new BookletBuilder();
        }
        if (serviceCode.equals(ServiceEnum.CALENDAR.name())) {
            return new CalendarBuilder();
        }
        if (serviceCode.equals(ServiceEnum.STICKER.name())) {
            return new StickerBuilder();
        }
        throw new IllegalArgumentException();
    }

    public static OrderItem createOrderItemByService(Order order, Service service) {
        OrderItem result = getBuilder(service).build();
        result.setService(service);
        result.setDateCreated(new Date());
        result.setTimeCreated(new Date());
        result.setOrder(order);
        return result;
    }

}
