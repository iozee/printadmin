package com.iozee.printadmin.service.orders.builders;

import com.iozee.printadmin.model.OrderPrintWork;
import com.iozee.printadmin.model.orders.OrderBooklet;

/**
 * @author izerenev
 * Date: 09.02.2015
 */
public class BookletBuilder implements EntityBuilder<OrderBooklet> {
    @Override
    public OrderBooklet build() {
        OrderBooklet result = new OrderBooklet();
        result.setFacePrint(new OrderPrintWork());
        result.setBackPrint(new OrderPrintWork());
        return result;
    }

}
