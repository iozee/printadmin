package com.iozee.printadmin.service.orders.helpers;

import com.iozee.printadmin.enums.PrintOptionEnum;
import com.iozee.printadmin.model.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Агргатор по печати - по лицевой и обратной стороне
 *
 * @author izerenev
 * Date: 17.09.2014
 */
public class PrintWork {

    private final Service service;

    public PrintWork(Service service) {
        this.service = service;
    }

    private final List<PrintOptionEnum> printOptions = new ArrayList<PrintOptionEnum>(4) {{
        add(PrintOptionEnum.NO_PRINT);
        add(PrintOptionEnum.FULL_COLOR);
        add(PrintOptionEnum.B_W);
        add(PrintOptionEnum.CMYK);
    }};

    private PrintOptionEnum printOption;

    private List<String> colors = new ArrayList<String>(4);

    public List<String> getColors() {
        return colors;
    }

    public void setColors(List<String> colors) {
        this.colors = colors;
    }

    public PrintOptionEnum getPrintOption() {
        return printOption;
    }

    public void setPrintOption(PrintOptionEnum printOption) {
        this.printOption = printOption;
    }

    public List<PrintOptionEnum> getPrintOptions() {
        return printOptions;
    }

    public Service getService() {
        return service;
    }
}
