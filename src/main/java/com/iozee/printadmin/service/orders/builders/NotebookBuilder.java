package com.iozee.printadmin.service.orders.builders;

import com.iozee.printadmin.model.OrderPrintWork;
import com.iozee.printadmin.model.orders.OrderNotebook;

/**
 * @author izerenev
 * Date: 09.02.2015
 */
public class NotebookBuilder implements EntityBuilder<OrderNotebook> {

    @Override
    public OrderNotebook build() {
        OrderNotebook result = new OrderNotebook();
        result.setCoverFacePrint(new OrderPrintWork());
        result.setCoverBackPrint(new OrderPrintWork());
        result.setCoverBackFacePrint(new OrderPrintWork());
        result.setCoverBackBackPrint(new OrderPrintWork());
        return result;
    }

}
