package com.iozee.printadmin.service.orders.builders;

import com.iozee.printadmin.model.OrderPrintWork;
import com.iozee.printadmin.model.orders.OrderLeaflet;

/**
 * @author izerenev
 * Date: 09.02.2015
 */
public class LeafletBuilder implements EntityBuilder<OrderLeaflet> {
    @Override
    public OrderLeaflet build() {
        OrderLeaflet result = new OrderLeaflet();
        result.setFacePrint(new OrderPrintWork());
        result.setBackPrint(new OrderPrintWork());
        return result;
    }

}
