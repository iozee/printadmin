package com.iozee.printadmin.service.orders.builders;

import com.iozee.printadmin.model.OrderLaminationWork;
import com.iozee.printadmin.model.OrderPrintWork;
import com.iozee.printadmin.model.orders.OrderVisitingCard;

/**
 * @author izerenev
 * Date: 09.02.2015
 */
public class VisitingCardBuilder implements EntityBuilder<OrderVisitingCard> {
    @Override
    public OrderVisitingCard build() {
        OrderVisitingCard result = new OrderVisitingCard();
        result.setFacePrint(new OrderPrintWork());
        result.setBackPrint(new OrderPrintWork());
        result.setLamination(new OrderLaminationWork());
        return result;
    }

}
