package com.iozee.printadmin.service;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * Class representing User entity
 */
public class UserData extends User {

    private final com.iozee.printadmin.model.User user;

    public UserData(com.iozee.printadmin.model.User user, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(user.getName(), user.getPassword(), user.isEnabled(), accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.user = user;
    }

    public com.iozee.printadmin.model.User getUser() {
        return user;
    }
}
