package com.iozee.printadmin.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.iozee.printadmin.dao.MySortOrder;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class CommonCriteria implements ICriteria {

    private int limit;

    private int offset;

    private String sortField;

    private MySortOrder sortOrder;

}
