package com.iozee.printadmin.service;

public interface ICriteria {

    void clear();

    boolean isEmpty();

}
