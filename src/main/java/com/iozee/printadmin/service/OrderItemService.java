package com.iozee.printadmin.service;

import org.springframework.security.access.prepost.PreAuthorize;
import com.iozee.printadmin.security.SPELSecurityExpressions;
import com.iozee.printadmin.model.Order;
import com.iozee.printadmin.model.OrderItem;
import com.iozee.printadmin.model.OrderItemStatus;
import com.iozee.printadmin.model.OrderItemStatusLink;
import com.iozee.printadmin.view.beans.TimelineEntry;

import java.util.List;

/**
 * @author izerenev
 * Date: 02.06.2014
 */
@PreAuthorize(SPELSecurityExpressions.IS_AUTHENTICATED)
public interface OrderItemService {

    OrderItemStatus getItemStatusById(int id);

    @PreAuthorize(SPELSecurityExpressions.MANAGER)
    void setStatus(OrderItemStatus status, OrderItem orderItem);

    @PreAuthorize(SPELSecurityExpressions.MANAGER)
    OrderItem createOrderItem(OrderItem orderItem);

    @PreAuthorize(SPELSecurityExpressions.MANAGER)
    void delete(OrderItem orderItem);

    @PreAuthorize(SPELSecurityExpressions.MANAGER)
    void deleteWithoutOrderUpdate(OrderItem orderItem);

    @PreAuthorize(SPELSecurityExpressions.MANAGER)
    void update(OrderItem orderItem);

    boolean isStatusChangeAllowed(OrderItemStatusLink statusLink);

    List<TimelineEntry> getTimelineEntriesByOrderItem(OrderItem orderItem);

    List<TimelineEntry> getTimelineEntriesByOrder(Order order);

}
