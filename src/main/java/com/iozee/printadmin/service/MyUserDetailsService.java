package com.iozee.printadmin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.iozee.printadmin.dao.UserDao;
import com.iozee.printadmin.model.User;

import java.io.Serializable;

@Service
@Transactional(readOnly = true)
public class MyUserDetailsService implements UserDetailsService, Serializable {

    @Autowired
    private UserDao dao;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        User user = dao.findByName(name);
        if (user != null) {
            return new UserData(user, true, true, true, dao.getUserAuthorities(user));
        }
        return null;
    }
}
