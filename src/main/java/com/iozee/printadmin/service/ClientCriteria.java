package com.iozee.printadmin.service;

import com.google.common.base.Strings;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientCriteria implements ICriteria {

    private String name;

    private String mobileFirst;

    private String phoneFirst;

    private String email;

    private Integer ordersCountFrom;

    private Integer ordersCountTo;

    public void clear() {
        name = null;
        mobileFirst = null;
        phoneFirst = null;
        email = null;
        ordersCountFrom = null;
        ordersCountTo = null;
    }

    @Override
    public boolean isEmpty() {
        return Strings.isNullOrEmpty(name)
                && Strings.isNullOrEmpty(mobileFirst)
                && Strings.isNullOrEmpty(phoneFirst)
                && Strings.isNullOrEmpty(email)
                && ordersCountFrom == null
                && ordersCountTo == null;
    }

}
