package com.iozee.printadmin.service;

import org.springframework.security.access.prepost.PreAuthorize;
import com.iozee.printadmin.model.CommonEntity;
import com.iozee.printadmin.security.SPELSecurityExpressions;
import com.iozee.printadmin.enums.OrderStatusEnum;
import com.iozee.printadmin.model.*;

import java.util.List;

@PreAuthorize(SPELSecurityExpressions.IS_AUTHENTICATED)
public interface OrderService {

    List<Order> getOrdersByClient(Client client);

    OrderStatus getStatusById(int id);

    OrderStatus getStatusByCode(String code);

    Iterable<OrderStatus> getAllStatuses();

    Order getFirstOrder();

    int getOrdersCountByStatus(OrderStatusEnum status);

    String getNewOrderId();

    Iterable<Material> getAllMaterials();

    <Entity extends CommonEntity> Entity getEntityById(int id, Class<Entity> cls);

    Iterable<Lamination> getAllLaminations();

    @PreAuthorize(SPELSecurityExpressions.MANAGER)
    Order create(Order order, List<OrderItem> orderItemList);

    @PreAuthorize(SPELSecurityExpressions.MANAGER)
    void update(Order entity);

    @PreAuthorize(SPELSecurityExpressions.MANAGER)
    void delete(Order entity);

    Order getById(int id);

    List<Order> getAllByCriteria(OrderCriteria criteria);

    int getCountByCriteria(OrderCriteria criteria);

}
