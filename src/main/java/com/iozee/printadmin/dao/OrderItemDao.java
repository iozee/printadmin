package com.iozee.printadmin.dao;

import org.springframework.data.repository.CrudRepository;
import com.iozee.printadmin.model.OrderItem;

public interface OrderItemDao extends CrudRepository<OrderItem, Integer>, OrderItemDaoSpec {
}
