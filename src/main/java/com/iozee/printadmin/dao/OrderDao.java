package com.iozee.printadmin.dao;

import org.springframework.data.repository.CrudRepository;
import com.iozee.printadmin.model.Client;
import com.iozee.printadmin.model.Order;
import com.iozee.printadmin.service.OrderCriteria;

import java.util.List;

// QueryDslPredicateExecutor won't work - Spring Data uses old QueryDSL classes
public interface OrderDao extends CrudRepository<Order, Integer>, CriteriaDao<Order, OrderCriteria>//, QueryDslPredicateExecutor<Order>
{

    List<Order> findByClient(Client client);

}
