package com.iozee.printadmin.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import com.iozee.printadmin.criteria.QOrderRepository;
import com.iozee.printadmin.model.Order;
import com.iozee.printadmin.service.OrderCriteria;

import java.util.List;

@Repository
public class OrderDaoImpl implements CriteriaDao<Order, OrderCriteria> {

    @Autowired
    @Lazy
    protected QOrderRepository orderRepository;

    @Override
    public List<Order> getAllByCriteria(OrderCriteria criteria) {
        return orderRepository.getAllByCriteria(criteria);
    }

    @Override
    public int getCountByCriteria(OrderCriteria criteria) {
        return orderRepository.getCountByCriteria(criteria);
    }

}
