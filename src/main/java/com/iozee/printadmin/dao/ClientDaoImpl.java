package com.iozee.printadmin.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import com.iozee.printadmin.criteria.QClientRepository;
import com.iozee.printadmin.model.Client;
import com.iozee.printadmin.service.ClientCriteria;

import java.util.List;

@Repository
public class ClientDaoImpl implements CriteriaDao<Client, ClientCriteria> {

    @Autowired
    @Lazy
    private QClientRepository clientRepository;

    @Override
    public List<Client> getAllByCriteria(ClientCriteria criteria) {
        return clientRepository.getAllByCriteria(criteria);
    }

    @Override
    public int getCountByCriteria(ClientCriteria criteria) {
        return clientRepository.getCountByCriteria(criteria);
    }
}
