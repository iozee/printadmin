package com.iozee.printadmin.dao;

import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.CrudRepository;
import com.iozee.printadmin.model.OrderItemStatus;

import javax.persistence.QueryHint;

public interface OrderItemStatusDao extends CrudRepository<OrderItemStatus, Integer> {

    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    @Override
    Iterable<OrderItemStatus> findAll();

    OrderItemStatus getStatusByCode(String status);

}
