package com.iozee.printadmin.dao;

import org.springframework.security.core.GrantedAuthority;
import com.iozee.printadmin.model.User;

import java.util.Set;

public interface UserDaoSpec {

    Set<GrantedAuthority> getUserAuthorities(User user);

}

