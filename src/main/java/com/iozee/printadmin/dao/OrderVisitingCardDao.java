package com.iozee.printadmin.dao;

import org.springframework.data.repository.CrudRepository;
import com.iozee.printadmin.model.orders.OrderVisitingCard;

public interface OrderVisitingCardDao extends CrudRepository<OrderVisitingCard, Integer> {
}
