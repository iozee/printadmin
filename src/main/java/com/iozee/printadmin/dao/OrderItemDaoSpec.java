package com.iozee.printadmin.dao;

import com.iozee.printadmin.model.Order;
import com.iozee.printadmin.model.OrderItem;
import com.iozee.printadmin.view.beans.TimelineEntry;

import java.util.List;

public interface OrderItemDaoSpec {

    List<TimelineEntry> getTimelineEntriesByOrderItem(OrderItem orderItem);

    List<TimelineEntry> getTimelineEntriesByOrder(Order order);

}
