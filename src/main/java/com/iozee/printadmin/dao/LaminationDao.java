package com.iozee.printadmin.dao;

import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.CrudRepository;
import com.iozee.printadmin.model.Lamination;

import javax.persistence.QueryHint;

public interface LaminationDao extends CrudRepository<Lamination, Integer> {

    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    @Override
    Iterable<Lamination> findAll();

}
