package com.iozee.printadmin.dao;

import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.CrudRepository;
import com.iozee.printadmin.model.OrderStatus;

import javax.persistence.QueryHint;

public interface OrderStatusDao extends CrudRepository<OrderStatus, Integer> {

    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    @Override
    Iterable<OrderStatus> findAll();

    OrderStatus findByCode(String code);

}
