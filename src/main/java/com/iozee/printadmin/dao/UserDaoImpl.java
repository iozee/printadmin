package com.iozee.printadmin.dao;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Repository;
import com.iozee.printadmin.model.Role;
import com.iozee.printadmin.model.User;

import java.util.HashSet;
import java.util.Set;

@Repository
public class UserDaoImpl implements UserDaoSpec {

    @Override
    public Set<GrantedAuthority> getUserAuthorities(User user) {
        Set<Role> roles = user.getRoles();
        Set<GrantedAuthority> authorities = new HashSet<>(roles.size());
        roles.forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getName())));
        return authorities;
    }

}
