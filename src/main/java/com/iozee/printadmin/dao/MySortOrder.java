package com.iozee.printadmin.dao;

import org.hibernate.criterion.Order;

public enum MySortOrder {

    ASCENDING, DESCENDING, UNSORTED;

    public Order getHibernateOrder(String attribute) {
        switch (this) {
            case ASCENDING:
                return Order.asc(attribute);
            case DESCENDING:
                return Order.desc(attribute);
        }
        throw new IllegalStateException();
    }
}
