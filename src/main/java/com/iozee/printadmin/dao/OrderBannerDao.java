package com.iozee.printadmin.dao;

import org.springframework.data.repository.CrudRepository;
import com.iozee.printadmin.model.orders.OrderBanner;

public interface OrderBannerDao extends CrudRepository<OrderBanner, Integer> {
}
