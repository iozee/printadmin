package com.iozee.printadmin.dao;

import com.iozee.printadmin.model.CommonEntity;
import com.iozee.printadmin.service.ICriteria;

import java.util.List;

public interface CriteriaDao<Entity extends CommonEntity, Criteria extends ICriteria> {

    List<Entity> getAllByCriteria(Criteria criteria);

    int getCountByCriteria(Criteria criteria);

}
