package com.iozee.printadmin.dao;

import org.springframework.data.repository.CrudRepository;
import com.iozee.printadmin.model.orders.OrderNotebook;

public interface OrderNotebookDao extends CrudRepository<OrderNotebook, Integer> {
}
