package com.iozee.printadmin.dao;

import org.springframework.data.repository.CrudRepository;
import com.iozee.printadmin.model.orders.OrderBrochure;

public interface OrderBrochureDao extends CrudRepository<OrderBrochure, Integer> {
}
