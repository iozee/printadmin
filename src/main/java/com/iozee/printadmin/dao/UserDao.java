package com.iozee.printadmin.dao;

import org.springframework.data.repository.CrudRepository;
import com.iozee.printadmin.model.User;

public interface UserDao extends CrudRepository<User, Integer>, UserDaoSpec {

    User findByName(String name);

}
