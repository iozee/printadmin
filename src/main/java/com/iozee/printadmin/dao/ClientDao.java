package com.iozee.printadmin.dao;

import org.springframework.data.repository.CrudRepository;
import com.iozee.printadmin.model.Client;
import com.iozee.printadmin.service.ClientCriteria;

public interface ClientDao extends CrudRepository<Client, Integer>, CriteriaDao<Client, ClientCriteria> {
}
