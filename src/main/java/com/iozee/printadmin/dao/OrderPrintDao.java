package com.iozee.printadmin.dao;

import org.springframework.data.repository.CrudRepository;
import com.iozee.printadmin.model.OrderPrintWork;

public interface OrderPrintDao extends CrudRepository<OrderPrintWork, Integer> {
}
