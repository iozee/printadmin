package com.iozee.printadmin.dao;

import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.CrudRepository;
import com.iozee.printadmin.model.Machine;

import javax.persistence.QueryHint;

public interface MachineDao extends CrudRepository<Machine, Integer> {

    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    @Override
    Iterable<Machine> findAll();

}
