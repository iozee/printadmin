package com.iozee.printadmin.dao;

import org.springframework.data.repository.CrudRepository;
import com.iozee.printadmin.model.orders.OrderLeaflet;

public interface OrderLeafletDao extends CrudRepository<OrderLeaflet, Integer> {
}
