package com.iozee.printadmin.dao;

import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.CrudRepository;
import com.iozee.printadmin.model.Material;

import javax.persistence.QueryHint;

public interface MaterialDao extends CrudRepository<Material, Integer> {

    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    @Override
    Iterable<Material> findAll();

}
