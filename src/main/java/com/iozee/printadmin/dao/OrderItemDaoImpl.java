package com.iozee.printadmin.dao;

import org.hibernate.SessionFactory;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.iozee.printadmin.dao.envers.EnversEntity;
import com.iozee.printadmin.model.Order;
import com.iozee.printadmin.model.OrderItem;
import com.iozee.printadmin.model.QOrderItem;
import com.iozee.printadmin.view.beans.TimelineEntry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Repository
public class OrderItemDaoImpl implements OrderItemDaoSpec {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<TimelineEntry> getTimelineEntriesByOrderItem(OrderItem orderItem) {
        List resultList = AuditReaderFactory.get(sessionFactory.getCurrentSession())
                .createQuery()
                .forRevisionsOfEntity(OrderItem.class, false, false)
                .add(AuditEntity.property(QOrderItem.orderItem.order.getMetadata().getName()).hasChanged())
                .add(AuditEntity.id().eq(orderItem.getId()))
                .addOrder(AuditEntity.id().asc())
                .getResultList();
        if (resultList == null || resultList.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList<TimelineEntry> timelineEntries = new ArrayList<TimelineEntry>(resultList.size());
        for (Object result : resultList) {
            Object[] res = (Object[]) result;
            String title = ((OrderItem) res[0]).getStatus().getName();
            Date date = ((EnversEntity) res[1]).getRevisionDate();
            timelineEntries.add(new TimelineEntry(title, date));
        }
        return timelineEntries;
    }

    @Override
    public List<TimelineEntry> getTimelineEntriesByOrder(Order order) {
        List resultList = AuditReaderFactory.get(sessionFactory.getCurrentSession())
                .createQuery()
                .forRevisionsOfEntity(OrderItem.class, false, false)
                .add(AuditEntity.property(QOrderItem.orderItem.order.getMetadata().getName()).eq(order))
                .add(AuditEntity.property(QOrderItem.orderItem.status.getMetadata().getName()).hasChanged())
                .addOrder(AuditEntity.id().asc())
                .getResultList();
        if (resultList == null || resultList.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList<TimelineEntry> timelineEntries = new ArrayList<TimelineEntry>(resultList.size());
        for (Object result : resultList) {
            Object[] res = (Object[]) result;
            String title = ((OrderItem) res[0]).getStatus().getName();
            Date date = ((EnversEntity) res[1]).getRevisionDate();
            String group = ((OrderItem) res[0]).getService().getName();
            int groupId = ((OrderItem) res[0]).getId();
            timelineEntries.add(new TimelineEntry(title, date, group, groupId));
        }
        return timelineEntries;
    }

}
