package com.iozee.printadmin.dao;

import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.CrudRepository;
import com.iozee.printadmin.model.Service;

import javax.persistence.QueryHint;

public interface ServiceDao extends CrudRepository<Service, Integer> {

    @QueryHints({@QueryHint(name = "org.hibernate.cacheable", value = "true")})
    @Override
    Iterable<Service> findAll();

}
