package com.iozee.printadmin.dao.envers;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;
import com.iozee.printadmin.model.User;

import javax.persistence.*;

@Entity
@RevisionEntity(EnversListener.class)
@Table(name = "aud_envers_custom")
@Access(AccessType.FIELD)
public class EnversEntity extends DefaultRevisionEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user", nullable = false)
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
