@QueryEntities({DefaultRevisionEntity.class})
package com.iozee.printadmin.dao.envers;

import com.querydsl.core.annotations.QueryEntities;
import org.hibernate.envers.DefaultRevisionEntity;