package com.iozee.printadmin.dao.envers;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.PathInits;
import com.iozee.printadmin.model.QUser;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QEnversEntity is a Querydsl query type for EnversEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QEnversEntity extends EntityPathBase<EnversEntity> {

    private static final long serialVersionUID = 1110755746L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QEnversEntity enversEntity = new QEnversEntity("enversEntity");

    public final QUser user;

    public QEnversEntity(String variable) {
        this(EnversEntity.class, forVariable(variable), INITS);
    }

    public QEnversEntity(Path<? extends EnversEntity> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QEnversEntity(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QEnversEntity(PathMetadata metadata, PathInits inits) {
        this(EnversEntity.class, metadata, inits);
    }

    public QEnversEntity(Class<? extends EnversEntity> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.user = inits.isInitialized("user") ? new QUser(forProperty("user")) : null;
    }

}

