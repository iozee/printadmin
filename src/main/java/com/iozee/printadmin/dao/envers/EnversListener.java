package com.iozee.printadmin.dao.envers;

import org.hibernate.envers.RevisionListener;
import com.iozee.printadmin.security.SecurityUtils;

public class EnversListener implements RevisionListener {

    public void newRevision(Object revisionEntity) {
        EnversEntity revision = (EnversEntity) revisionEntity;
        revision.setUser(SecurityUtils.getCurrentUser());
    }

}