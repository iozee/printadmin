package com.iozee.printadmin.criteria;

import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.jpa.hibernate.HibernateQuery;
import com.querydsl.jpa.hibernate.HibernateQueryFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.iozee.printadmin.model.CommonEntity;
import com.iozee.printadmin.service.ICriteria;

import javax.annotation.PostConstruct;
import javax.annotation.concurrent.Immutable;
import java.util.List;

@Repository
public abstract class QCommonHibernateRepository<Entity extends CommonEntity, QEntity extends EntityPathBase<Entity>, Criteria extends ICriteria> {

    @Autowired
    private SessionFactory sessionFactory;

    private SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    protected abstract QEntity getEntity();

    protected final HibernateQuery<Entity> getNewQuery() {
        return new HibernateQueryFactory(getSessionFactory().getCurrentSession()).selectFrom(getEntity());
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @PostConstruct
    private void init() {
        getSessionFactory();
    }

    @Transactional(readOnly = true)
    public List<Entity> getAllByCriteria(Criteria criteria) {
        return applyCriteria(criteria).fetch();
    }

    @Transactional(readOnly = true)
    public int getCountByCriteria(Criteria criteria) {
        return (int) applyCriteria(criteria).fetchCount();
    }

    protected abstract HibernateQuery<Entity> applyCriteria(Criteria criteria);

}
