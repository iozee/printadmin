package com.iozee.printadmin.criteria;

import com.google.common.base.Strings;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.hibernate.HibernateQuery;
import org.springframework.stereotype.Repository;
import com.iozee.printadmin.model.Client;
import com.iozee.printadmin.model.QClient;
import com.iozee.printadmin.service.ClientCriteria;

import javax.annotation.concurrent.Immutable;

@Repository
public class QClientRepository extends QCommonHibernateRepository<Client, QClient, ClientCriteria> {

    @Override
    protected QClient getEntity() {
        return QClient.client;
    }

    private void addNameCondition(JPQLQuery<Client> query, String name) {
        if (!Strings.isNullOrEmpty(name)) {
            query.where(getEntity().name.containsIgnoreCase(name));
        }
    }

    private void addMobileFirstCondition(JPQLQuery<Client> query, String mobileFirst) {
        if (!Strings.isNullOrEmpty(mobileFirst)) {
            query.where(getEntity().mobileFirst.equalsIgnoreCase(mobileFirst));
        }
    }

    private void addPhoneFirstCondition(JPQLQuery<Client> query, String phoneFirst) {
        if (!Strings.isNullOrEmpty(phoneFirst)) {
            query.where(getEntity().phoneFirst.equalsIgnoreCase(phoneFirst));
        }
    }

    private void addEmailCondition(JPQLQuery<Client> query, String email) {
        if (!Strings.isNullOrEmpty(email)) {
            query.where(getEntity().email.containsIgnoreCase(email));
        }
    }

    private void addOrdersCountBetweenCondition(JPQLQuery<Client> query, Integer ordersCountFrom, Integer ordersCountTo) {
        if (ordersCountFrom != null && ordersCountTo != null) {
            query.where(getEntity().ordersCount.between(ordersCountFrom, ordersCountTo));
        }
    }

    private void addOrdersCountFromCondition(JPQLQuery<Client> query, Integer ordersCountFrom) {
        if (ordersCountFrom != null) {
            query.where(getEntity().ordersCount.goe(ordersCountFrom));
        }
    }

    private void addOrdersCountToCondition(JPQLQuery<Client> query, Integer ordersCountTo) {
        if (ordersCountTo != null) {
            query.where(getEntity().ordersCount.loe(ordersCountTo));
        }
    }

    @Override
    protected HibernateQuery<Client> applyCriteria(ClientCriteria criteria) {
        HibernateQuery<Client> query = getNewQuery();
        addNameCondition(query, criteria.getName());
        addMobileFirstCondition(query, criteria.getMobileFirst());
        addPhoneFirstCondition(query, criteria.getPhoneFirst());
        addEmailCondition(query, criteria.getEmail());
        addOrdersCountFromCondition(query, criteria.getOrdersCountFrom());
        addOrdersCountToCondition(query, criteria.getOrdersCountTo());
        addOrdersCountBetweenCondition(query, criteria.getOrdersCountFrom(), criteria.getOrdersCountTo());
        return query;
    }

}
