package com.iozee.printadmin.criteria;

import com.google.common.base.Strings;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.ComparableExpressionBase;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.hibernate.HibernateQuery;
import org.springframework.stereotype.Repository;
import com.iozee.printadmin.dao.MySortOrder;
import com.iozee.printadmin.enums.OrderStatusEnum;
import com.iozee.printadmin.model.*;
import com.iozee.printadmin.service.OrderCriteria;

import javax.annotation.concurrent.Immutable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

@Repository
public class QOrderRepository extends QCommonHibernateRepository<Order, QOrder, OrderCriteria> {

    protected HibernateQuery<Order> applyCriteria(OrderCriteria criteria) {
        HibernateQuery<Order> query = getNewQuery();
        addOrderIdCondition(query, criteria.getOrderId());
        addStartDateCondition(query, criteria.getStartDate());
        addEndDateCondition(query, criteria.getEndDate());
        addDateBetweenCondition(query, criteria.getStartDate(), criteria.getEndDate());
        addCostFromCondition(query, criteria.getCostFrom());
        addCostToCondition(query, criteria.getCostTo());
        addCostBetweenCondition(query, criteria.getCostFrom(), criteria.getCostTo());
        addClientCondition(query, criteria.getClient());
        addStatusCondition(query, criteria.getStatus());
        addServicesCondition(query, criteria.getServices());
        addSort(query, criteria);
        setLimit(query, criteria);
        return query;
    }

    private void setLimit(JPQLQuery<Order> query, OrderCriteria criteria) {
        query.limit(criteria.getLimit());
    }

    private void addSort(JPQLQuery<Order> query, OrderCriteria orderCriteria) {
        String sortField = orderCriteria.getSortField();
        if (Strings.isNullOrEmpty(sortField)) {
            return;
        }
        MySortOrder sortOrder = orderCriteria.getSortOrder() == null ? MySortOrder.ASCENDING : orderCriteria.getSortOrder();
        switch (sortField) {
            case "dateCreated":
                applyOrderByField(query, sortOrder, getEntity().dateCreated);
                break;
            case "itemCount":
                applyOrderByField(query, sortOrder, getEntity().itemCount);
                break;
            case "cost":
                applyOrderByField(query, sortOrder, getEntity().cost);
                break;
            case "readiness":
                applyOrderByField(query, sortOrder, getEntity().readiness);
                break;
        }
    }

    private void applyOrderByField(JPQLQuery<Order> query, MySortOrder sortOrder, ComparableExpressionBase<? extends Comparable> dateCreated) {
        OrderSpecifier orderSpecifier;
        switch (sortOrder) {
            case ASCENDING:
                orderSpecifier = dateCreated.asc();
                break;
            case DESCENDING:
            default:
                orderSpecifier = dateCreated.desc();
                break;
        }
        query.orderBy(orderSpecifier);
    }

    @Override
    protected QOrder getEntity() {
        return QOrder.order;
    }

  /*
  public QOrderRepository(int firstItemIndex, int limit, String sortField, MySortOrder sortOrder, Map<String, Object> conditions) {
    super(firstItemIndex, limit);
    processOrderIdCondition(conditions);
    processClientCondition(conditions);
    processStatusCondition(conditions);
  }
  */

    private void processOrderIdCondition(Map<String, Object> conditions) {
        String orderIdFilter = (String) conditions.get("orderId");
        if (!Strings.isNullOrEmpty(orderIdFilter)) {
            //addOrderIdCondition(query, orderIdFilter);
        }
    }

    private void processClientCondition(Map<String, Object> conditions) {
        Client clientFilter = (Client) conditions.get("client");
        if (clientFilter != null) {
            // addClientCondition(query, clientFilter);
        }
    }

    private void processStatusCondition(Map<String, Object> conditions) {
        OrderStatusEnum statusFilter = (OrderStatusEnum) conditions.get("status");
        if (statusFilter != null) {
            switch (statusFilter) {
                case NEW:
                    //addNewStatusCondition(query);
                    break;
                case IN_WORK:
                    //addInWorkStatusCondition(query);
                    break;
                case COMPLETE:
                    //addCompleteStatusCondition(query);
                    break;
            }
        }
    }

    private void addStatusCondition(JPQLQuery<Order> query, OrderStatusEnum status) {
        if (status == null) {
            return;
        }
        switch (status) {
            case NEW:
                addNewStatusCondition(query);
                break;
            case IN_WORK:
                addInWorkStatusCondition(query);
                break;
            case COMPLETE:
                addCompleteStatusCondition(query);
                break;
        }
    }

    private void addOrderIdCondition(JPQLQuery<Order> query, String orderId) {
        if (!Strings.isNullOrEmpty(orderId)) {
            query.where(getEntity().orderId.contains(orderId));
        }
    }

    private void addClientCondition(JPQLQuery<Order> query, Client client) {
        if (client != null) {
            query.where(getEntity().client.eq(client));
        }
    }

    private void addServicesCondition(JPQLQuery<Order> query, Collection<Service> services) {
        if (services != null && !services.isEmpty()) {
            query.innerJoin(getEntity().orderItems, QOrderItem.orderItem)
                    .where(QOrderItem.orderItem.service.in(services));
        }
    }

    private void addDateBetweenCondition(JPQLQuery<Order> query, Date startDate, Date endDate) {
        if (startDate != null && endDate != null) {
            query.where(getEntity().dateCreated.between(startDate, endDate));
        }
    }

    private void addStartDateCondition(JPQLQuery<Order> query, Date startDate) {
        if (startDate != null) {
            query.where(getEntity().dateCreated.goe(startDate));
        }
    }

    private void addEndDateCondition(JPQLQuery<Order> query, Date endDate) {
        if (endDate != null) {
            query.where(getEntity().dateCreated.loe(endDate));
        }
    }

    private void addCostBetweenCondition(JPQLQuery<Order> query, BigDecimal costFrom, BigDecimal costTo) {
        if (costFrom != null && costTo != null) {
            query.where(getEntity().cost.between(costFrom, costTo));
        }
    }

    private void addCostFromCondition(JPQLQuery<Order> query, BigDecimal costFrom) {
        if (costFrom != null) {
            query.where(getEntity().cost.goe(costFrom));
        }
    }

    private void addCostToCondition(JPQLQuery<Order> query, BigDecimal costTo) {
        if (costTo != null) {
            query.where(getEntity().cost.loe(costTo));
        }
    }

    private void addNewStatusCondition(JPQLQuery<Order> query) {
        query.where(getEntity().readiness.eq((byte) 0));
    }

    private void addInWorkStatusCondition(JPQLQuery<Order> query) {
        query.where(getEntity().readiness.between((byte) 1, (byte) 99));
    }

    private void addCompleteStatusCondition(JPQLQuery<Order> query) {
        query.where(getEntity().readiness.eq((byte) 100));
    }

}
