package com.iozee.printadmin.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author izerenev
 * Date: 03.12.2014
 */
public class AjaxAwareAuthenticationEntryPoint extends LoginUrlAuthenticationEntryPoint {

    public AjaxAwareAuthenticationEntryPoint(final String loginFormUrl) {
        super(loginFormUrl);
    }

    @Override
    public void commence(final HttpServletRequest request, final HttpServletResponse response, final AuthenticationException authException)
            throws IOException, ServletException {
        String jsfRequestHeader = request.getHeader("faces-request");
        if ("XMLHttpRequest".equalsIgnoreCase(request.getHeader("X-Requested-With")) || (jsfRequestHeader != null && jsfRequestHeader.contains("ajax"))) {
            response.getWriter().print(xmlPartialRedirectToLogin(request));
            response.flushBuffer();
        } else {
            super.commence(request, response, authException);
        }
    }

    private String xmlPartialRedirectToLogin(HttpServletRequest request) {
        return "<?xml version='1.0' encoding='UTF-8'?>" + "<partial-response><redirect url=\"" + request.getContextPath() + request.getServletPath() + "/login" + "\"/></partial-response>";
    }
}