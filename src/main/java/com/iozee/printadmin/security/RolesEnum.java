package com.iozee.printadmin.security;

import com.iozee.printadmin.model.Role;

import java.util.*;

/**
 * @author izerenev
 * Date: 21.03.2014
 */
public enum RolesEnum {

    ROLE_ANONYMOUS,

    ROLE_SA,

    ROLE_MANAGER,

    ROLE_PREPRESS,

    ROLE_PRESS,

    ROLE_POSTPRESS;

  /*
  These constants are used only where enums can not be used (e.g. as annotation values)
   */

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String SA = "ROLE_SA";

    public static final String MANAGER = "ROLE_MANAGER";

    public static final String PREPRESS = "PREPRESS";

    public static final String PRESS = "PRESS";

    public static final String POSTPRESS = "POSTPRESS";

    public static Collection<RolesEnum> getAuthorizedRoles() {
        return Collections.unmodifiableCollection(new ArrayList<RolesEnum>(5) {{
            add(ROLE_SA);
            add(ROLE_MANAGER);
            add(ROLE_PREPRESS);
            add(ROLE_PRESS);
            add(ROLE_POSTPRESS);
        }});
    }

    @Override
    public String toString() {
        return name();
    }

    public static Set<RolesEnum> toRoleSet(Collection<Role> roles) {
        Set<RolesEnum> result = new HashSet<>(roles.size());
        roles.forEach(role -> result.add(RolesEnum.valueOf(role.getName())));
        return Collections.unmodifiableSet(result);
    }
}
