package com.iozee.printadmin.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import com.iozee.printadmin.model.User;
import com.iozee.printadmin.service.UserData;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class SecurityUtils {

    public static User getCurrentUser() {
        return ((UserData) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
    }

    /*
     * Used in custom security taglib
     */
    public static boolean isManager() {
        return isCurrentUserInRole(RolesEnum.ROLE_MANAGER);
    }

    /*
     * Used in custom security taglib
     */
    public static boolean isPrePress() {
        return isCurrentUserInRole(RolesEnum.ROLE_PREPRESS);
    }

    /*
     * Used in custom security taglib
     */
    public static boolean isSA() {
        return isCurrentUserInRole(RolesEnum.ROLE_SA);
    }

    /*
     * Used in custom security taglib
     */
    public static boolean isLoggedIn() {
        return !Collections.disjoint(RolesEnum.getAuthorizedRoles(), getUserRoles());
    }

    public static boolean hasAnyRole(Collection<RolesEnum> roles) {
        return !Collections.disjoint(roles, getUserRoles());
    }

    private static Set<RolesEnum> getUserRoles() {
        SecurityContext context = SecurityContextHolder.getContext();
        if (context == null) {
            return Collections.emptySet();
        }
        Authentication authentication = context.getAuthentication();
        if (authentication == null) {
            return Collections.emptySet();
        }
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        if (authorities == null || authorities.isEmpty()) {
            return Collections.emptySet();
        }
        Set<RolesEnum> roles = new HashSet<>(authorities.size());
        authorities.forEach(authority -> roles.add(RolesEnum.valueOf(authority.getAuthority())));
        return Collections.unmodifiableSet(roles);
    }

    private static boolean isCurrentUserInRole(RolesEnum role) {
        Collection<? extends GrantedAuthority> userAuthorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        for (GrantedAuthority userAuthority : userAuthorities) {
            if (userAuthority.getAuthority().equals(RolesEnum.ROLE_SA.toString())) {
                return true;
            }
            if (userAuthority.getAuthority().equals(role.toString())) {
                return true;
            }
        }
        return false;
    }

}
