package com.iozee.printadmin.security;

public enum SPELSecurityExpressions {
    // todo
    ;

    public static final String IS_AUTHENTICATED = "isAuthenticated()";

    public static final String MANAGER = "hasAnyRole('ROLE_SA, ROLE_MANAGER')";

}
