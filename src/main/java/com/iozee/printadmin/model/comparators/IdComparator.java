package com.iozee.printadmin.model.comparators;

import com.iozee.printadmin.model.CommonEntity;

import java.util.Comparator;

public class IdComparator implements Comparator<CommonEntity> {
    @Override
    public int compare(CommonEntity o1, CommonEntity o2) {
        if (o1.getId() > o2.getId()) {
            return 1;
        }
        if (o1.getId() < o2.getId()) {
            return -1;
        }
        return 0;
    }
}
