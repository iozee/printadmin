package com.iozee.printadmin.model;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QOrderPrintWork is a Querydsl query type for OrderPrintWork
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QOrderPrintWork extends EntityPathBase<OrderPrintWork> {

    private static final long serialVersionUID = -1674252408L;

    public static final QOrderPrintWork orderPrintWork = new QOrderPrintWork("orderPrintWork");

    public final QCommonEntity _super = new QCommonEntity(this);

    public final BooleanPath bw = createBoolean("bw");

    public final BooleanPath c = createBoolean("c");

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public final BooleanPath isFullColor = createBoolean("isFullColor");

    public final BooleanPath k = createBoolean("k");

    public final BooleanPath m = createBoolean("m");

    public final BooleanPath noPrint = createBoolean("noPrint");

    public final BooleanPath y = createBoolean("y");

    public QOrderPrintWork(String variable) {
        super(OrderPrintWork.class, forVariable(variable));
    }

    public QOrderPrintWork(Path<? extends OrderPrintWork> path) {
        super(path.getType(), path.getMetadata());
    }

    public QOrderPrintWork(PathMetadata metadata) {
        super(OrderPrintWork.class, metadata);
    }

}

