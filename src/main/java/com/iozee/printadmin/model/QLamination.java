package com.iozee.printadmin.model;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QLamination is a Querydsl query type for Lamination
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QLamination extends EntityPathBase<Lamination> {

    private static final long serialVersionUID = -821928912L;

    public static final QLamination lamination = new QLamination("lamination");

    public final QNamedCommonEntity _super = new QNamedCommonEntity(this);

    //inherited
    public final NumberPath<Integer> id = _super.id;

    //inherited
    public final StringPath name = _super.name;

    public QLamination(String variable) {
        super(Lamination.class, forVariable(variable));
    }

    public QLamination(Path<? extends Lamination> path) {
        super(path.getType(), path.getMetadata());
    }

    public QLamination(PathMetadata metadata) {
        super(Lamination.class, metadata);
    }

}

