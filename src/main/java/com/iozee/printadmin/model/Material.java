package com.iozee.printadmin.model;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author izerenev
 * Date: 22.08.2014
 */
@Entity
@Table(name = "material")
@Access(AccessType.FIELD)
public class Material extends NamedCommonEntity {

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "width")
    private int width;

    @Column(name = "height")
    private int height;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
