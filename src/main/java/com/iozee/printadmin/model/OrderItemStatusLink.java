package com.iozee.printadmin.model;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Order item status transition
 */
@Entity
@Table(name = "order_item_status_link")
@Access(AccessType.FIELD)
@Immutable
public class OrderItemStatusLink extends NamedCommonEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orderItemStatusId", nullable = false)
    private OrderItemStatus orderItemStatus;

    /**
     * Next order item status
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nextOrderItemStatusId", nullable = false)
    private OrderItemStatus nextStatus;

    /**
     * Transition security
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "orderItemStatusLink")
    @BatchSize(size = 2)
    private Set<OrderItemStatusLinkSecurity> orderItemStatusLinkSecurities;

    public OrderItemStatus getNextStatus() {
        return nextStatus;
    }

    public void setNextStatus(OrderItemStatus nextStatus) {
        this.nextStatus = nextStatus;
    }

    public Set<Role> getRoles() {
        return orderItemStatusLinkSecurities.stream()
                .map(OrderItemStatusLinkSecurity::getRole)
                .collect(Collectors.toCollection(HashSet::new));
    }
}