package com.iozee.printadmin.model;

import javax.persistence.*;
import java.io.Serializable;

@MappedSuperclass
public abstract class CommonEntity implements Serializable {

    private int id;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    public final int getId() {
        return id;
    }

    public final void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "@" + id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommonEntity)) {
            return false;
        }
        if (id == 0) {
            return super.equals(o);
        }
        CommonEntity that = (CommonEntity) o;
        String thisId = getClass().getSimpleName() + "@" + id;
        String thatId = that.getClass().getSimpleName() + "@" + that.id;

        return thisId.equals(thatId);
    }

    @Override
    public int hashCode() {
        if (id == 0) {
            return super.hashCode();
        }
        String thisId = getClass().getSimpleName() + "@" + id;
        return thisId.hashCode();
    }
}
