package com.iozee.printadmin.model;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;
import org.hibernate.envers.Audited;
import com.iozee.printadmin.enums.PrintOptionEnum;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author izerenev
 * Date: 29.09.2014
 */
@Entity
@Table(name = "order_print_work")
@Access(AccessType.FIELD)
@Audited
@DynamicUpdate
@SelectBeforeUpdate
public class OrderPrintWork extends CommonEntity implements IWork {

    @Transient
    private PrintOptionEnum printOption;

    @Transient
    private List<String> cmyk;

    @Column(name = "noPrint")
    private boolean noPrint;

    @Column(name = "isFullColor")
    private boolean isFullColor;

    @Column(name = "bw")
    private boolean bw;

    @Column(name = "c")
    private boolean c;

    @Column(name = "m")
    private boolean m;

    @Column(name = "y")
    private boolean y;

    @Column(name = "k")
    private boolean k;

    public PrintOptionEnum getPrintOption() {
        if (printOption != null) {
            return printOption;
        }
        if (bw) {
            return PrintOptionEnum.B_W;
        }
        if (isFullColor) {
            return PrintOptionEnum.FULL_COLOR;
        }
        if (noPrint) {
            return PrintOptionEnum.NO_PRINT;
        }
        if (c || m || y || k) {
            return PrintOptionEnum.CMYK;
        }
        return PrintOptionEnum.NO_PRINT;
    }

    public void setPrintOption(PrintOptionEnum printOption) {
        this.printOption = printOption;
        switch (printOption) {
            case FULL_COLOR:
                noPrint = false;
                isFullColor = true;
                bw = false;
                c = false;
                m = false;
                y = false;
                k = false;
                return;
            case B_W:
                noPrint = false;
                isFullColor = false;
                bw = true;
                c = false;
                m = false;
                y = false;
                k = false;
                return;
            case NO_PRINT:
                noPrint = true;
                isFullColor = false;
                bw = false;
                c = false;
                m = false;
                y = false;
                k = false;
                return;
            case CMYK:
                noPrint = false;
                isFullColor = false;
                bw = false;
                c = false;
                m = false;
                y = false;
                k = false;
                return;
        }
    }

    public List<String> getCmyk() {
        if (cmyk != null) {
            return cmyk;
        }
        return new ArrayList<String>(4) {{
            if (c) {
                add("c");
            }
            if (m) {
                add("m");
            }
            if (y) {
                add("y");
            }
            if (k) {
                add("k");
            }
        }};
    }

    public void setCmyk(List<String> cmyk) {
        this.cmyk = cmyk;
        for (String color : cmyk) {
            if ("c".equals(color)) {
                c = true;
                continue;
            }
            if ("m".equals(color)) {
                m = true;
                continue;
            }
            if ("y".equals(color)) {
                y = true;
                continue;
            }
            if ("k".equals(color)) {
                k = true;
            }
        }
    }

    public boolean isNoPrint() {
        return noPrint;
    }

    public void setNoPrint(boolean noPrint) {
        this.noPrint = noPrint;
    }

    public boolean isFullColor() {
        return isFullColor;
    }

    public void setIsFullColor(boolean fullColor) {
        this.isFullColor = fullColor;
    }

    public boolean isBw() {
        return bw;
    }

    public void setBw(boolean bw) {
        this.bw = bw;
    }

    public boolean isC() {
        return c;
    }

    public void setC(boolean c) {
        this.c = c;
    }

    public boolean isM() {
        return m;
    }

    public void setM(boolean m) {
        this.m = m;
    }

    public boolean isY() {
        return y;
    }

    public void setY(boolean y) {
        this.y = y;
    }

    public boolean isK() {
        return k;
    }

    public void setK(boolean k) {
        this.k = k;
    }

}