package com.iozee.printadmin.model;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.util.Set;

/**
 * @author izerenev
 * Date: 10.06.2014
 */
@Entity
@Table(name = "order_item_status")
@Access(AccessType.FIELD)
@Immutable
public class OrderItemStatus extends NamedCommonEntity {

    @Column(name = "code")
    private String code;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "orderItemStatus")
    private Set<OrderItemStatusLink> orderItemStatusLinks;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Set<OrderItemStatusLink> getOrderItemStatusLinks() {
        return orderItemStatusLinks;
    }

    public void setOrderItemStatusLinks(Set<OrderItemStatusLink> orderItemStatusLinks) {
        this.orderItemStatusLinks = orderItemStatusLinks;
    }
}
