package com.iozee.printadmin.model;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;

/**
 * @author izerenev
 * Date: 10.06.2014
 */
@Entity
@Table(name = "order_status")
@Access(AccessType.FIELD)
@Immutable
public class OrderStatus extends NamedCommonEntity {

    @Column(name = "code")
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
