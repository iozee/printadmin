package com.iozee.printadmin.model;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathInits;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QOrderItemStatusLinkSecurity is a Querydsl query type for OrderItemStatusLinkSecurity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QOrderItemStatusLinkSecurity extends EntityPathBase<OrderItemStatusLinkSecurity> {

    private static final long serialVersionUID = -1920637195L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QOrderItemStatusLinkSecurity orderItemStatusLinkSecurity = new QOrderItemStatusLinkSecurity("orderItemStatusLinkSecurity");

    public final QCommonEntity _super = new QCommonEntity(this);

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public final QOrderItemStatusLink orderItemStatusLink;

    public final QRole role;

    public QOrderItemStatusLinkSecurity(String variable) {
        this(OrderItemStatusLinkSecurity.class, forVariable(variable), INITS);
    }

    public QOrderItemStatusLinkSecurity(Path<? extends OrderItemStatusLinkSecurity> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QOrderItemStatusLinkSecurity(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QOrderItemStatusLinkSecurity(PathMetadata metadata, PathInits inits) {
        this(OrderItemStatusLinkSecurity.class, metadata, inits);
    }

    public QOrderItemStatusLinkSecurity(Class<? extends OrderItemStatusLinkSecurity> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.orderItemStatusLink = inits.isInitialized("orderItemStatusLink") ? new QOrderItemStatusLink(forProperty("orderItemStatusLink"), inits.get("orderItemStatusLink")) : null;
        this.role = inits.isInitialized("role") ? new QRole(forProperty("role")) : null;
    }

}

