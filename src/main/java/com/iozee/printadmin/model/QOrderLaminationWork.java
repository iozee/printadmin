package com.iozee.printadmin.model;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.PathInits;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QOrderLaminationWork is a Querydsl query type for OrderLaminationWork
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QOrderLaminationWork extends EntityPathBase<OrderLaminationWork> {

    private static final long serialVersionUID = -1005831777L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QOrderLaminationWork orderLaminationWork = new QOrderLaminationWork("orderLaminationWork");

    public final QCommonEntity _super = new QCommonEntity(this);

    public final BooleanPath bothSide = createBoolean("bothSide");

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public final QLamination lamination;

    public QOrderLaminationWork(String variable) {
        this(OrderLaminationWork.class, forVariable(variable), INITS);
    }

    public QOrderLaminationWork(Path<? extends OrderLaminationWork> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QOrderLaminationWork(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QOrderLaminationWork(PathMetadata metadata, PathInits inits) {
        this(OrderLaminationWork.class, metadata, inits);
    }

    public QOrderLaminationWork(Class<? extends OrderLaminationWork> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.lamination = inits.isInitialized("lamination") ? new QLamination(forProperty("lamination")) : null;
    }

}

