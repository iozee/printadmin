package com.iozee.printadmin.model;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Table(name = "order_item_status_link_security")
@Access(AccessType.FIELD)
@Immutable
public class OrderItemStatusLinkSecurity extends CommonEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orderItemStatusLinkId", nullable = false)
    private OrderItemStatusLink orderItemStatusLink;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "roleId", nullable = false)
    private Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}