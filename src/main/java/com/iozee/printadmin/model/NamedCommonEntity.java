package com.iozee.printadmin.model;

import com.google.common.base.Strings;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
public abstract class NamedCommonEntity extends CommonEntity implements Serializable, Comparable {
    private String name;

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name != null && !name.equals("") ? name : super.toString();
    }

    @Override
    public int compareTo(Object o) {
        if (getClass() != o.getClass()) {
            throw new IllegalArgumentException("Error comparing object " + getId() + " to object " + o + ". Input object class mismatch - " + o.getClass());
        }
        if (Strings.isNullOrEmpty(getName())) {
            return -1;
        }
        if (Strings.isNullOrEmpty(((NamedCommonEntity) o).getName())) {
            return 1;
        }
        return getName().compareTo(((NamedCommonEntity) o).getName());
    }
}
