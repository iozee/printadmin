package com.iozee.printadmin.model.orders;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.*;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QOrderBanner is a Querydsl query type for OrderBanner
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QOrderBanner extends EntityPathBase<OrderBanner> {

    private static final long serialVersionUID = 486414889L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QOrderBanner orderBanner = new QOrderBanner("orderBanner");

    public final com.iozee.printadmin.model.QOrderItem _super;

    //inherited
    public final NumberPath<Integer> amount;

    //inherited
    public final StringPath comment;

    //inherited
    public final NumberPath<java.math.BigDecimal> cost;

    //inherited
    public final DateTimePath<java.util.Date> dateCreated;

    //inherited
    public final BooleanPath deleted;

    public final NumberPath<Integer> height = createNumber("height", Integer.class);

    //inherited
    public final NumberPath<Integer> id;

    public final com.iozee.printadmin.model.QMaterial material;

    // inherited
    public final com.iozee.printadmin.model.QOrder order;

    public final com.iozee.printadmin.model.QOrderPrintWork print;

    // inherited
    public final com.iozee.printadmin.model.QService service;

    // inherited
    public final com.iozee.printadmin.model.QOrderItemStatus status;

    //inherited
    public final DateTimePath<java.util.Date> timeCreated;

    public final NumberPath<Integer> width = createNumber("width", Integer.class);

    public QOrderBanner(String variable) {
        this(OrderBanner.class, forVariable(variable), INITS);
    }

    public QOrderBanner(Path<? extends OrderBanner> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QOrderBanner(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QOrderBanner(PathMetadata metadata, PathInits inits) {
        this(OrderBanner.class, metadata, inits);
    }

    public QOrderBanner(Class<? extends OrderBanner> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new com.iozee.printadmin.model.QOrderItem(type, metadata, inits);
        this.amount = _super.amount;
        this.comment = _super.comment;
        this.cost = _super.cost;
        this.dateCreated = _super.dateCreated;
        this.deleted = _super.deleted;
        this.id = _super.id;
        this.material = inits.isInitialized("material") ? new com.iozee.printadmin.model.QMaterial(forProperty("material")) : null;
        this.order = _super.order;
        this.print = inits.isInitialized("print") ? new com.iozee.printadmin.model.QOrderPrintWork(forProperty("print")) : null;
        this.service = _super.service;
        this.status = _super.status;
        this.timeCreated = _super.timeCreated;
    }

}

