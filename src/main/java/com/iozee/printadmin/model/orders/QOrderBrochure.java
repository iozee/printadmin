package com.iozee.printadmin.model.orders;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.*;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QOrderBrochure is a Querydsl query type for OrderBrochure
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QOrderBrochure extends EntityPathBase<OrderBrochure> {

    private static final long serialVersionUID = 1514500065L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QOrderBrochure orderBrochure = new QOrderBrochure("orderBrochure");

    public final com.iozee.printadmin.model.QOrderItem _super;

    //inherited
    public final NumberPath<Integer> amount;

    public final com.iozee.printadmin.model.QMaterial blockMaterial;

    //inherited
    public final StringPath comment;

    //inherited
    public final NumberPath<java.math.BigDecimal> cost;

    public final com.iozee.printadmin.model.QOrderPrintWork coverBackBackPrint;

    public final com.iozee.printadmin.model.QOrderPrintWork coverBackFacePrint;

    public final com.iozee.printadmin.model.QLamination coverBackLamination;

    public final com.iozee.printadmin.model.QMaterial coverBackMaterial;

    public final com.iozee.printadmin.model.QOrderPrintWork coverBackPrint;

    public final com.iozee.printadmin.model.QOrderPrintWork coverFacePrint;

    public final com.iozee.printadmin.model.QLamination coverLamination;

    public final com.iozee.printadmin.model.QMaterial coverMaterial;

    //inherited
    public final DateTimePath<java.util.Date> dateCreated;

    //inherited
    public final BooleanPath deleted;

    public final NumberPath<Integer> height = createNumber("height", Integer.class);

    //inherited
    public final NumberPath<Integer> id;

    // inherited
    public final com.iozee.printadmin.model.QOrder order;

    public final NumberPath<Integer> pageCount = createNumber("pageCount", Integer.class);

    // inherited
    public final com.iozee.printadmin.model.QService service;

    // inherited
    public final com.iozee.printadmin.model.QOrderItemStatus status;

    //inherited
    public final DateTimePath<java.util.Date> timeCreated;

    public final NumberPath<Integer> width = createNumber("width", Integer.class);

    public QOrderBrochure(String variable) {
        this(OrderBrochure.class, forVariable(variable), INITS);
    }

    public QOrderBrochure(Path<? extends OrderBrochure> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QOrderBrochure(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QOrderBrochure(PathMetadata metadata, PathInits inits) {
        this(OrderBrochure.class, metadata, inits);
    }

    public QOrderBrochure(Class<? extends OrderBrochure> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new com.iozee.printadmin.model.QOrderItem(type, metadata, inits);
        this.amount = _super.amount;
        this.blockMaterial = inits.isInitialized("blockMaterial") ? new com.iozee.printadmin.model.QMaterial(forProperty("blockMaterial")) : null;
        this.comment = _super.comment;
        this.cost = _super.cost;
        this.coverBackBackPrint = inits.isInitialized("coverBackBackPrint") ? new com.iozee.printadmin.model.QOrderPrintWork(forProperty("coverBackBackPrint")) : null;
        this.coverBackFacePrint = inits.isInitialized("coverBackFacePrint") ? new com.iozee.printadmin.model.QOrderPrintWork(forProperty("coverBackFacePrint")) : null;
        this.coverBackLamination = inits.isInitialized("coverBackLamination") ? new com.iozee.printadmin.model.QLamination(forProperty("coverBackLamination")) : null;
        this.coverBackMaterial = inits.isInitialized("coverBackMaterial") ? new com.iozee.printadmin.model.QMaterial(forProperty("coverBackMaterial")) : null;
        this.coverBackPrint = inits.isInitialized("coverBackPrint") ? new com.iozee.printadmin.model.QOrderPrintWork(forProperty("coverBackPrint")) : null;
        this.coverFacePrint = inits.isInitialized("coverFacePrint") ? new com.iozee.printadmin.model.QOrderPrintWork(forProperty("coverFacePrint")) : null;
        this.coverLamination = inits.isInitialized("coverLamination") ? new com.iozee.printadmin.model.QLamination(forProperty("coverLamination")) : null;
        this.coverMaterial = inits.isInitialized("coverMaterial") ? new com.iozee.printadmin.model.QMaterial(forProperty("coverMaterial")) : null;
        this.dateCreated = _super.dateCreated;
        this.deleted = _super.deleted;
        this.id = _super.id;
        this.order = _super.order;
        this.service = _super.service;
        this.status = _super.status;
        this.timeCreated = _super.timeCreated;
    }

}

