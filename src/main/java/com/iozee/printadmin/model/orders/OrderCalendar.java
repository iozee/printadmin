package com.iozee.printadmin.model.orders;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import com.iozee.printadmin.model.Lamination;
import com.iozee.printadmin.model.Material;
import com.iozee.printadmin.model.OrderItem;
import com.iozee.printadmin.model.OrderPrintWork;

import javax.persistence.*;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "order_calendar")
@Access(AccessType.FIELD)
@PrimaryKeyJoinColumn(name = "id")
@Audited
@DynamicUpdate
@SelectBeforeUpdate
public class OrderCalendar extends OrderItem {

    @Column(name = "width", nullable = false)
    private Integer width;

    @Column(name = "height", nullable = false)
    private Integer height;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    @JoinColumn(name = "coverMaterial", nullable = false)
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private Material coverMaterial;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    @JoinColumn(name = "coverFacePrint", nullable = false)
    @Cascade(CascadeType.SAVE_UPDATE)
    private OrderPrintWork coverFacePrint;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    @JoinColumn(name = "coverBackPrint", nullable = false)
    @Cascade(CascadeType.SAVE_UPDATE)
    private OrderPrintWork coverBackPrint;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "coverLamination")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private Lamination coverLamination;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    @JoinColumn(name = "blockMaterial", nullable = false)
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private Material blockMaterial;

    @Column(name = "pageCount", nullable = false)
    private Integer pageCount;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    @JoinColumn(name = "coverBackMaterial", nullable = false)
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private Material coverBackMaterial;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    @JoinColumn(name = "coverBackFacePrint", nullable = false)
    @Cascade(CascadeType.SAVE_UPDATE)
    private OrderPrintWork coverBackFacePrint;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    @JoinColumn(name = "coverBackBackPrint", nullable = false)
    @Cascade(CascadeType.SAVE_UPDATE)
    private OrderPrintWork coverBackBackPrint;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "coverBackLamination")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private Lamination coverBackLamination;

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer rootWidth) {
        this.width = rootWidth;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer rootHeight) {
        this.height = rootHeight;
    }

    public Material getCoverMaterial() {
        return coverMaterial;
    }

    public void setCoverMaterial(Material coverMaterial) {
        this.coverMaterial = coverMaterial;
    }

    public OrderPrintWork getCoverFacePrint() {
        return coverFacePrint;
    }

    public void setCoverFacePrint(OrderPrintWork coverFacePrint) {
        this.coverFacePrint = coverFacePrint;
    }

    public OrderPrintWork getCoverBackPrint() {
        return coverBackPrint;
    }

    public void setCoverBackPrint(OrderPrintWork coverBackPrint) {
        this.coverBackPrint = coverBackPrint;
    }

    public Material getBlockMaterial() {
        return blockMaterial;
    }

    public void setBlockMaterial(Material blockMaterial) {
        this.blockMaterial = blockMaterial;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer blockPageCount) {
        this.pageCount = blockPageCount;
    }

    public Material getCoverBackMaterial() {
        return coverBackMaterial;
    }

    public void setCoverBackMaterial(Material coverBackMaterial) {
        this.coverBackMaterial = coverBackMaterial;
    }

    public OrderPrintWork getCoverBackFacePrint() {
        return coverBackFacePrint;
    }

    public void setCoverBackFacePrint(OrderPrintWork coverBackFacePrint) {
        this.coverBackFacePrint = coverBackFacePrint;
    }

    public OrderPrintWork getCoverBackBackPrint() {
        return coverBackBackPrint;
    }

    public void setCoverBackBackPrint(OrderPrintWork coverBackBackPrint) {
        this.coverBackBackPrint = coverBackBackPrint;
    }

    public Lamination getCoverLamination() {
        return coverLamination;
    }

    public void setCoverLamination(Lamination coverLamination) {
        this.coverLamination = coverLamination;
    }

    public Lamination getCoverBackLamination() {
        return coverBackLamination;
    }

    public void setCoverBackLamination(Lamination coverBackLamination) {
        this.coverBackLamination = coverBackLamination;
    }
}