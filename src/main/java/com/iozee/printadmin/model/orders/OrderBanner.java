package com.iozee.printadmin.model.orders;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import com.iozee.printadmin.model.Material;
import com.iozee.printadmin.model.OrderItem;
import com.iozee.printadmin.model.OrderPrintWork;

import javax.persistence.*;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "order_banner")
@Access(AccessType.FIELD)
@PrimaryKeyJoinColumn(name = "id")
@Audited
@DynamicUpdate
@SelectBeforeUpdate
public class OrderBanner extends OrderItem {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    @JoinColumn(name = "material", nullable = false)
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private Material material;

    @Column(name = "width", nullable = false)
    private Integer width;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "height", nullable = false)
    private Integer height;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    @JoinColumn(name = "print", nullable = false)
    @Cascade(CascadeType.SAVE_UPDATE)
    private OrderPrintWork print;

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public OrderPrintWork getPrint() {
        return print;
    }

    public void setPrint(OrderPrintWork print) {
        this.print = print;
    }
}
