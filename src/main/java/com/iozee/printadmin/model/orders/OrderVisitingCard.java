package com.iozee.printadmin.model.orders;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import com.iozee.printadmin.model.Material;
import com.iozee.printadmin.model.OrderItem;
import com.iozee.printadmin.model.OrderLaminationWork;
import com.iozee.printadmin.model.OrderPrintWork;

import javax.persistence.*;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author izerenev
 * Date: 25.12.2014
 */
@Entity
@Table(name = "order_visiting_card")
@Access(AccessType.FIELD)
@PrimaryKeyJoinColumn(name = "id")
@Audited
@DynamicUpdate
@SelectBeforeUpdate
public class OrderVisitingCard extends OrderItem {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    @JoinColumn(name = "material", nullable = false)
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private Material material;

    @Column(name = "width", nullable = false)
    private Integer width;

    @Column(name = "height", nullable = false)
    private Integer height;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    @JoinColumn(name = "facePrint", nullable = false)
    @Cascade(CascadeType.SAVE_UPDATE)
    private OrderPrintWork facePrint;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    @JoinColumn(name = "backPrint", nullable = false)
    @Cascade(CascadeType.SAVE_UPDATE)
    private OrderPrintWork backPrint;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @LazyToOne(LazyToOneOption.NO_PROXY)
    @JoinColumn(name = "lamination", nullable = false)
    @Cascade(CascadeType.SAVE_UPDATE)
    private OrderLaminationWork lamination;

    public OrderLaminationWork getLamination() {
        return lamination;
    }

    public void setLamination(OrderLaminationWork lamination) {
        this.lamination = lamination;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public OrderPrintWork getFacePrint() {
        return facePrint;
    }

    public void setFacePrint(OrderPrintWork facePrint) {
        this.facePrint = facePrint;
    }

    public OrderPrintWork getBackPrint() {
        return backPrint;
    }

    public void setBackPrint(OrderPrintWork backPrint) {
        this.backPrint = backPrint;
    }
}
