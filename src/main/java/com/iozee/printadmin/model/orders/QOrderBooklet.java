package com.iozee.printadmin.model.orders;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.*;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QOrderBooklet is a Querydsl query type for OrderBooklet
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QOrderBooklet extends EntityPathBase<OrderBooklet> {

    private static final long serialVersionUID = -1699358923L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QOrderBooklet orderBooklet = new QOrderBooklet("orderBooklet");

    public final com.iozee.printadmin.model.QOrderItem _super;

    //inherited
    public final NumberPath<Integer> amount;

    public final com.iozee.printadmin.model.QOrderPrintWork backPrint;

    //inherited
    public final StringPath comment;

    //inherited
    public final NumberPath<java.math.BigDecimal> cost;

    //inherited
    public final DateTimePath<java.util.Date> dateCreated;

    //inherited
    public final BooleanPath deleted;

    public final com.iozee.printadmin.model.QOrderPrintWork facePrint;

    public final NumberPath<Integer> height = createNumber("height", Integer.class);

    //inherited
    public final NumberPath<Integer> id;

    public final com.iozee.printadmin.model.QMaterial material;

    // inherited
    public final com.iozee.printadmin.model.QOrder order;

    // inherited
    public final com.iozee.printadmin.model.QService service;

    // inherited
    public final com.iozee.printadmin.model.QOrderItemStatus status;

    //inherited
    public final DateTimePath<java.util.Date> timeCreated;

    public final NumberPath<Integer> width = createNumber("width", Integer.class);

    public QOrderBooklet(String variable) {
        this(OrderBooklet.class, forVariable(variable), INITS);
    }

    public QOrderBooklet(Path<? extends OrderBooklet> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QOrderBooklet(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QOrderBooklet(PathMetadata metadata, PathInits inits) {
        this(OrderBooklet.class, metadata, inits);
    }

    public QOrderBooklet(Class<? extends OrderBooklet> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this._super = new com.iozee.printadmin.model.QOrderItem(type, metadata, inits);
        this.amount = _super.amount;
        this.backPrint = inits.isInitialized("backPrint") ? new com.iozee.printadmin.model.QOrderPrintWork(forProperty("backPrint")) : null;
        this.comment = _super.comment;
        this.cost = _super.cost;
        this.dateCreated = _super.dateCreated;
        this.deleted = _super.deleted;
        this.facePrint = inits.isInitialized("facePrint") ? new com.iozee.printadmin.model.QOrderPrintWork(forProperty("facePrint")) : null;
        this.id = _super.id;
        this.material = inits.isInitialized("material") ? new com.iozee.printadmin.model.QMaterial(forProperty("material")) : null;
        this.order = _super.order;
        this.service = _super.service;
        this.status = _super.status;
        this.timeCreated = _super.timeCreated;
    }

}

