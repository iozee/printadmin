package com.iozee.printadmin.model;

import org.hibernate.annotations.Formula;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;

@Entity
@Table(name = "client")
@Access(AccessType.FIELD)
@Audited
public class Client extends NamedCommonEntity {

    @Column(name = "phoneFirst")
    private String phoneFirst;

    @Column(name = "phoneSecond")
    private String phoneSecond;

    @Column(name = "mobileFirst")
    private String mobileFirst;

    @Column(name = "mobileSecond")
    private String mobileSecond;

    @Column(name = "email")
    private String email;

    @NotAudited
    @Formula("(SELECT COUNT(*) FROM orders WHERE orders.clientId = id)")
    private int ordersCount;

    public String getPhoneFirst() {
        return phoneFirst;
    }

    public void setPhoneFirst(String phoneFirst) {
        this.phoneFirst = phoneFirst;
    }

    public String getPhoneSecond() {
        return phoneSecond;
    }

    public void setPhoneSecond(String phoneSecond) {
        this.phoneSecond = phoneSecond;
    }

    public String getMobileFirst() {
        return mobileFirst;
    }

    public void setMobileFirst(String mobileFirst) {
        this.mobileFirst = mobileFirst;
    }

    public String getMobileSecond() {
        return mobileSecond;
    }

    public void setMobileSecond(String mobileSecond) {
        this.mobileSecond = mobileSecond;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getOrdersCount() {
        return ordersCount;
    }

}
