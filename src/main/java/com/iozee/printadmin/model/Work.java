package com.iozee.printadmin.model;

import javax.persistence.*;

/**
 * @author izerenev
 * Date: 16.09.2014
 */
@Entity
@Table(name = "service_work")
@Access(AccessType.FIELD)
public class Work extends NamedCommonEntity {

    @Column(name = "code")
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}