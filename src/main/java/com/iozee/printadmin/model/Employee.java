package com.iozee.printadmin.model;

import javax.persistence.*;

/**
 * @author izerenev
 * Date: 08.05.2014
 */
@Entity
@Table(name = "employee")
@Access(AccessType.FIELD)
public class Employee extends NamedCommonEntity {

    @Column(name = "phoneFirst")
    private String phoneFirst;

    @Column(name = "phoneSecond")
    private String phoneSecond;

    @Column(name = "phoneThird")
    private String phoneThird;

    @Column(name = "email")
    private String email;

    public String getPhoneFirst() {
        return phoneFirst;
    }

    public void setPhoneFirst(String phoneFirst) {
        this.phoneFirst = phoneFirst;
    }

    public String getPhoneSecond() {
        return phoneSecond;
    }

    public void setPhoneSecond(String phoneSecond) {
        this.phoneSecond = phoneSecond;
    }

    public String getPhoneThird() {
        return phoneThird;
    }

    public void setPhoneThird(String phoneThird) {
        this.phoneThird = phoneThird;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
