package com.iozee.printadmin.model;

import org.hibernate.annotations.Formula;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;

@Entity
@Table(name = "service")
@Access(AccessType.FIELD)
@Audited
public class Service extends NamedCommonEntity {

    @NotAudited
    @Formula("(SELECT COUNT(*) FROM order_item WHERE order_item.serviceId = id)")
    private int orderItemsCount;

    @Column(name = "code")
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getOrderItemsCount() {
        return orderItemsCount;
    }

}
