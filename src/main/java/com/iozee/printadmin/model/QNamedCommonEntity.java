package com.iozee.printadmin.model;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QNamedCommonEntity is a Querydsl query type for NamedCommonEntity
 */
@Generated("com.querydsl.codegen.SupertypeSerializer")
public class QNamedCommonEntity extends EntityPathBase<NamedCommonEntity> {

    private static final long serialVersionUID = -316552412L;

    public static final QNamedCommonEntity namedCommonEntity = new QNamedCommonEntity("namedCommonEntity");

    public final QCommonEntity _super = new QCommonEntity(this);

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public final StringPath name = createString("name");

    public QNamedCommonEntity(String variable) {
        super(NamedCommonEntity.class, forVariable(variable));
    }

    public QNamedCommonEntity(Path<? extends NamedCommonEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QNamedCommonEntity(PathMetadata metadata) {
        super(NamedCommonEntity.class, metadata);
    }

}

