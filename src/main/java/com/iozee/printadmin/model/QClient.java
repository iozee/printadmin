package com.iozee.printadmin.model;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QClient is a Querydsl query type for Client
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QClient extends EntityPathBase<Client> {

    private static final long serialVersionUID = -1882031197L;

    public static final QClient client = new QClient("client");

    public final QNamedCommonEntity _super = new QNamedCommonEntity(this);

    public final StringPath email = createString("email");

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public final StringPath mobileFirst = createString("mobileFirst");

    public final StringPath mobileSecond = createString("mobileSecond");

    //inherited
    public final StringPath name = _super.name;

    public final NumberPath<Integer> ordersCount = createNumber("ordersCount", Integer.class);

    public final StringPath phoneFirst = createString("phoneFirst");

    public final StringPath phoneSecond = createString("phoneSecond");

    public QClient(String variable) {
        super(Client.class, forVariable(variable));
    }

    public QClient(Path<? extends Client> path) {
        super(path.getType(), path.getMetadata());
    }

    public QClient(PathMetadata metadata) {
        super(Client.class, metadata);
    }

}

