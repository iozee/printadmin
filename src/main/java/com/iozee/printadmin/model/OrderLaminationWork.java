package com.iozee.printadmin.model;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;

@Entity
@Table(name = "order_lamination_work")
@Access(AccessType.FIELD)
@Audited
@DynamicUpdate
@SelectBeforeUpdate
public class OrderLaminationWork extends CommonEntity implements IWork {

    /**
     * двусторонняя ламинация
     */
    @Column(name = "isBothSide", nullable = false)
    private Boolean bothSide;

    /**
     * Ссылка на вид ламинации
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "lamination")
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private Lamination lamination;

    public Lamination getLamination() {
        return lamination;
    }

    public void setLamination(Lamination coverLamination) {
        this.lamination = coverLamination;
    }

    public Boolean getBothSide() {
        return bothSide;
    }

    public void setBothSide(Boolean isBothSide) {
        this.bothSide = isBothSide;
    }
}