package com.iozee.printadmin.model;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.*;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QUser is a Querydsl query type for User
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUser extends EntityPathBase<User> {

    private static final long serialVersionUID = -2073837298L;

    public static final QUser user = new QUser("user");

    public final QNamedCommonEntity _super = new QNamedCommonEntity(this);

    public final DatePath<java.util.Date> created = createDate("created", java.util.Date.class);

    public final BooleanPath enabled = createBoolean("enabled");

    //inherited
    public final NumberPath<Integer> id = _super.id;

    public final DatePath<java.util.Date> modified = createDate("modified", java.util.Date.class);

    //inherited
    public final StringPath name = _super.name;

    public final StringPath password = createString("password");

    public final SetPath<Role, QRole> roles = this.<Role, QRole>createSet("roles", Role.class, QRole.class, PathInits.DIRECT2);

    public QUser(String variable) {
        super(User.class, forVariable(variable));
    }

    public QUser(Path<? extends User> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUser(PathMetadata metadata) {
        super(User.class, metadata);
    }

}

