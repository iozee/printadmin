package com.iozee.printadmin.model;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.*;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QOrderItemStatusLink is a Querydsl query type for OrderItemStatusLink
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QOrderItemStatusLink extends EntityPathBase<OrderItemStatusLink> {

    private static final long serialVersionUID = -1070337899L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QOrderItemStatusLink orderItemStatusLink = new QOrderItemStatusLink("orderItemStatusLink");

    public final QNamedCommonEntity _super = new QNamedCommonEntity(this);

    //inherited
    public final NumberPath<Integer> id = _super.id;

    //inherited
    public final StringPath name = _super.name;

    public final QOrderItemStatus nextStatus;

    public final QOrderItemStatus orderItemStatus;

    public final SetPath<OrderItemStatusLinkSecurity, QOrderItemStatusLinkSecurity> orderItemStatusLinkSecurities = this.<OrderItemStatusLinkSecurity, QOrderItemStatusLinkSecurity>createSet("orderItemStatusLinkSecurities", OrderItemStatusLinkSecurity.class, QOrderItemStatusLinkSecurity.class, PathInits.DIRECT2);

    public QOrderItemStatusLink(String variable) {
        this(OrderItemStatusLink.class, forVariable(variable), INITS);
    }

    public QOrderItemStatusLink(Path<? extends OrderItemStatusLink> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QOrderItemStatusLink(PathMetadata metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QOrderItemStatusLink(PathMetadata metadata, PathInits inits) {
        this(OrderItemStatusLink.class, metadata, inits);
    }

    public QOrderItemStatusLink(Class<? extends OrderItemStatusLink> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.nextStatus = inits.isInitialized("nextStatus") ? new QOrderItemStatus(forProperty("nextStatus")) : null;
        this.orderItemStatus = inits.isInitialized("orderItemStatus") ? new QOrderItemStatus(forProperty("orderItemStatus")) : null;
    }

}

