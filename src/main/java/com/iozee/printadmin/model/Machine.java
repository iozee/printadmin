package com.iozee.printadmin.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author izerenev
 * Date: 08.05.2014
 */
@Entity
@Table(name = "machine")
@Access(AccessType.FIELD)
public class Machine extends NamedCommonEntity {
}
