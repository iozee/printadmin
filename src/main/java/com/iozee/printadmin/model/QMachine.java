package com.iozee.printadmin.model;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QMachine is a Querydsl query type for Machine
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMachine extends EntityPathBase<Machine> {

    private static final long serialVersionUID = 1751299951L;

    public static final QMachine machine = new QMachine("machine");

    public final QNamedCommonEntity _super = new QNamedCommonEntity(this);

    //inherited
    public final NumberPath<Integer> id = _super.id;

    //inherited
    public final StringPath name = _super.name;

    public QMachine(String variable) {
        super(Machine.class, forVariable(variable));
    }

    public QMachine(Path<? extends Machine> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMachine(PathMetadata metadata) {
        super(Machine.class, metadata);
    }

}

