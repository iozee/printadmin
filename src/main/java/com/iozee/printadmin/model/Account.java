package com.iozee.printadmin.model;

import javax.persistence.*;

/**
 * @author izerenev
 * Date: 03.09.2014
 */
@Entity
@Table(name = "account")
@Access(AccessType.FIELD)
public class Account extends NamedCommonEntity {

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
