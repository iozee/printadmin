package com.iozee.printadmin.model;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.*;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QOrderItemStatus is a Querydsl query type for OrderItemStatus
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QOrderItemStatus extends EntityPathBase<OrderItemStatus> {

    private static final long serialVersionUID = 1506230779L;

    public static final QOrderItemStatus orderItemStatus = new QOrderItemStatus("orderItemStatus");

    public final QNamedCommonEntity _super = new QNamedCommonEntity(this);

    public final StringPath code = createString("code");

    //inherited
    public final NumberPath<Integer> id = _super.id;

    //inherited
    public final StringPath name = _super.name;

    public final SetPath<OrderItemStatusLink, QOrderItemStatusLink> orderItemStatusLinks = this.<OrderItemStatusLink, QOrderItemStatusLink>createSet("orderItemStatusLinks", OrderItemStatusLink.class, QOrderItemStatusLink.class, PathInits.DIRECT2);

    public QOrderItemStatus(String variable) {
        super(OrderItemStatus.class, forVariable(variable));
    }

    public QOrderItemStatus(Path<? extends OrderItemStatus> path) {
        super(path.getType(), path.getMetadata());
    }

    public QOrderItemStatus(PathMetadata metadata) {
        super(OrderItemStatus.class, metadata);
    }

}

