package com.iozee.printadmin.model;

import org.hibernate.annotations.Immutable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author izerenev
 * Date: 13.10.2014
 */
@Entity
@Table(name = "lamination")
@Access(AccessType.FIELD)
@Immutable
public class Lamination extends NamedCommonEntity {
}