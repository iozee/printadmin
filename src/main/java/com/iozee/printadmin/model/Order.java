package com.iozee.printadmin.model;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SelectBeforeUpdate;
import com.iozee.printadmin.enums.OrderStatusEnum;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "orders")
@Access(AccessType.FIELD)

@DynamicUpdate
@SelectBeforeUpdate

// soft delete
@SQLDelete(sql = "UPDATE orders SET deleted = 1 WHERE id = ?")
public class Order extends CommonEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clientId", nullable = false)
    private Client client;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
    private Set<OrderItem> orderItems;

    @Formula("(SELECT COUNT(*) FROM order_item oi WHERE oi.orderId = id and oi.deleted = 0)")
    private int itemCount;

    @Column(name = "dateCreated", nullable = false)
    private Date dateCreated;

    @Column(name = "timeCreated", nullable = false)
    private Date timeCreated;

    @Formula("(SELECT IFNULL( (SELECT CEILING( ((SELECT COUNT(*)FROM order_item INNER JOIN order_item_status STATUS ON order_item.status = status.id WHERE order_item.orderId = id AND status.code IN ('COMPLETE', 'GIVEN_TO_CLIENT') AND order_item.deleted = 0 ) / (SELECT COUNT( * ) FROM order_item WHERE order_item.orderId = id AND order_item.deleted = 0)) * 100)), 0))")
    private byte readiness;

    @Transient
    private OrderStatusEnum status;

    @Formula("(SELECT IFNULL((SELECT SUM(oi.cost) FROM order_item oi WHERE oi.orderId = id AND oi.deleted = 0), 0))")
    private BigDecimal cost;

    @Column(name = "orderId", nullable = false)
    private String orderId;

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public void setTimeCreated(Date timeCreated) {
        this.timeCreated = timeCreated;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Set<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(Set<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public int getItemCount() {
        return itemCount;
    }

    public OrderStatusEnum getStatus() {
        if (readiness == 0) {
            status = OrderStatusEnum.NEW;
        } else if (readiness > 0 && readiness < 100) {
            status = OrderStatusEnum.IN_WORK;
        } else {
            status = OrderStatusEnum.COMPLETE;
        }
        return status;
    }

    public Date getTimeCreated() {
        return timeCreated;
    }

    public byte getReadiness() {
        return readiness;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
