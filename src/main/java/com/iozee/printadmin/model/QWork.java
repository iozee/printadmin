package com.iozee.printadmin.model;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QWork is a Querydsl query type for Work
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QWork extends EntityPathBase<Work> {

    private static final long serialVersionUID = -1368955671L;

    public static final QWork work = new QWork("work");

    public final QNamedCommonEntity _super = new QNamedCommonEntity(this);

    public final StringPath code = createString("code");

    //inherited
    public final NumberPath<Integer> id = _super.id;

    //inherited
    public final StringPath name = _super.name;

    public QWork(String variable) {
        super(Work.class, forVariable(variable));
    }

    public QWork(Path<? extends Work> path) {
        super(path.getType(), path.getMetadata());
    }

    public QWork(PathMetadata metadata) {
        super(Work.class, metadata);
    }

}

