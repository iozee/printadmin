package com.iozee.printadmin.view;

/**
 * @author izerenev
 * Date: 26.08.2014
 */
/*
@ManagedBean
@ViewScoped
*/
public class ServiceAttributeConstants {
    public static final String SERVICE_ATTR_SIZE = "size";
    public static final String SERVICE_ATTR_COLOR = "color";
    public static final String SERVICE_ATTR_FORMAT = "format";
    public static final String SERVICE_ATTR_DENSITY = "density";
    public static final String SERVICE_ATTR_RESOLUTION = "resolution";
    public static final String SERVICE_ATTR_MATERIAL = "material";
    public static final String SERVICE_ATTR_SYMBOL_HEIGHT = "averageSymbolHeight";
}
