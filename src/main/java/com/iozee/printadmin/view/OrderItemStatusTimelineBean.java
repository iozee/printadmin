package com.iozee.printadmin.view;

import org.primefaces.model.timeline.TimelineEvent;
import org.primefaces.model.timeline.TimelineModel;
import com.iozee.printadmin.MyBeansFactory;
import com.iozee.printadmin.model.OrderItem;
import com.iozee.printadmin.service.OrderItemService;
import com.iozee.printadmin.view.beans.TimelineEntry;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

@ManagedBean
@ViewScoped
public class OrderItemStatusTimelineBean implements Serializable {

    private OrderItem orderItem;

    private TimelineModel orderItemTimeline;

    private OrderItemService orderItemService;

    @PostConstruct
    private void init() {
        orderItemService = MyBeansFactory.getInstance().getOrderItemService();
    }

    public TimelineModel getOrderItemTimeline() {
        return orderItemTimeline;
    }

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
        initTimeline();
    }

    private void initTimeline() {
        if (orderItem == null) {
            return;
        }
        orderItemTimeline = new TimelineModel();
        List<TimelineEntry> timelineEntries = orderItemService.getTimelineEntriesByOrderItem(orderItem);
        timelineEntries.forEach(timelineEntry -> orderItemTimeline.add(new TimelineEvent(timelineEntry.getTitle(), timelineEntry.getDate())));
    }

}
