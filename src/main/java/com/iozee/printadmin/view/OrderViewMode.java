package com.iozee.printadmin.view;

/**
 * @author izerenev
 * Date: 27.02.2015
 */
public enum OrderViewMode {

    ORDER_LIST,

    ORDER

}
