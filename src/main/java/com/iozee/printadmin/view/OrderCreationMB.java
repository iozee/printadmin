package com.iozee.printadmin.view;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.timeline.TimelineSelectEvent;
import org.primefaces.model.timeline.TimelineEvent;
import org.primefaces.model.timeline.TimelineModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.iozee.printadmin.MyBeansFactory;
import com.iozee.printadmin.utils.ViewUtils;
import com.iozee.printadmin.enums.OrderItemStatusEnum;
import com.iozee.printadmin.enums.OrderStatusEnum;
import com.iozee.printadmin.model.*;
import com.iozee.printadmin.service.OrderCriteria;
import com.iozee.printadmin.service.OrderItemService;
import com.iozee.printadmin.service.OrderService;
import com.iozee.printadmin.service.orders.utils.OrderItemBuilderFactory;
import com.iozee.printadmin.service.orders.utils.OrderUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ManagedBean
@ViewScoped
public class OrderCreationMB implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(OrderCreationMB.class);

    private OrderService orderService;

    private OrderItemService orderItemService;

    private TimelineModel allOrdersTimeline;

    private int orderWizardStep;

    private Order order;

    private OrderItem orderItem;

    private List<OrderItem> orderItemList;

    private boolean orderUnionMode;

    private Client client;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @PostConstruct
    private void init() {
        orderService = MyBeansFactory.getInstance().getOrderService();
        orderItemService = MyBeansFactory.getInstance().getOrderItemService();
    }

    public void initForOrderCreation() {
        orderItemList = new ArrayList<OrderItem>();
        order = new Order();
        orderItem = null;
        orderWizardStep = 1;
    }

    /**
     * Init for adding positions to existing order
     */
    public void initForAddingOrderItem() {
        orderItemList = null;
        orderWizardStep = 1;
        orderItem = null;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }

    public Iterable<Material> getAllMaterials() {
        return orderService.getAllMaterials();
    }

    public Iterable<Lamination> getAllLaminations() {
        return orderService.getAllLaminations();
    }

    public List<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(List<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public int getOrderWizardStep() {
        if (orderWizardStep == 1 && order != null && order.getClient() != null) {
            orderWizardStep = 2;
        }
        return orderWizardStep;
    }

    public void wizardStep1() {
        orderWizardStep = 1;
    }

    public void wizardStep2() {
        orderWizardStep = 2;
    }

    /**
     * transition to step 3
     */
    public void wizardStep3(Service orderItemService) {
        orderItem = OrderItemBuilderFactory.createOrderItemByService(order, orderItemService);
        orderWizardStep = 3;
    }

    /**
     * transition to step 4
     */
    private void wizardStep4() {
        orderWizardStep = 4;
        orderItemList.add(orderItem);
    }

    public void orderCheckout() {
        wizardStep4();
    }

    public void cancelItemAddition() {
        throw new UnsupportedOperationException();
    }

    public void createOrder() {
        try {
            orderService.create(order, orderItemList);
            ViewUtils.showMessage("Заказ номер " + order.getOrderId() + " успешно создан");
            // todo RequestContext.getCurrentInstance().execute("PF('addOrderDialog').hide(); formEdited = false;");
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            ViewUtils.showError();
        }
    }

    public boolean isOrderEmpty() {
        return orderItemList == null || orderItemList.isEmpty();
    }

    public Iterable<OrderStatus> getAllOrderStatuses() {
        return orderService.getAllStatuses();
    }

    public TimelineModel getAllOrdersTimeline() {
        if (allOrdersTimeline == null) {
            allOrdersTimeline = new TimelineModel();
            List<Order> allOrders = orderService.getAllByCriteria(OrderCriteria.builder().build());
            allOrders.forEach(order -> allOrdersTimeline.add(new TimelineEvent(order.getOrderId(), order.getTimeCreated())));
        }
        return allOrdersTimeline;
    }

    public void resetOrderHistoryTimeline() {
        allOrdersTimeline = null;
    }

    public List<Order> getOrdersByClient(Client client) {
        return Collections.emptyList();
        // return orderService.getOrdersByClient(client);
    }

    public int getNewOrdersCount() {
        return orderService.getOrdersCountByStatus(OrderStatusEnum.NEW);
    }

    public int getCompleteOrdersCount() {
        return orderService.getOrdersCountByStatus(OrderStatusEnum.COMPLETE);
    }

    public void deleteOrder() {
        String orderId = order.getOrderId();
        orderService.delete(order);
        ViewUtils.showMessage("Заказ номер " + orderId + " успешно удален");
    }

    public List<OrderItem> getSortedOrderItems() {
        return OrderUtils.getSortedOrderItemList(order.getOrderItems());
    }

    public void updateOrderItem() {
        try {
            orderItemService.update(orderItem);
            ViewUtils.changesSavedMessage();
        } catch (Exception e) {
            ViewUtils.showError();
        }
    }

    public void addOrderItem() {
        orderItemService.createOrderItem(orderItem);
        //orderService.refreshOrderInSession(order);
        ViewUtils.showMessage("Позиция успешно добавлена к заказу " + order.getOrderId());
        // todo RequestContext.getCurrentInstance().execute("PF('addOrderDialog').hide(); formEdited = false;");
    }

    public void deleteOrderItem() {
        String orderItemServiceName = orderItem.getService().getName();
        orderItemService.delete(orderItem);
        String message = "Позиция " + orderItemServiceName + " удалена из заказа";
        if (order.getId() == 0) {
            orderItemList.remove(orderItem);
        } else {
            //orderService.refreshOrderInSession(order);
            message += " " + order.getOrderId();
        }
        ViewUtils.showMessage(message);
    }

    public OrderStatus getOrderStatusByCode(String code) {
        return orderService.getStatusByCode(code);
    }

    public void setOrderItemStatus(OrderItemStatusLink statusLink) {
        if (!orderItemService.isStatusChangeAllowed(statusLink)) {
            ViewUtils.showMessage("Нет прав для перевода в статус");
            return;
        }
        OrderItemStatus newStatus = statusLink.getNextStatus();
        orderItemService.setStatus(newStatus, orderItem);
        //orderService.refreshOrderInSession(order);
        if (newStatus.getCode().equals(OrderItemStatusEnum.COMPLETE.name())) {
            // todo RequestContext.getCurrentInstance().update("orderTableForm");
        }
        ViewUtils.showMessage("Статус позиции заказа изменен на \"" + newStatus.getName() + "\"");
    }

    public boolean isStatusChangeAllowed(OrderItemStatusLink statusLink) {
        return orderItemService.isStatusChangeAllowed(statusLink);
    }

    public void onHistoryEntrySelect(TimelineSelectEvent e) {
        order = orderService.getAllByCriteria(OrderCriteria.builder().orderId(e.getTimelineEvent().getData().toString()).build()).iterator().next();
    }

    public BigDecimal getTotalOrderCost() {
        BigDecimal result = BigDecimal.ZERO;
        getOrderItemList().forEach(orderItem -> result.add(orderItem.getCost())); // todo
        return result;
    }

    public boolean isOrderUnionMode() {
        return orderUnionMode;
    }

    public void setOrderUnionMode(boolean orderUnionMode) {
        this.orderUnionMode = orderUnionMode;
    }

    public void onClientSelect(SelectEvent event) {
        order.setClient((Client) event.getObject());
        ViewUtils.showMessage(event.getObject().toString());
    }
}