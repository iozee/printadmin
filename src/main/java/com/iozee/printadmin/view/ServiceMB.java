package com.iozee.printadmin.view;

import com.iozee.printadmin.MyBeansFactory;
import com.iozee.printadmin.model.Service;
import com.iozee.printadmin.service.ServiceService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

@ManagedBean
@ViewScoped
public class ServiceMB implements Serializable {

    private Service service;

    private ServiceService serviceService;

    @PostConstruct
    private void init() {
        serviceService = MyBeansFactory.getInstance().getServiceService();
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Iterable<Service> getAllServices() {
        return serviceService.getAllServices();
    }
}