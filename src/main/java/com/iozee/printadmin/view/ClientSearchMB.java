package com.iozee.printadmin.view;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import com.iozee.printadmin.MyBeansFactory;
import com.iozee.printadmin.utils.ViewUtils;
import com.iozee.printadmin.model.Client;
import com.iozee.printadmin.service.ClientCriteria;
import com.iozee.printadmin.service.ClientService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@ManagedBean
@ViewScoped
public class ClientSearchMB implements Serializable {

    private ClientService clientService;

    private LazyDataModel<Client> lazyDataModel;

    private ClientCriteria clientCriteria;

    @PostConstruct
    private void init() {
        clientService = MyBeansFactory.getInstance().getClientService();
        clientCriteria = new ClientCriteria();
        initDataModel();
    }

    public ClientCriteria getClientCriteria() {
        return clientCriteria;
    }

    public LazyDataModel<Client> getLazyDataModel() {
        return lazyDataModel;
    }

    private void initDataModel() {
        lazyDataModel = new LazyDataModel<Client>() {

            @Override
            public Client getRowData(String rowKey) {
                return clientService.getById(Integer.valueOf(rowKey));
            }

            @Override
            public List<Client> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                List<Client> result = clientService.getAllByCriteria(clientCriteria);
                lazyDataModel.setRowCount(clientService.getCountByCriteria(clientCriteria));
                return result;
            }
        };
    }

    public List<Client> completeClientByName(String name) {
        ClientCriteria clientCriteria = new ClientCriteria();
        clientCriteria.setName(name);
        return clientService.getAllByCriteria(clientCriteria);
    }

    public void onClientSelect(SelectEvent event) {
        ViewUtils.showMessage(event.getObject().toString());
    }
}
