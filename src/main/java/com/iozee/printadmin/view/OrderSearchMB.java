package com.iozee.printadmin.view;

import com.iozee.printadmin.MyBeansFactory;
import com.iozee.printadmin.dao.MySortOrder;
import com.iozee.printadmin.enums.ServiceEnum;
import com.iozee.printadmin.model.Order;
import com.iozee.printadmin.service.OrderCriteria;
import com.iozee.printadmin.service.OrderService;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@ManagedBean
@ViewScoped
public class OrderSearchMB implements Serializable {

    private OrderService orderService;

    private LazyDataModel<Order> lazyDataModel;

    private OrderCriteria orderCriteria;

    @PostConstruct
    private void init() {
        orderService = MyBeansFactory.getInstance().getOrderService();
        orderCriteria = OrderCriteria
                .builder()
                .limit(20)
                .offset(0)
                .sortField("dateCreated")
                .sortOrder(MySortOrder.DESCENDING)
                .build();
        initDataModel();
    }

    public LazyDataModel<Order> getLazyDataModel() {
        return lazyDataModel;
    }

    private void initDataModel() {
        lazyDataModel = new LazyDataModel<Order>() {
            @Override
            public Order getRowData(String rowKey) {
                return orderService.getById(Integer.valueOf(rowKey));
            }

            @Override
            public List<Order> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

                List<Order> result = orderService.getAllByCriteria(orderCriteria);
                lazyDataModel.setRowCount(orderService.getCountByCriteria(orderCriteria));
                return result;
            }
        };
    }

    public OrderCriteria getOrderCriteria() {
        return orderCriteria;
    }

    public String convertDate(Date date) {
        return new SimpleDateFormat("dd.MM.yyyy").format(date);
    }

    public boolean containsService(ServiceEnum serviceEnum) {
        return orderCriteria.containsService(serviceEnum);
    }

    public void removeService(ServiceEnum serviceEnum) {
        orderCriteria.removeService(serviceEnum);
    }

    public void resetSearch() {
        orderCriteria.clear();
    }
}
