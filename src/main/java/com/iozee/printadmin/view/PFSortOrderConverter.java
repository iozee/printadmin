package com.iozee.printadmin.view;

import org.primefaces.model.SortOrder;
import com.iozee.printadmin.dao.MySortOrder;

public class PFSortOrderConverter {

    public static MySortOrder getMySortOrder(SortOrder sortOrder) {
        switch (sortOrder) {
            case ASCENDING:
                return MySortOrder.ASCENDING;
            case DESCENDING:
                return MySortOrder.DESCENDING;
            case UNSORTED:
                return MySortOrder.UNSORTED;
        }
        throw new IllegalArgumentException("Unknown sort order " + sortOrder);
    }

}
