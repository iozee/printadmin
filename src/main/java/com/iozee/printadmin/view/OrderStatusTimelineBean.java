package com.iozee.printadmin.view;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.primefaces.model.timeline.TimelineEvent;
import org.primefaces.model.timeline.TimelineGroup;
import org.primefaces.model.timeline.TimelineModel;
import com.iozee.printadmin.MyBeansFactory;
import com.iozee.printadmin.model.Order;
import com.iozee.printadmin.service.OrderItemService;
import com.iozee.printadmin.view.beans.TimelineEntry;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@ManagedBean
@ViewScoped
public class OrderStatusTimelineBean implements Serializable {

    private Order order;

    private TimelineModel orderItemTimeline;

    private OrderItemService orderItemService;

    @PostConstruct
    private void init() {
        orderItemService = MyBeansFactory.getInstance().getOrderItemService();
    }

    public TimelineModel getOrderItemTimeline() {
        return orderItemTimeline;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
        initTimeline();
    }

    private void initTimeline() {
        if (order == null) {
            return;
        }
        orderItemTimeline = new TimelineModel();
        List<TimelineEntry> timelineEntries = orderItemService.getTimelineEntriesByOrder(order);

        Multimap<TimelineEntry, TimelineEntry> timelineEntryMap = ArrayListMultimap.create();

        timelineEntries.forEach(entry -> timelineEntryMap.get(entry).add(entry));

        for (Map.Entry<TimelineEntry, Collection<TimelineEntry>> entry : timelineEntryMap.asMap().entrySet()) {
            String groupId = String.valueOf(entry.getKey().getGroupId());
            TimelineGroup group = new TimelineGroup(groupId, entry.getKey().getGroup());
            orderItemTimeline.addGroup(group);
            for (TimelineEntry timelineEntry : entry.getValue()) {
                TimelineEvent event = new TimelineEvent(timelineEntry.getTitle(), timelineEntry.getDate());
                event.setGroup(groupId);
                orderItemTimeline.add(event);
            }
        }
    }

}
