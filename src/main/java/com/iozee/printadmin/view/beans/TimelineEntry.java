package com.iozee.printadmin.view.beans;

import java.util.Date;

public class TimelineEntry {

    private String title;

    private Date date;

    private String group;

    private int groupId;

    public TimelineEntry(String title, Date date) {
        this.title = title;
        this.date = date;
    }

    public TimelineEntry(String title, Date date, String group, int groupId) {
        this.title = title;
        this.date = date;
        this.group = group;
        this.groupId = groupId;
    }

    public String getTitle() {
        return title;
    }

    public Date getDate() {
        return date;
    }

    public String getGroup() {
        return group;
    }

    public int getGroupId() {
        return groupId;
    }
}
