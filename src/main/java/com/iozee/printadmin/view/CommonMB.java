package com.iozee.printadmin.view;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;

@ManagedBean
@ApplicationScoped
public class CommonMB implements Serializable {

    public String getWordForm(int number, String one, String two, String five) {
        String result = number + " ";
        number = number % 100;
        if (number > 20) {
            number = number % 10;
        }
        if (number > 4) {
            return result + five;
        } else {
            switch (number) {
                case 1:
                    return result + one;
                case 0:
                    return result + five;
                default:
                    return result + two;
            }
        }
    }

}
