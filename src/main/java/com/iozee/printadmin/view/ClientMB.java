package com.iozee.printadmin.view;

import com.iozee.printadmin.MyBeansFactory;
import com.iozee.printadmin.utils.ViewUtils;
import com.iozee.printadmin.model.Client;
import com.iozee.printadmin.service.ClientService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;

@ManagedBean
@ViewScoped
public class ClientMB implements Serializable {

    private Client client;

    private String mobileFirst;

    private ClientService clientService;

    @PostConstruct
    private void init() {
        clientService = MyBeansFactory.getInstance().getClientService();
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getMobileFirst() {
        return mobileFirst;
    }

    public void setMobileFirst(String mobileFirst) {
        this.mobileFirst = mobileFirst;
    }

    public Client getClient() {
        if (client == null) {
            resetEntity();
        }
        return client;
    }

    public void createClient() {
        if (client == null) {
            ViewUtils.showError();
            return;
        }
        clientService.create(client);
        // todo RequestContext.getCurrentInstance().execute("PF('addClientDialog').hide();");
        ViewUtils.showMessage("Клиент добавлен в базу данных");
    }

    public void updateClient() {
        if (client == null) {
            ViewUtils.showError();
            return;
        }
        clientService.update(client);
        ViewUtils.changesSavedMessage();
    }

    public void resetEntity() {
        client = new Client();
    }

    public void deleteClient() {
        if (client == null) {
            ViewUtils.showError();
            return;
        }
        clientService.delete(client);
        ViewUtils.showMessage("Информация о клиенте удалена");
    }

    public Iterable<Client> getAllClients() {
        return clientService.getAll();
    }
}
