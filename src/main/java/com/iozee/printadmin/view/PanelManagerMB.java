package com.iozee.printadmin.view;

import com.google.common.base.Strings;
import org.primefaces.event.SelectEvent;
import com.iozee.printadmin.utils.ViewUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author izerenev
 * Date: 24.06.2014
 * @ManagedBean
 * @ViewScoped
 */
@Deprecated
public class PanelManagerMB implements Serializable {

    private PanelManagerMB() {
        panels.put(PanelCode.ORDERS, new Panel(PanelCode.ORDERS));
        panels.put(PanelCode.CLIENTS, new Panel(PanelCode.CLIENTS));
        panels.put(PanelCode.SERVICES, new Panel(PanelCode.SERVICES));
    }

    private final Map<String, Panel> panels = new HashMap<String, Panel>();

    public interface PanelCode {
        String ORDERS = "ordersPanel";
        String CLIENTS = "clientsPanel";
        String SERVICES = "servicesPanel";
    }

    public interface PanelFormCode {
        interface OrderPanel {
            String ORDER = "orderForm";
        }
    }

    private class Panel {
        private Panel(String code) {
            this.code = code;
        }

        private final String code;

        private boolean isRendered;

        private boolean isVisible;

        private boolean isVisible() {
            return isVisible;
        }

        private boolean isRendered() {
            return isRendered;
        }

        public String getCode() {
            return code;
        }

        private Panel setRendered(boolean isRendered) {
            this.isRendered = isRendered;
            return this;
        }

        private void setVisible(boolean isVisible) {
            this.isVisible = isVisible;
        }
    }

    public boolean isPanelRendered(String code) {
        return panels.get(code).isRendered();
    }

    public boolean isPanelVisible(String code) {
        return panels.get(code).isVisible();
    }

    public void renderPanel(String code) {
        panels.get(code).setRendered(true);
    }

    public void hidePanel(String code) {
        panels.get(code).setVisible(false);
    }

    public void showPanel(String panelCode, String updateIds) {
        hideAllButTargetPanel(panelCode);
        updateAndShowPanel(panelCode, updateIds);
    }

    /**
     * hide all visible panels except this one
     *
     * @param panelCode panel code
     */
    private void hideAllButTargetPanel(String panelCode) {
        StringBuilder execute = new StringBuilder();
        panels.values()
                .stream()
                .filter(panel -> panel.isVisible() && !panel.getCode().equals(panelCode))
                .forEach(panel -> {
                    panel.setVisible(false);
                    execute.append("document.getElementById('").append(panel.getCode()).append("Inner').setAttribute('style', 'display:none !important');");
                });
        if (!execute.toString().equals("")) {
            // todo RequestContext.getCurrentInstance().execute(execute.toString());
        }
    }

    private void updateAndShowPanel(String panelCode, String updateIds) {
        Panel targetPanel = panels.get(panelCode);
        boolean wasRendered = targetPanel.isRendered();
        boolean wasVisible = targetPanel.isVisible();
        targetPanel.setRendered(true).setVisible(true);

        if (!wasRendered) {
            // if it's queried for the first time - render panel
            // todo RequestContext.getCurrentInstance().update(panelCode);
            ViewUtils.showMessage("Rendering " + panelCode);
        } else {
            // else just update the content
            if (!Strings.isNullOrEmpty(updateIds)) {
                // todo RequestContext.getCurrentInstance().update(updateIds);
                ViewUtils.showMessage("Updating " + updateIds);
            }
            if (!wasVisible) {
                // and showing it if it was hidden
                // todo RequestContext.getCurrentInstance().execute("document.getElementById('" + targetPanel.getCode() + "Inner').setAttribute('style', 'display:block !important');");
                //RequestContext.getCurrentInstance().execute("jQuery(\"#" + panelCode + " form.filter(\":hidden\").show();");
                ViewUtils.showMessage("and showing it");
            }
        }
    }

    private void updateAndShowPanel(String panelCode) {
        updateAndShowPanel(panelCode, null);
    }

    public void showOrderFromClientHistory(SelectEvent event) {
        updateAndShowPanel(PanelCode.ORDERS);
        // todo RequestContext.getCurrentInstance().execute("jQuery(\"#" + PanelCode.ORDERS + " form:not(#orderForm)\").hide();");
        //showExternalDialog(PanelCode.ORDERS, ":orderForm:orderDialog");
        ViewUtils.showMessage("selected");
    }

    private void showExternalDialog(String panelCode, String dialogId) {
        // todo RequestContext.getCurrentInstance().update(dialogId);
        String[] parts = dialogId.split(":");
        // todo RequestContext.getCurrentInstance().execute("jQuery(\"#" + panelCode + " form:not(#" + parts[1] + ")\").filter(\":visible\").hide();");
        // todo RequestContext.getCurrentInstance().execute("PF(\"" + parts[2] + "\").show()");
    }
}
