package com.iozee.printadmin.view;

import org.springframework.security.core.context.SecurityContextHolder;
import com.iozee.printadmin.security.SecurityUtils;
import com.iozee.printadmin.utils.ViewUtils;
import com.iozee.printadmin.model.User;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

@ManagedBean
@ViewScoped
public class UserMB implements Serializable {

    private String username;

    private String password;

    private static final int idleTimeout = 1000 * 60 * 30; // 30 min

    private final int countdown = 120;  // 120 sec

    public User getCurrentUser() {
        return SecurityUtils.getCurrentUser();
    }

    public int getIdleTimeout() {
        return idleTimeout;
    }

    public int getCountdown() {
        return countdown;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void logout() {
        SecurityContextHolder.clearContext();
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("login");
        } catch (IOException e) {
            ViewUtils.showError();
        }
    }

    public void onIdle() {
        String script = "PF('idleTimerKnob').setValue(" + countdown + ");" +
                "PF('idleTimerDialog').show();" +
                "PF('idleTimer').start();";
        ViewUtils.executeJavaScript(script);
    }

    public void onActive() {
        String script = "PF('idleTimerKnob').setValue(" + countdown + ");" +
                "PF('idleTimerDialog').hide();" +
                "PF('idleTimer').stop(true);";
        ViewUtils.executeJavaScript(script);
    }

    public String getErrorCode() {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, Object> requestMap = context.getExternalContext().getRequestMap();
        Integer errorCode = (Integer) requestMap.get("javax.servlet.error.status_code");
        if (errorCode != null) {
            String errorText = String.valueOf(errorCode);
            Object exception = requestMap.get("javax.servlet.error.exception");
            if (exception != null) {
                errorText += ": " + ((Exception) exception).getMessage();
            }
            return errorText;
        }
        return null;
    }
}
