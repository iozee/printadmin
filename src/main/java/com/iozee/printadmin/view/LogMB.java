package com.iozee.printadmin.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.iozee.printadmin.IozeeApplication;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author izerenev
 * Date: 04.09.2014
 */
@ManagedBean
@RequestScoped
public class LogMB {

    private static final Logger LOG = LoggerFactory.getLogger(LogMB.class);

    private List<String> logList;

    private int lastTimeEntryCount;

    private int newEntryCount;

    @PostConstruct
    private void updateLog() {
        try {
            logList = new ArrayList<String>();
            File file = getLogFile();
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                if (!"".equals(line)) {
                    logList.add(line);
                }
            }
            if (logList.isEmpty()) {
                return;
            }
            br.close();
            newEntryCount = logList.size() - lastTimeEntryCount;
            lastTimeEntryCount = logList.size();
            int fromIndex = logList.size() <= 20 ? logList.size() : 20;
            logList = new ArrayList<String>(logList.subList(logList.size() - fromIndex, logList.size()));
            Collections.reverse(logList);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    public void clearLog() throws FileNotFoundException {
        logList = new ArrayList<String>();
        lastTimeEntryCount = 0;
        newEntryCount = 0;
        PrintWriter writer = new PrintWriter(getLogFile());
        writer.print("");
        writer.close();
    }

    private File getLogFile() {
        return new File(IozeeApplication.getServletContext().getRealPath("WEB-INF/logs/boneCP.log"));
    }

    public List<String> getLogList() {
        return logList;
    }

    public int getNewEntryCount() {
        return newEntryCount;
    }
}
