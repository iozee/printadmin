package com.iozee.printadmin.view.converters;

import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.iozee.printadmin.view.AbstractConverter;
import com.iozee.printadmin.model.Lamination;
import com.iozee.printadmin.service.OrderService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

@Component
public class LaminationConverter extends AbstractConverter {

    @Autowired
    private OrderService service;

    public Object getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (Strings.isNullOrEmpty(submittedValue)) {
            return null;
        }
        return service.getEntityById(Integer.parseInt(submittedValue), Lamination.class);
    }
}
