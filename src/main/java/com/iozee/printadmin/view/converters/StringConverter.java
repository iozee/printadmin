package com.iozee.printadmin.view.converters;

import org.springframework.stereotype.Component;
import com.iozee.printadmin.view.AbstractConverter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

@Component
public class StringConverter extends AbstractConverter {

    public Object getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue == null || submittedValue.equals("")) {
            return null;
        }
        return submittedValue;
    }

}
