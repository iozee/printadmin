package com.iozee.printadmin.view.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.iozee.printadmin.view.AbstractConverter;
import com.iozee.printadmin.service.OrderItemService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

@Component
public class OrderItemStatusConverter extends AbstractConverter {

    @Autowired
    private OrderItemService orderItemService;

    public Object getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue == null || submittedValue.equals("")) {
            return null;
        }
        return orderItemService.getItemStatusById(Integer.parseInt(submittedValue));
    }

}
