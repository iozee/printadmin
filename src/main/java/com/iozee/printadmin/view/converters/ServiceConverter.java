package com.iozee.printadmin.view.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.iozee.printadmin.view.AbstractConverter;
import com.iozee.printadmin.service.ServiceService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

@Component
public class ServiceConverter extends AbstractConverter {

    @Autowired
    private ServiceService service;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        if (s == null || s.equals("")) {
            return null;
        }
        return service.getById(Integer.parseInt(s));
    }
}
