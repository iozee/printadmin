package com.iozee.printadmin.view.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.iozee.printadmin.view.AbstractConverter;
import com.iozee.printadmin.model.Material;
import com.iozee.printadmin.service.OrderService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

@Component
public class MaterialConverter extends AbstractConverter {

    @Autowired
    private OrderService orderService;

    public Object getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue == null || submittedValue.equals("")) {
            return null;
        }
        return orderService.getEntityById(Integer.parseInt(submittedValue), Material.class);
    }
}
