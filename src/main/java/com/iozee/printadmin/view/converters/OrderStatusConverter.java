package com.iozee.printadmin.view.converters;

import com.google.common.base.Strings;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.iozee.printadmin.enums.OrderStatusEnum;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * @author izerenev
 * Date: 18.03.2014
 */
@Component
@Scope(value = "session")
public class OrderStatusConverter implements Converter {

    public Object getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (Strings.isNullOrEmpty(submittedValue)) {
            return null;
        }
        return OrderStatusEnum.valueOf(submittedValue);
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object value) {
        if (value == null || value.equals("")) {
            return "";
        } else {
            return value.toString();
        }
    }

}
