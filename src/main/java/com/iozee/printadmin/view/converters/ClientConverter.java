package com.iozee.printadmin.view.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.iozee.printadmin.view.AbstractConverter;
import com.iozee.printadmin.service.ClientService;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

@Component
public class ClientConverter extends AbstractConverter {

    @Autowired
    private ClientService service;

    public Object getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue == null || submittedValue.equals("")) {
            return null;
        }
        return service.getById(Integer.parseInt(submittedValue));
    }
}
