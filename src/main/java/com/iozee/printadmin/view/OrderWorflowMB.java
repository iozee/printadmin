package com.iozee.printadmin.view;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.TreeNode;
import com.iozee.printadmin.utils.ViewUtils;
import com.iozee.printadmin.model.OrderStatus;
import com.iozee.printadmin.service.OrderService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import java.io.Serializable;

@ManagedBean
@ViewScoped
public class OrderWorflowMB implements Serializable {

    @Inject
    private OrderService orderService;

    private TreeNode root;

    private TreeNode selectedNode;

    public OrderService getOrderService() {
        return orderService;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    private void populate(TreeNode parent, OrderStatus status) {
    /*Set<OrderStatus> nextStatuses = status.getNextStatuses();
    for (OrderStatus nextStatus : nextStatuses) {
      TreeNode node = new DefaultTreeNode(nextStatus.getName(), parent);
      node.setExpanded(true);
      node.setRowKey(nextStatus.getCode());
      populate(node, nextStatus);
    } */
    }

    public TreeNode getRoot() {
        return root;
    }

    public TreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(TreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }

    public void onNodeSelect(NodeSelectEvent event) {
        TreeNode node = event.getTreeNode();
        ViewUtils.showMessage(orderService.getStatusByCode(node.getRowKey().replace("_0", "")).getName());
    }
}
