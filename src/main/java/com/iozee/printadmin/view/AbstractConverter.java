package com.iozee.printadmin.view;

import com.iozee.printadmin.model.CommonEntity;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

public abstract class AbstractConverter implements Converter {
    @Override
    public final String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        if (value == null || value.equals("")) {
            return "";
        } else {
            return String.valueOf(((CommonEntity) value).getId());
        }
    }
}
