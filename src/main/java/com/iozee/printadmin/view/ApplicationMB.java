package com.iozee.printadmin.view;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import java.util.Date;

@ManagedBean
@ApplicationScoped
public class ApplicationMB {

    private long startupDate;

    private ApplicationMB() {
        startupDate = new Date().getTime();
    }

    public long getStartupDate() {
        return startupDate;
    }
}
