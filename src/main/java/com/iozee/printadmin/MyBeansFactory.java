package com.iozee.printadmin;

import lombok.Getter;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import com.iozee.printadmin.service.ClientService;
import com.iozee.printadmin.service.OrderItemService;
import com.iozee.printadmin.service.OrderService;
import com.iozee.printadmin.service.ServiceService;

/**
 * Needed for @Autowired injections to work with WELD
 */
@Getter
public class MyBeansFactory {

    public MyBeansFactory() {
    }

    public static MyBeansFactory getInstance() {
        return IozeeApplication.getApplicationContext().getBean(MyBeansFactory.class);
    }

    @Autowired
    @Lazy
    private OrderService orderService;

    @Autowired
    @Lazy
    private OrderItemService orderItemService;

    @Autowired
    @Lazy
    private ClientService clientService;

    @Autowired
    @Lazy
    private ServiceService serviceService;

    @Autowired
    @Lazy
    private SessionFactory sessionFactory;

}
