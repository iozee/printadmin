package com.iozee.printadmin;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

public class IozeeApplication implements ApplicationContextAware, ServletContextAware {

    private static ApplicationContext applicationContext;

    private static ServletContext servletContext;

    private IozeeApplication() {
        //Locale.setDefault(new Locale("ru"));
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        IozeeApplication.applicationContext = applicationContext;
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static ServletContext getServletContext() {
        return servletContext;
    }

    @Override
    public void setServletContext(ServletContext servletContext) {
        IozeeApplication.servletContext = servletContext;
    }

    public static Object getBean(String beanName) {
        return applicationContext.getBean(beanName);
    }

    public static HttpSession getSession() {
        return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getSession(true);
    }
}
