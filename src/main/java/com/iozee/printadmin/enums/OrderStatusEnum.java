package com.iozee.printadmin.enums;

/**
 * @author izerenev
 * Date: 21.10.2014
 */
public enum OrderStatusEnum {

    NEW("Новый"),

    IN_WORK("В работе"),

    COMPLETE("Выполнен");

    String name;

    OrderStatusEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
