package com.iozee.printadmin.enums;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author izerenev
 * Date: 06.10.2014
 */
public enum ProductPartEnum {

    ROOT,

    COVER,

    BLOCK,

    COVER_BACK;

    public static Optional<ProductPartEnum> getByValue(String value) {
        return Arrays.stream(ProductPartEnum.values())
                .filter(type -> type.name().equalsIgnoreCase(value))
                .findFirst();
    }
}
