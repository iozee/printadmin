package com.iozee.printadmin.enums;

public enum OrderItemStatusEnum {

    NEW,

    COMPLETE,

    GIVEN_TO_CLIENT;

}
