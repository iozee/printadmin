package com.iozee.printadmin.enums;

/**
 * @author izerenev
 * Date: 09.10.2014
 */
public enum PrintOptionEnum {

    NO_PRINT("без печати"),

    FULL_COLOR("полноцвет"),

    B_W("черно-белая"),

    CMYK("свои настройки");

    private final String name;

    PrintOptionEnum(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }
}
