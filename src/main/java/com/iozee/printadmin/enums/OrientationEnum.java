package com.iozee.printadmin.enums;

/**
 * @author izerenev
 * Date: 23.09.2014
 */
public enum OrientationEnum {
    NA(), LANDSCAPE("Альбомная"), PORTRAIT("Портретная");

    private String name;

    OrientationEnum(String name) {
        this.name = name;
    }

    OrientationEnum() {
    }

    public String getName() {
        return name;
    }
}
