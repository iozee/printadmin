package com.iozee.printadmin.enums;

/**
 * @author izerenev
 * Date: 07.10.2014
 */
public enum ServiceEnum implements IService {

    NOTEBOOK("Блокнот"),

    BANNER("Баннер"),

    VISITING_CARD("Визитка"),

    BROCHURE("Брошюра"),

    LEAFLET("Лифлет"),

    BOOKLET("Буклет"),

    CALENDAR("Календарь"),

    STICKER("Наклейка");

    private final String name;

    ServiceEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
