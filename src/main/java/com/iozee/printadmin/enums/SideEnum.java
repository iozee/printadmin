package com.iozee.printadmin.enums;

/**
 * @author izerenev
 * Date: 02.10.2014
 */
public enum SideEnum {

    FACE, BACK

}
