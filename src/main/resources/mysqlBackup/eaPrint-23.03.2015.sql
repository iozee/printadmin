-- MySQL dump 10.13  Distrib 5.6.12, for Win64 (x86_64)
--
-- Host: localhost    Database: eaPrint
-- ------------------------------------------------------
-- Server version	5.6.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='аккаунты';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'starter',1);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `phoneFirst` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `phoneSecond` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `mobileFirst` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `mobileSecond` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ordersCount` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (1,'Карлос','8-888-8888888',NULL,'8-888-844',NULL,'sdfsdfsdf@ma.ru',2),(2,'Тоже Валерка','',NULL,'+7 (904) 663-22-81',NULL,'',2),(3,'Тот еще Федор','',NULL,'',NULL,'',2),(9,'Павлик','',NULL,'',NULL,'',1),(11,'Генацвали','8 (342) 423-32-33',NULL,'+7 (882) 233-12-21',NULL,'',0),(12,'Перчинка','',NULL,'+7 (878) 362-25-53',NULL,'',1),(14,'Петров','',NULL,'',NULL,'',0),(15,'Васечкин','',NULL,'',NULL,'',0),(16,'Валентин Федорович','',NULL,'',NULL,'',0),(17,'Генка','',NULL,'+7 (445) 555-54-33',NULL,'',0),(18,'Винтыч','',NULL,'',NULL,'',0),(19,'Базилио','',NULL,'',NULL,'',0),(20,'Мухомор','8 (455) 734-75-46',NULL,'+7 (546) 456-45-54',NULL,'gdfg@rwerw.ru',0),(21,'Чумачечий','',NULL,'',NULL,'',0),(22,'Вазелинчиков','',NULL,'',NULL,'',1),(23,'Валентин','',NULL,'+7 (553) 581-31-89',NULL,'',0),(26,'Тереб','',NULL,'',NULL,'',0),(27,'Акакий','8 (534) 534-64-36',NULL,'+7 (345) 332-64-57',NULL,'34sdf@fghgfh.ru',0),(29,'Жозефина','8 (234) 355-64-56',NULL,'+7 (402) 321-23-21',NULL,'doe3243rer@f44f.ru',0),(41,'Филармон','8 (324) 322-34-55',NULL,'+7 (989) 223-43-24',NULL,'doerer@ff.ru',0),(42,'Тетка с бородой','',NULL,'',NULL,'',0);
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_aud`
--

DROP TABLE IF EXISTS `client_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `REVEND` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `mobileFirst` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `mobileSecond` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `phoneFirst` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `phoneSecond` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FK_603b04k7860dht5yha79snlm8` (`REV`),
  KEY `FK_tls4tophmmvf2d50miv5ndxt8` (`REVEND`),
  CONSTRAINT `FK_603b04k7860dht5yha79snlm8` FOREIGN KEY (`REV`) REFERENCES `envers_custom` (`id`),
  CONSTRAINT `FK_tls4tophmmvf2d50miv5ndxt8` FOREIGN KEY (`REVEND`) REFERENCES `envers_custom` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_aud`
--

LOCK TABLES `client_aud` WRITE;
/*!40000 ALTER TABLE `client_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_source`
--

DROP TABLE IF EXISTS `client_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_source` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='источник, откуда клиент узнал о типографии';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_source`
--

LOCK TABLES `client_source` WRITE;
/*!40000 ALTER TABLE `client_source` DISABLE KEYS */;
/*!40000 ALTER TABLE `client_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `phoneFirst` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `phoneSecond` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `phoneThird` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='сотрудник типографии';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `envers_custom`
--

DROP TABLE IF EXISTS `envers_custom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `envers_custom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` bigint(20) NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `envers_custom`
--

LOCK TABLES `envers_custom` WRITE;
/*!40000 ALTER TABLE `envers_custom` DISABLE KEYS */;
INSERT INTO `envers_custom` VALUES (2,1426542333053,1),(3,1426542371048,1),(4,1426542448821,1),(5,1426627247541,1),(6,1426627258542,1),(7,1426716499501,1),(8,1426716506824,1),(9,1426716531246,1),(10,1427056964395,1),(11,1427056969983,1);
/*!40000 ALTER TABLE `envers_custom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lamination`
--

DROP TABLE IF EXISTS `lamination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lamination` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `price` decimal(10,2) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='виды ламинации';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lamination`
--

LOCK TABLES `lamination` WRITE;
/*!40000 ALTER TABLE `lamination` DISABLE KEYS */;
INSERT INTO `lamination` VALUES (1,'глянец',3.00),(2,'мат',4.00);
/*!40000 ALTER TABLE `lamination` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `machine`
--

DROP TABLE IF EXISTS `machine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `machine` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `work` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='оборудование';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `machine`
--

LOCK TABLES `machine` WRITE;
/*!40000 ALTER TABLE `machine` DISABLE KEYS */;
INSERT INTO `machine` VALUES (1,'принтер',1),(2,'резчик',NULL),(3,'ламинатор',3),(4,'лаковщик',2);
/*!40000 ALTER TABLE `machine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `material`
--

DROP TABLE IF EXISTS `material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `material` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='все материалы';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `material`
--

LOCK TABLES `material` WRITE;
/*!40000 ALTER TABLE `material` DISABLE KEYS */;
INSERT INTO `material` VALUES (1,'бумажка 5х3',1.00,5,3),(2,'бумажка 4х3',2.00,4,3);
/*!40000 ALTER TABLE `material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_amount_level`
--

DROP TABLE IF EXISTS `order_amount_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_amount_level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service` int(10) unsigned NOT NULL,
  `amount` int(10) unsigned NOT NULL,
  `price` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `service` (`service`),
  CONSTRAINT `FK_49c4fl0xtp1ag13xoieapkv07` FOREIGN KEY (`service`) REFERENCES `service` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_amount_level`
--

LOCK TABLES `order_amount_level` WRITE;
/*!40000 ALTER TABLE `order_amount_level` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_amount_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_banner`
--

DROP TABLE IF EXISTS `order_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_banner` (
  `id` int(10) unsigned NOT NULL,
  `material` int(10) unsigned NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `print` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_qoe6dhffak6lj3kfv5ay5jgf9` (`print`),
  KEY `FK_5tscd3m0yxqcb6wqg9cktdy5o` (`material`),
  CONSTRAINT `FK_5tscd3m0yxqcb6wqg9cktdy5o` FOREIGN KEY (`material`) REFERENCES `material` (`id`),
  CONSTRAINT `FK_aw3j65l7x3rpcaid3qieu3omi` FOREIGN KEY (`id`) REFERENCES `order_item` (`id`),
  CONSTRAINT `FK_qoe6dhffak6lj3kfv5ay5jgf9` FOREIGN KEY (`print`) REFERENCES `order_print_work` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_banner`
--

LOCK TABLES `order_banner` WRITE;
/*!40000 ALTER TABLE `order_banner` DISABLE KEYS */;
INSERT INTO `order_banner` VALUES (45,2,44,43,41),(46,2,4,4,42),(47,2,4,4,43),(48,2,4,4,44),(49,2,7,7,45),(77,1,1,1,97);
/*!40000 ALTER TABLE `order_banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_banner_aud`
--

DROP TABLE IF EXISTS `order_banner_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_banner_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `material` int(11) DEFAULT NULL,
  `print` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_banner_aud`
--

LOCK TABLES `order_banner_aud` WRITE;
/*!40000 ALTER TABLE `order_banner_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_banner_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_booklet`
--

DROP TABLE IF EXISTS `order_booklet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_booklet` (
  `id` int(10) unsigned NOT NULL,
  `material` int(10) unsigned NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `facePrint` int(10) unsigned NOT NULL,
  `backPrint` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_nss4c6cvhtsvh3cr2f76qac0g` (`backPrint`),
  UNIQUE KEY `UK_n5591xyadh2d00s9tt6tlxtgc` (`facePrint`),
  KEY `FK_8kgo5j8mct9ft0i5usnpvdkx6` (`material`),
  CONSTRAINT `FK_8kgo5j8mct9ft0i5usnpvdkx6` FOREIGN KEY (`material`) REFERENCES `material` (`id`),
  CONSTRAINT `FK_k1o9gdfqk3oemkxmi3l4cnsf5` FOREIGN KEY (`id`) REFERENCES `order_item` (`id`),
  CONSTRAINT `FK_n5591xyadh2d00s9tt6tlxtgc` FOREIGN KEY (`facePrint`) REFERENCES `order_print_work` (`id`),
  CONSTRAINT `FK_nss4c6cvhtsvh3cr2f76qac0g` FOREIGN KEY (`backPrint`) REFERENCES `order_print_work` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_booklet`
--

LOCK TABLES `order_booklet` WRITE;
/*!40000 ALTER TABLE `order_booklet` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_booklet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_booklet_aud`
--

DROP TABLE IF EXISTS `order_booklet_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_booklet_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `backPrint` int(11) DEFAULT NULL,
  `facePrint` int(11) DEFAULT NULL,
  `material` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_booklet_aud`
--

LOCK TABLES `order_booklet_aud` WRITE;
/*!40000 ALTER TABLE `order_booklet_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_booklet_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_brochure`
--

DROP TABLE IF EXISTS `order_brochure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_brochure` (
  `id` int(10) unsigned NOT NULL,
  `width` int(10) unsigned DEFAULT NULL,
  `height` int(10) unsigned DEFAULT NULL,
  `coverMaterial` int(10) unsigned NOT NULL,
  `coverFacePrint` int(10) unsigned DEFAULT NULL,
  `coverBackPrint` int(10) unsigned DEFAULT NULL,
  `coverLamination` int(10) unsigned DEFAULT NULL,
  `blockMaterial` int(10) unsigned NOT NULL,
  `pageCount` int(10) unsigned NOT NULL,
  `coverBackMaterial` int(10) unsigned NOT NULL,
  `coverBackFacePrint` int(10) unsigned DEFAULT NULL,
  `coverBackBackPrint` int(10) unsigned DEFAULT NULL,
  `coverBackLamination` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_su8krcdg77ayh74g1v2ik6djg` (`coverBackBackPrint`),
  UNIQUE KEY `UK_mcac35w08fr2sroijhl0eadv1` (`coverBackFacePrint`),
  UNIQUE KEY `UK_7253tn9h3ci8neovixcef0p0l` (`coverBackPrint`),
  UNIQUE KEY `UK_tq3kxmdyw52agg3stys4g87m0` (`coverFacePrint`),
  KEY `FK_6unlie0wfcp6tke151c6x8a24` (`blockMaterial`),
  KEY `FK_f2fy60t95sj6s4kktmfk68jih` (`coverBackLamination`),
  KEY `FK_sckj81qm00ul4l9p23vscrtib` (`coverBackMaterial`),
  KEY `FK_jqju0r05ljdum564oh8fn1qfn` (`coverLamination`),
  KEY `FK_b7b4tloc1cs16huwiiux0a4kr` (`coverMaterial`),
  CONSTRAINT `FK_6unlie0wfcp6tke151c6x8a24` FOREIGN KEY (`blockMaterial`) REFERENCES `material` (`id`),
  CONSTRAINT `FK_7253tn9h3ci8neovixcef0p0l` FOREIGN KEY (`coverBackPrint`) REFERENCES `order_print_work` (`id`),
  CONSTRAINT `FK_b7b4tloc1cs16huwiiux0a4kr` FOREIGN KEY (`coverMaterial`) REFERENCES `material` (`id`),
  CONSTRAINT `FK_f2fy60t95sj6s4kktmfk68jih` FOREIGN KEY (`coverBackLamination`) REFERENCES `lamination` (`id`),
  CONSTRAINT `FK_jqju0r05ljdum564oh8fn1qfn` FOREIGN KEY (`coverLamination`) REFERENCES `lamination` (`id`),
  CONSTRAINT `FK_mcac35w08fr2sroijhl0eadv1` FOREIGN KEY (`coverBackFacePrint`) REFERENCES `order_print_work` (`id`),
  CONSTRAINT `FK_moodbto8imvby78fpcs4n3jj8` FOREIGN KEY (`id`) REFERENCES `order_item` (`id`),
  CONSTRAINT `FK_sckj81qm00ul4l9p23vscrtib` FOREIGN KEY (`coverBackMaterial`) REFERENCES `material` (`id`),
  CONSTRAINT `FK_su8krcdg77ayh74g1v2ik6djg` FOREIGN KEY (`coverBackBackPrint`) REFERENCES `order_print_work` (`id`),
  CONSTRAINT `FK_tq3kxmdyw52agg3stys4g87m0` FOREIGN KEY (`coverFacePrint`) REFERENCES `order_print_work` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_brochure`
--

LOCK TABLES `order_brochure` WRITE;
/*!40000 ALTER TABLE `order_brochure` DISABLE KEYS */;
INSERT INTO `order_brochure` VALUES (51,6,6,2,51,50,NULL,1,66,1,49,48,NULL);
/*!40000 ALTER TABLE `order_brochure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_brochure_aud`
--

DROP TABLE IF EXISTS `order_brochure_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_brochure_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `pageCount` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `blockMaterial` int(11) DEFAULT NULL,
  `coverBackBackPrint` int(11) DEFAULT NULL,
  `coverBackFacePrint` int(11) DEFAULT NULL,
  `coverBackLamination` int(11) DEFAULT NULL,
  `coverBackMaterial` int(11) DEFAULT NULL,
  `coverBackPrint` int(11) DEFAULT NULL,
  `coverFacePrint` int(11) DEFAULT NULL,
  `coverLamination` int(11) DEFAULT NULL,
  `coverMaterial` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_brochure_aud`
--

LOCK TABLES `order_brochure_aud` WRITE;
/*!40000 ALTER TABLE `order_brochure_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_brochure_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_calendar`
--

DROP TABLE IF EXISTS `order_calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_calendar` (
  `id` int(10) unsigned NOT NULL COMMENT 'ссылка на позицию заказа',
  `width` int(10) unsigned DEFAULT NULL,
  `height` int(10) unsigned DEFAULT NULL,
  `coverMaterial` int(10) unsigned NOT NULL,
  `coverFacePrint` int(10) unsigned NOT NULL,
  `coverBackPrint` int(10) unsigned NOT NULL,
  `coverLamination` int(10) unsigned DEFAULT NULL,
  `blockMaterial` int(10) unsigned NOT NULL,
  `pageCount` int(10) unsigned DEFAULT NULL,
  `coverBackMaterial` int(10) unsigned NOT NULL,
  `coverBackFacePrint` int(10) unsigned NOT NULL,
  `coverBackBackPrint` int(10) unsigned NOT NULL,
  `coverBackLamination` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_9b8xh8j989vxygag4nawil5r5` (`coverBackBackPrint`),
  UNIQUE KEY `UK_px08kqkr496gk9pgrk7jk8ndt` (`coverBackFacePrint`),
  UNIQUE KEY `UK_mdrm2tecapmkr7dr8c9uwlhh6` (`coverBackPrint`),
  UNIQUE KEY `UK_4p1nuxp2g7aaov3sij0uqv69m` (`coverFacePrint`),
  KEY `FK_fcqtqwc2csghlf756wffrcqnx` (`blockMaterial`),
  KEY `FK_fjgc4h9wh9w629mg3mm0ts6r9` (`coverBackLamination`),
  KEY `FK_njkj1rxexp115t5mt2viy42v0` (`coverBackMaterial`),
  KEY `FK_pkimnpgan87rfnriheky1ixhj` (`coverLamination`),
  KEY `FK_pc7f8gi6klh5fycj3rc7a8x1f` (`coverMaterial`),
  CONSTRAINT `FK_4p1nuxp2g7aaov3sij0uqv69m` FOREIGN KEY (`coverFacePrint`) REFERENCES `order_print_work` (`id`),
  CONSTRAINT `FK_9b8xh8j989vxygag4nawil5r5` FOREIGN KEY (`coverBackBackPrint`) REFERENCES `order_print_work` (`id`),
  CONSTRAINT `FK_fcqtqwc2csghlf756wffrcqnx` FOREIGN KEY (`blockMaterial`) REFERENCES `material` (`id`),
  CONSTRAINT `FK_fjgc4h9wh9w629mg3mm0ts6r9` FOREIGN KEY (`coverBackLamination`) REFERENCES `lamination` (`id`),
  CONSTRAINT `FK_mdrm2tecapmkr7dr8c9uwlhh6` FOREIGN KEY (`coverBackPrint`) REFERENCES `order_print_work` (`id`),
  CONSTRAINT `FK_njkj1rxexp115t5mt2viy42v0` FOREIGN KEY (`coverBackMaterial`) REFERENCES `material` (`id`),
  CONSTRAINT `FK_pc7f8gi6klh5fycj3rc7a8x1f` FOREIGN KEY (`coverMaterial`) REFERENCES `material` (`id`),
  CONSTRAINT `FK_pkimnpgan87rfnriheky1ixhj` FOREIGN KEY (`coverLamination`) REFERENCES `lamination` (`id`),
  CONSTRAINT `FK_px08kqkr496gk9pgrk7jk8ndt` FOREIGN KEY (`coverBackFacePrint`) REFERENCES `order_print_work` (`id`),
  CONSTRAINT `FK_s3e5iwxfkwd9xv1sbj0igqvw7` FOREIGN KEY (`id`) REFERENCES `order_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_calendar`
--

LOCK TABLES `order_calendar` WRITE;
/*!40000 ALTER TABLE `order_calendar` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_calendar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_calendar_aud`
--

DROP TABLE IF EXISTS `order_calendar_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_calendar_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `pageCount` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `blockMaterial` int(11) DEFAULT NULL,
  `coverBackBackPrint` int(11) DEFAULT NULL,
  `coverBackFacePrint` int(11) DEFAULT NULL,
  `coverBackLamination` int(11) DEFAULT NULL,
  `coverBackMaterial` int(11) DEFAULT NULL,
  `coverBackPrint` int(11) DEFAULT NULL,
  `coverFacePrint` int(11) DEFAULT NULL,
  `coverLamination` int(11) DEFAULT NULL,
  `coverMaterial` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_calendar_aud`
--

LOCK TABLES `order_calendar_aud` WRITE;
/*!40000 ALTER TABLE `order_calendar_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_calendar_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_format`
--

DROP TABLE IF EXISTS `order_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_format` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderItem` int(10) unsigned NOT NULL COMMENT 'ссылка на позицию заказа',
  `service` int(10) unsigned NOT NULL COMMENT 'ссылка на услугу',
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sshf64otngtfbin1fkahjcmvg` (`service`),
  CONSTRAINT `FK_sshf64otngtfbin1fkahjcmvg` FOREIGN KEY (`service`) REFERENCES `service` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='данные о форматах изделий в заказах';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_format`
--

LOCK TABLES `order_format` WRITE;
/*!40000 ALTER TABLE `order_format` DISABLE KEYS */;
INSERT INTO `order_format` VALUES (1,111,1,3,4),(2,112,1,4,4),(3,113,1,3,3),(4,114,1,4,7),(5,115,1,2,2),(6,116,1,2,2),(7,117,1,2,2),(8,118,1,3,3),(9,119,1,3,3),(10,120,1,3,3);
/*!40000 ALTER TABLE `order_format` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_item`
--

DROP TABLE IF EXISTS `order_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderId` int(10) unsigned NOT NULL,
  `serviceId` int(10) unsigned NOT NULL COMMENT 'ссылка на услугу',
  `status` int(10) unsigned NOT NULL,
  `amount` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'тираж',
  `cost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `comment` text COLLATE utf8_bin,
  `timeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dateCreated` date NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_knuoabgwovh7grap9bvoflfgc` (`orderId`),
  KEY `FK_3otbcf36co1l7evsd31rdy23v` (`serviceId`),
  KEY `FK_ph7ljaxde0kitm63h8q9orw8r` (`status`),
  CONSTRAINT `FK_3otbcf36co1l7evsd31rdy23v` FOREIGN KEY (`serviceId`) REFERENCES `service` (`id`),
  CONSTRAINT `FK_knuoabgwovh7grap9bvoflfgc` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`),
  CONSTRAINT `FK_ph7ljaxde0kitm63h8q9orw8r` FOREIGN KEY (`status`) REFERENCES `order_item_status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_item`
--

LOCK TABLES `order_item` WRITE;
/*!40000 ALTER TABLE `order_item` DISABLE KEYS */;
INSERT INTO `order_item` VALUES (41,46,7,1,2,2.00,NULL,'2015-03-11 20:41:35','2015-02-03',0),(42,47,7,1,34,445.00,NULL,'2015-03-11 20:41:35','2015-02-02',0),(43,47,1,1,5,6.00,NULL,'2015-03-11 20:41:35','2015-02-04',0),(44,49,7,1,5,5.00,NULL,'2015-03-11 20:41:35','2015-02-02',0),(45,50,6,1,3443,344.00,NULL,'2015-03-11 20:41:35','2015-02-02',0),(46,51,6,1,34,333.00,NULL,'2015-03-11 20:41:35','2015-02-02',0),(47,52,6,1,4,44.00,NULL,'2015-03-11 20:41:35','2015-02-02',0),(48,52,6,1,4,44.00,NULL,'2015-03-11 20:41:35','2015-02-02',0),(49,53,6,1,7,7.00,NULL,'2015-03-11 20:41:35','2015-02-02',0),(50,54,9,1,4,4.00,NULL,'2015-03-11 20:41:35','2015-02-02',0),(51,55,8,1,56,666.00,NULL,'2015-03-11 20:41:35','2015-02-02',0),(52,55,7,1,5,5.00,NULL,'2015-03-11 20:41:35','2015-02-02',0),(65,56,13,3,51,57.00,NULL,'2015-03-11 20:41:35','2015-02-02',0),(69,56,13,10,64,4.00,NULL,'2015-03-11 20:41:35','2015-02-01',0),(70,56,13,5,41,4.00,NULL,'2015-03-11 20:41:35','2015-02-02',0),(71,57,7,2,1,1.00,'что тут сказать, сделайте визитку нахуй!','2015-03-13 21:11:51','2015-02-02',0),(72,57,13,2,7,7.00,NULL,'2015-03-11 20:41:35','2015-02-21',0),(73,57,13,2,7,7.00,NULL,'2015-03-11 20:41:35','2015-02-21',0),(74,57,13,2,4,4.00,NULL,'2015-03-11 20:41:35','2015-02-21',0),(75,57,13,2,6,555.00,NULL,'2015-03-11 20:41:35','2015-02-21',0),(76,57,13,1,7,7.00,NULL,'2015-03-11 20:41:35','2015-02-21',0),(77,57,6,1,1,11.00,NULL,'2015-03-11 20:41:35','2015-02-22',0),(78,55,1,1,1,1.00,'21323','2015-03-11 20:41:35','2015-02-24',0),(79,57,13,1,411,4.00,NULL,'2015-03-11 20:41:35','2015-02-24',0),(82,59,13,1,2,2.00,NULL,'2015-03-11 20:41:35','2015-02-24',0),(83,60,13,1,4,4.00,NULL,'2015-03-11 20:41:35','2015-02-24',0),(84,61,13,1,3,3.00,NULL,'2015-03-11 20:41:35','2015-02-25',0),(86,63,13,1,34,3.00,NULL,'2015-03-11 22:46:33','2015-03-12',1),(87,63,13,1,3,3.00,NULL,'2015-03-11 22:46:30','2015-03-12',1),(88,63,13,1,3,3.00,NULL,'2015-03-11 22:45:52','2015-03-12',1),(89,64,13,8,5,5.00,NULL,'2015-03-22 20:42:44','2015-03-17',0),(90,64,13,5,1,1.00,NULL,'2015-03-22 20:42:49','2015-03-18',0),(91,64,13,1,3,3.00,NULL,'2015-03-17 22:20:54','2015-03-18',0);
/*!40000 ALTER TABLE `order_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_item_aud`
--

DROP TABLE IF EXISTS `order_item_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_item_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `REVEND` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `cost` decimal(19,2) DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `timeCreated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `status_MOD` bit(1) DEFAULT NULL,
  `orderId` int(11) DEFAULT NULL,
  `serviceId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_item_aud`
--

LOCK TABLES `order_item_aud` WRITE;
/*!40000 ALTER TABLE `order_item_aud` DISABLE KEYS */;
INSERT INTO `order_item_aud` VALUES (89,2,0,3,5,NULL,5.00,'2015-03-17 01:45:25','\0','2015-03-17 01:45:25',1,NULL,64,13),(89,3,1,4,5,NULL,5.00,'2015-03-17 00:00:00','\0','2015-03-17 01:45:25',1,NULL,64,13),(89,4,1,7,5,NULL,5.00,'2015-03-17 00:00:00','\0','2015-03-17 01:45:25',1,NULL,64,13),(89,7,1,8,5,NULL,5.00,'2015-03-17 00:00:00','\0','2015-03-17 01:45:25',5,NULL,64,13),(89,8,1,9,54,NULL,5.00,'2015-03-17 00:00:00','\0','2015-03-17 01:45:25',5,NULL,64,13),(89,9,1,10,5,NULL,5.00,'2015-03-17 00:00:00','\0','2015-03-19 01:08:19',6,NULL,64,13),(89,10,1,NULL,5,NULL,5.00,'2015-03-17 00:00:00','\0','2015-03-19 01:08:19',8,'',64,13),(90,5,0,11,1,NULL,1.00,'2015-03-18 01:20:40','\0','2015-03-18 01:20:40',1,NULL,64,13),(90,11,1,NULL,1,NULL,1.00,'2015-03-18 00:00:00','\0','2015-03-18 01:20:40',5,'',64,13),(91,6,0,NULL,3,NULL,3.00,'2015-03-18 01:20:54','\0','2015-03-18 01:20:54',1,NULL,64,13);
/*!40000 ALTER TABLE `order_item_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_item_status`
--

DROP TABLE IF EXISTS `order_item_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_item_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_bin NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_item_status`
--

LOCK TABLES `order_item_status` WRITE;
/*!40000 ALTER TABLE `order_item_status` DISABLE KEYS */;
INSERT INTO `order_item_status` VALUES (1,'NEW','Новый'),(2,'COMPLETE','Выполнен'),(3,'GIVEN_TO_CLIENT','Выдан клиенту'),(5,'ON_AGREEMENT','На согласовании'),(6,'DESIGN_APPROVED','Дизайн утвержден'),(7,'DESIGN_NOT_APPROVED','Дизайн не утвержден'),(8,'INVOICE_TO_PAY','Счет к оплате'),(9,'TO_PRE_PRESS','Передан в допечатную обработку'),(10,'IN_PRE_PRESS','Принято - допечатная обработка'),(11,'READY_PRE_PRESS','Готово - допечатная обработка'),(12,'IN_PRESS','Принято - печать'),(13,'REDY_PRESS','Готово - печать'),(14,'IN_POST_PRESS','Принято - постпечатная обработка'),(15,'REAY_POST_PRESS','Готово - постпечатная обработка'),(16,'CANCEL','Отмена');
/*!40000 ALTER TABLE `order_item_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_item_status_link`
--

DROP TABLE IF EXISTS `order_item_status_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_item_status_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderItemStatusId` int(10) unsigned DEFAULT NULL,
  `nextOrderItemStatusId` int(10) unsigned DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_6gacphmy4uf6hcgxja3vd22n6` (`nextOrderItemStatusId`),
  KEY `FK_aye3ynrw2mmo31qqyl361stx1` (`orderItemStatusId`),
  CONSTRAINT `FK_6gacphmy4uf6hcgxja3vd22n6` FOREIGN KEY (`nextOrderItemStatusId`) REFERENCES `order_item_status` (`id`),
  CONSTRAINT `FK_aye3ynrw2mmo31qqyl361stx1` FOREIGN KEY (`orderItemStatusId`) REFERENCES `order_item_status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='иерархия статусов заказа';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_item_status_link`
--

LOCK TABLES `order_item_status_link` WRITE;
/*!40000 ALTER TABLE `order_item_status_link` DISABLE KEYS */;
INSERT INTO `order_item_status_link` VALUES (1,1,5,'новый - на согласовании',''),(2,5,6,'на согласовании - дизайн утвержден',''),(3,5,7,'на согласовании - дизайн не утвержден',''),(4,6,8,'Дизайн утвержден - счет к оплате',''),(5,7,6,'диз не утв - диз утв',''),(15,8,9,'счет - передано в prepress',''),(16,9,10,'передано prepress - prepress accept',''),(17,10,11,'prepress accept - prepress ready',''),(18,11,12,'prepress ready - press accept',''),(19,12,13,'press accept - press ready',''),(20,13,14,'press ready - postpress accept',''),(21,15,2,'postpress ready - complete',''),(22,2,3,'complete - client',''),(23,14,15,'postpress accept - pstpress ready','');
/*!40000 ALTER TABLE `order_item_status_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_item_status_link_security`
--

DROP TABLE IF EXISTS `order_item_status_link_security`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_item_status_link_security` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderItemStatusLinkId` int(10) unsigned DEFAULT NULL,
  `roleId` int(10) unsigned NOT NULL COMMENT 'роль, которой доступен этот переход',
  PRIMARY KEY (`id`),
  KEY `FK_k2l8pbnpy7shyh7r0ej7odix` (`orderItemStatusLinkId`),
  KEY `FK_jty86tsq6c5uhfv4fan9umx48` (`roleId`),
  CONSTRAINT `FK_jty86tsq6c5uhfv4fan9umx48` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`),
  CONSTRAINT `FK_k2l8pbnpy7shyh7r0ej7odix` FOREIGN KEY (`orderItemStatusLinkId`) REFERENCES `order_item_status_link` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='какой роли доступен переход из статуса в статус';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_item_status_link_security`
--

LOCK TABLES `order_item_status_link_security` WRITE;
/*!40000 ALTER TABLE `order_item_status_link_security` DISABLE KEYS */;
INSERT INTO `order_item_status_link_security` VALUES (1,1,4),(2,2,5),(3,3,5),(4,4,4),(5,5,5),(6,15,4),(7,16,5),(8,17,5),(9,18,6),(10,19,6),(11,20,7),(12,22,7),(13,22,4),(14,23,7);
/*!40000 ALTER TABLE `order_item_status_link_security` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_lamination_work`
--

DROP TABLE IF EXISTS `order_lamination_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_lamination_work` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lamination` int(10) unsigned NOT NULL COMMENT 'ссылка на тип ламинации',
  `isBothSide` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_ai0upv5a0cl5pu7htjcrir570` (`lamination`),
  CONSTRAINT `FK_ai0upv5a0cl5pu7htjcrir570` FOREIGN KEY (`lamination`) REFERENCES `lamination` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='данные о работах по ламинации';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_lamination_work`
--

LOCK TABLES `order_lamination_work` WRITE;
/*!40000 ALTER TABLE `order_lamination_work` DISABLE KEYS */;
INSERT INTO `order_lamination_work` VALUES (1,1,1);
/*!40000 ALTER TABLE `order_lamination_work` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_lamination_work_aud`
--

DROP TABLE IF EXISTS `order_lamination_work_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_lamination_work_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `REVEND` int(11) DEFAULT NULL,
  `isBothSide` bit(1) DEFAULT NULL,
  `lamination` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_lamination_work_aud`
--

LOCK TABLES `order_lamination_work_aud` WRITE;
/*!40000 ALTER TABLE `order_lamination_work_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_lamination_work_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_leaflet`
--

DROP TABLE IF EXISTS `order_leaflet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_leaflet` (
  `id` int(10) unsigned NOT NULL,
  `material` int(10) unsigned NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `facePrint` int(10) unsigned NOT NULL,
  `backPrint` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_13ffu6lxagj2peipjb4agbxho` (`backPrint`),
  UNIQUE KEY `UK_j3g561nfp2298fqjj3ehl1k9u` (`facePrint`),
  KEY `FK_q8tho2a4mn79co4kdn17gqjhh` (`material`),
  CONSTRAINT `FK_13ffu6lxagj2peipjb4agbxho` FOREIGN KEY (`backPrint`) REFERENCES `order_print_work` (`id`),
  CONSTRAINT `FK_j3g561nfp2298fqjj3ehl1k9u` FOREIGN KEY (`facePrint`) REFERENCES `order_print_work` (`id`),
  CONSTRAINT `FK_q8tho2a4mn79co4kdn17gqjhh` FOREIGN KEY (`material`) REFERENCES `material` (`id`),
  CONSTRAINT `FK_sbt806dmclbm4yqb0d9vvuwib` FOREIGN KEY (`id`) REFERENCES `order_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_leaflet`
--

LOCK TABLES `order_leaflet` WRITE;
/*!40000 ALTER TABLE `order_leaflet` DISABLE KEYS */;
INSERT INTO `order_leaflet` VALUES (50,1,5,5,47,46);
/*!40000 ALTER TABLE `order_leaflet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_leaflet_aud`
--

DROP TABLE IF EXISTS `order_leaflet_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_leaflet_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `backPrint` int(11) DEFAULT NULL,
  `facePrint` int(11) DEFAULT NULL,
  `material` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_leaflet_aud`
--

LOCK TABLES `order_leaflet_aud` WRITE;
/*!40000 ALTER TABLE `order_leaflet_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_leaflet_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_material`
--

DROP TABLE IF EXISTS `order_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_material` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderItem` int(10) unsigned NOT NULL COMMENT 'ссылка на позицию заказа',
  `service` int(10) unsigned NOT NULL COMMENT 'ссылка на услугу',
  `material` int(10) unsigned NOT NULL COMMENT 'ссылка на материал',
  PRIMARY KEY (`id`),
  KEY `FK_o9c0sejobk03k7fl0uc9ftvdr` (`material`),
  CONSTRAINT `FK_o9c0sejobk03k7fl0uc9ftvdr` FOREIGN KEY (`material`) REFERENCES `material` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='данные о материале в заказе для услуги';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_material`
--

LOCK TABLES `order_material` WRITE;
/*!40000 ALTER TABLE `order_material` DISABLE KEYS */;
INSERT INTO `order_material` VALUES (1,114,3,1),(2,114,4,1),(3,114,5,2),(4,115,3,2),(5,115,4,2),(6,115,5,2),(7,116,3,2),(8,116,4,1),(9,116,5,1),(10,117,3,2),(11,117,4,2),(12,117,5,2),(13,118,3,2),(14,118,4,2),(15,118,5,2),(16,119,3,1),(17,119,4,2),(18,119,5,2),(19,120,3,1),(20,120,4,1),(21,120,5,2);
/*!40000 ALTER TABLE `order_material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_notebook`
--

DROP TABLE IF EXISTS `order_notebook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_notebook` (
  `id` int(10) unsigned NOT NULL COMMENT 'ссылка на позицию заказа',
  `width` int(10) unsigned DEFAULT NULL,
  `height` int(10) unsigned DEFAULT NULL,
  `coverMaterial` int(10) unsigned NOT NULL,
  `coverFacePrint` int(10) unsigned NOT NULL,
  `coverBackPrint` int(10) unsigned NOT NULL,
  `coverLamination` int(10) unsigned DEFAULT NULL,
  `blockMaterial` int(10) unsigned NOT NULL,
  `pageCount` int(10) unsigned DEFAULT NULL,
  `coverBackMaterial` int(10) unsigned NOT NULL,
  `coverBackFacePrint` int(10) unsigned NOT NULL,
  `coverBackBackPrint` int(10) unsigned NOT NULL,
  `coverBackLamination` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_mxvf874pkjq90u7egfq0cdqcu` (`blockMaterial`),
  KEY `FK_dqfle07xokek0b8l0yb34pvay` (`coverBackLamination`),
  KEY `FK_fs43vgch59tg4a9n0bwimwljo` (`coverBackMaterial`),
  KEY `FK_4fc3abpwiylniv48xrdewxndu` (`coverLamination`),
  KEY `FK_he37omgcidxtkpkkno3b5xnxq` (`coverMaterial`),
  CONSTRAINT `FK_4fc3abpwiylniv48xrdewxndu` FOREIGN KEY (`coverLamination`) REFERENCES `lamination` (`id`),
  CONSTRAINT `FK_dqfle07xokek0b8l0yb34pvay` FOREIGN KEY (`coverBackLamination`) REFERENCES `lamination` (`id`),
  CONSTRAINT `FK_fs43vgch59tg4a9n0bwimwljo` FOREIGN KEY (`coverBackMaterial`) REFERENCES `material` (`id`),
  CONSTRAINT `FK_he37omgcidxtkpkkno3b5xnxq` FOREIGN KEY (`coverMaterial`) REFERENCES `material` (`id`),
  CONSTRAINT `FK_mxvf874pkjq90u7egfq0cdqcu` FOREIGN KEY (`blockMaterial`) REFERENCES `material` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_notebook`
--

LOCK TABLES `order_notebook` WRITE;
/*!40000 ALTER TABLE `order_notebook` DISABLE KEYS */;
INSERT INTO `order_notebook` VALUES (43,6,6,1,38,37,NULL,1,5,1,36,35,NULL),(78,1,1,1,101,100,NULL,1,11,1,99,98,NULL),(133,455,455,2,0,0,NULL,1,455,1,0,0,NULL),(141,455,45,2,0,0,NULL,2,455,2,0,0,NULL),(148,233,33,2,0,0,NULL,1,344,1,0,0,NULL);
/*!40000 ALTER TABLE `order_notebook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_notebook_aud`
--

DROP TABLE IF EXISTS `order_notebook_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_notebook_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `pageCount` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `blockMaterial` int(11) DEFAULT NULL,
  `coverBackBackPrint` int(11) DEFAULT NULL,
  `coverBackFacePrint` int(11) DEFAULT NULL,
  `coverBackLamination` int(11) DEFAULT NULL,
  `coverBackMaterial` int(11) DEFAULT NULL,
  `coverBackPrint` int(11) DEFAULT NULL,
  `coverFacePrint` int(11) DEFAULT NULL,
  `coverLamination` int(11) DEFAULT NULL,
  `coverMaterial` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_notebook_aud`
--

LOCK TABLES `order_notebook_aud` WRITE;
/*!40000 ALTER TABLE `order_notebook_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_notebook_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_page_count`
--

DROP TABLE IF EXISTS `order_page_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_page_count` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderItem` int(10) unsigned NOT NULL COMMENT 'ссылка на позицию заказа',
  `service` int(10) unsigned NOT NULL COMMENT 'ссылка на услугу',
  `pageCount` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='данные о количестве страниц в заказе для услуги';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_page_count`
--

LOCK TABLES `order_page_count` WRITE;
/*!40000 ALTER TABLE `order_page_count` DISABLE KEYS */;
INSERT INTO `order_page_count` VALUES (1,111,4,3),(2,112,4,2),(3,113,4,2),(4,114,4,2),(5,115,4,1),(6,116,4,2),(7,117,4,1),(8,118,4,2),(9,119,4,2),(10,120,4,324);
/*!40000 ALTER TABLE `order_page_count` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_print_work`
--

DROP TABLE IF EXISTS `order_print_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_print_work` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noPrint` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'без печати',
  `isFullColor` tinyint(4) DEFAULT NULL,
  `bw` tinyint(1) NOT NULL DEFAULT '0',
  `c` tinyint(1) NOT NULL DEFAULT '0',
  `m` tinyint(1) NOT NULL DEFAULT '0',
  `y` tinyint(1) NOT NULL DEFAULT '0',
  `k` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='данные о печати для услуг заказа';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_print_work`
--

LOCK TABLES `order_print_work` WRITE;
/*!40000 ALTER TABLE `order_print_work` DISABLE KEYS */;
INSERT INTO `order_print_work` VALUES (1,0,0,0,0,0,0,0),(2,0,0,0,1,1,0,0),(3,0,0,0,1,0,1,0),(4,0,0,0,0,1,0,1),(5,0,0,1,0,0,0,0),(9,0,0,0,0,0,0,0),(10,0,1,0,0,0,0,0),(11,0,0,0,0,0,0,0),(12,0,1,0,0,0,0,0),(13,0,0,0,0,0,0,0),(14,0,1,0,0,0,0,0),(15,0,0,0,0,0,0,0),(16,0,1,0,0,0,0,0),(17,0,0,0,0,0,0,0),(18,0,0,1,0,0,0,0),(19,0,0,0,0,0,0,0),(20,0,1,0,1,1,0,0),(25,0,0,0,0,0,0,1),(26,0,0,0,1,0,0,0),(27,0,0,0,0,1,1,0),(28,0,0,0,1,0,0,1),(29,0,1,0,0,0,0,0),(30,0,0,0,1,0,0,1),(31,0,0,0,0,0,0,0),(32,0,0,0,0,0,0,0),(33,0,0,0,0,0,0,0),(34,0,0,0,0,0,0,0),(35,0,0,0,0,0,0,0),(36,0,0,0,0,0,0,0),(37,0,0,0,0,0,0,0),(38,0,0,0,0,0,0,0),(39,0,0,0,0,0,0,0),(40,0,0,0,0,0,0,0),(41,0,0,0,1,0,1,0),(42,0,0,0,1,1,0,1),(43,0,0,0,1,1,1,1),(44,0,0,0,1,1,0,1),(45,0,0,0,1,1,1,1),(46,0,0,0,1,0,1,0),(47,0,1,0,0,0,0,0),(48,1,0,0,0,0,0,0),(49,1,0,0,0,0,0,0),(50,1,0,0,0,0,0,0),(51,1,0,0,0,0,0,0),(52,1,0,0,0,0,0,0),(53,1,0,0,0,0,0,0),(54,1,0,0,0,0,0,0),(55,1,0,0,0,0,0,0),(56,1,0,0,0,0,0,0),(57,1,0,0,0,0,0,0),(58,1,0,0,0,0,0,0),(59,1,0,0,0,0,0,0),(60,1,0,0,0,0,0,0),(61,1,0,0,0,0,0,0),(62,1,0,0,0,0,0,0),(63,1,0,0,0,0,0,0),(64,1,0,0,0,0,0,0),(65,1,0,0,0,0,0,0),(66,1,0,0,0,0,0,0),(67,1,0,0,0,0,0,0),(68,1,0,0,0,0,0,0),(69,1,0,0,0,0,0,0),(70,1,0,0,0,0,0,0),(71,1,0,0,0,0,0,0),(72,1,0,0,0,0,0,0),(73,1,0,0,0,0,0,0),(74,1,0,0,0,0,0,0),(75,1,0,0,0,0,0,0),(76,1,0,0,0,0,0,0),(77,1,0,0,0,0,0,0),(78,1,0,0,0,0,0,0),(79,1,0,0,0,0,0,0),(80,1,0,0,0,0,0,0),(81,1,0,0,0,0,0,0),(82,1,0,0,0,0,0,0),(83,1,0,0,0,0,0,0),(84,1,0,0,0,0,0,0),(85,1,0,0,0,0,0,0),(86,1,0,0,0,0,0,0),(87,1,0,0,0,0,0,0),(92,1,0,0,0,0,0,0),(93,1,0,0,0,0,0,0),(94,1,0,0,0,0,0,0),(95,0,1,0,0,0,0,0),(96,1,0,0,0,0,0,0),(97,1,0,0,0,0,0,0),(98,0,0,0,1,0,1,0),(99,0,1,0,0,0,0,0),(100,0,0,0,1,0,1,0),(101,0,1,0,0,0,0,0),(102,0,0,0,1,1,0,0),(103,1,0,0,0,0,0,0),(104,1,0,0,0,0,0,0),(105,1,0,0,0,0,0,0),(106,1,0,0,0,0,0,0),(107,1,0,0,0,0,0,0),(108,1,0,0,0,0,0,0),(109,1,0,0,0,0,0,0),(110,1,0,0,0,0,0,0),(111,1,0,0,0,0,0,0),(112,1,0,0,0,0,0,0),(113,1,0,0,0,0,0,0),(114,1,0,0,0,0,0,0);
/*!40000 ALTER TABLE `order_print_work` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_print_work_aud`
--

DROP TABLE IF EXISTS `order_print_work_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_print_work_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `REVEND` int(11) DEFAULT NULL,
  `bw` bit(1) DEFAULT NULL,
  `c` bit(1) DEFAULT NULL,
  `isFullColor` bit(1) DEFAULT NULL,
  `k` bit(1) DEFAULT NULL,
  `m` bit(1) DEFAULT NULL,
  `noPrint` bit(1) DEFAULT NULL,
  `y` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_print_work_aud`
--

LOCK TABLES `order_print_work_aud` WRITE;
/*!40000 ALTER TABLE `order_print_work_aud` DISABLE KEYS */;
INSERT INTO `order_print_work_aud` VALUES (112,2,0,NULL,'\0','\0','\0','\0','\0','','\0'),(113,5,0,NULL,'\0','\0','\0','\0','\0','','\0'),(114,6,0,NULL,'\0','\0','\0','\0','\0','','\0');
/*!40000 ALTER TABLE `order_print_work_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_status`
--

DROP TABLE IF EXISTS `order_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_bin NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_status`
--

LOCK TABLES `order_status` WRITE;
/*!40000 ALTER TABLE `order_status` DISABLE KEYS */;
INSERT INTO `order_status` VALUES (4,'NEW','Новый'),(5,'IN_WORK','В работе'),(6,'COMPLETE','Выполнен');
/*!40000 ALTER TABLE `order_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_sticker`
--

DROP TABLE IF EXISTS `order_sticker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_sticker` (
  `id` int(10) unsigned NOT NULL,
  `material` int(10) unsigned NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `print` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_595gs7pqc9cch5pqullolpqkt` (`print`),
  KEY `FK_h83c6p5g5ivxp5ekaf8txgu34` (`material`),
  CONSTRAINT `FK_ew1re2lyaou4drgkc7054a8uf` FOREIGN KEY (`id`) REFERENCES `order_item` (`id`),
  CONSTRAINT `FK_h83c6p5g5ivxp5ekaf8txgu34` FOREIGN KEY (`material`) REFERENCES `material` (`id`),
  CONSTRAINT `FK_pd1wxrupkfm9dwre3yqxc970x` FOREIGN KEY (`print`) REFERENCES `order_print_work` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_sticker`
--

LOCK TABLES `order_sticker` WRITE;
/*!40000 ALTER TABLE `order_sticker` DISABLE KEYS */;
INSERT INTO `order_sticker` VALUES (65,1,55,55,75),(69,1,4,4,79),(70,1,4,4,80),(72,1,7,7,92),(73,1,7,7,93),(74,1,4,4,94),(75,1,6,6,95),(76,1,7,7,96),(79,1,411,4,102),(82,2,2,2,105),(83,1,4,4,106),(84,2,3,3,107),(86,1,31,31,109),(87,2,3,3,110),(88,1,3,3,111),(89,1,515,5,112),(90,1,1,1,113),(91,1,3,3,114);
/*!40000 ALTER TABLE `order_sticker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_sticker_aud`
--

DROP TABLE IF EXISTS `order_sticker_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_sticker_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `material` int(11) DEFAULT NULL,
  `print` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_sticker_aud`
--

LOCK TABLES `order_sticker_aud` WRITE;
/*!40000 ALTER TABLE `order_sticker_aud` DISABLE KEYS */;
INSERT INTO `order_sticker_aud` VALUES (89,2,5,5,1,112),(89,3,5,51,1,112),(89,4,5,515,1,112),(89,7,5,515,1,112),(89,8,5,515,1,112),(89,9,5,515,1,112),(89,10,5,515,1,112),(90,5,1,1,1,113),(90,11,1,1,1,113),(91,6,3,3,1,114);
/*!40000 ALTER TABLE `order_sticker_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_visiting_card`
--

DROP TABLE IF EXISTS `order_visiting_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_visiting_card` (
  `id` int(10) unsigned NOT NULL,
  `material` int(10) unsigned NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `facePrint` int(10) unsigned NOT NULL,
  `backPrint` int(10) unsigned NOT NULL,
  `lamination` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_nvg1v8hbodhydsuyu6au7ftm5` (`backPrint`),
  UNIQUE KEY `UK_1ys0dtc75ymbg0kibs22jysjs` (`facePrint`),
  KEY `FK_nw9o2mbatdtpi3cgoae247t3f` (`material`),
  KEY `FK_flf6y6kd1haa8aw29qk5m2rpt` (`lamination`),
  CONSTRAINT `FK_1ys0dtc75ymbg0kibs22jysjs` FOREIGN KEY (`facePrint`) REFERENCES `order_print_work` (`id`),
  CONSTRAINT `FK_as1guqpfa5n4g9v0iw1m7ewqq` FOREIGN KEY (`id`) REFERENCES `order_item` (`id`),
  CONSTRAINT `FK_flf6y6kd1haa8aw29qk5m2rpt` FOREIGN KEY (`lamination`) REFERENCES `order_lamination_work` (`id`),
  CONSTRAINT `FK_nvg1v8hbodhydsuyu6au7ftm5` FOREIGN KEY (`backPrint`) REFERENCES `order_print_work` (`id`),
  CONSTRAINT `FK_nw9o2mbatdtpi3cgoae247t3f` FOREIGN KEY (`material`) REFERENCES `material` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_visiting_card`
--

LOCK TABLES `order_visiting_card` WRITE;
/*!40000 ALTER TABLE `order_visiting_card` DISABLE KEYS */;
INSERT INTO `order_visiting_card` VALUES (41,2,2,2,32,31,1),(42,2,4,4,34,33,1),(44,1,5,5,40,39,1),(52,2,5,5,53,52,1),(71,1,1,1,87,86,1);
/*!40000 ALTER TABLE `order_visiting_card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_visiting_card_aud`
--

DROP TABLE IF EXISTS `order_visiting_card_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_visiting_card_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `backPrint` int(11) DEFAULT NULL,
  `facePrint` int(11) DEFAULT NULL,
  `lamination` int(11) DEFAULT NULL,
  `material` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_visiting_card_aud`
--

LOCK TABLES `order_visiting_card_aud` WRITE;
/*!40000 ALTER TABLE `order_visiting_card_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_visiting_card_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderId` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'идентификатор заказа',
  `clientId` int(10) unsigned NOT NULL,
  `itemCount` int(10) unsigned NOT NULL DEFAULT '0',
  `timeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dateCreated` date NOT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `cost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `comment` text COLLATE utf8_bin,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_6lkg0gij43h4fuca4mx56q2lh` (`clientId`),
  KEY `FK_qro50btxtakk2eg9v13c1se48` (`status`),
  CONSTRAINT `FK_6lkg0gij43h4fuca4mx56q2lh` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`),
  CONSTRAINT `FK_qro50btxtakk2eg9v13c1se48` FOREIGN KEY (`status`) REFERENCES `order_status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (46,'150246',9,1,'2015-02-11 12:25:56','2015-02-11',4,2.00,NULL,0),(47,'150247',9,2,'2015-02-11 12:36:49','2015-02-11',4,451.00,NULL,0),(49,'150248',2,1,'2015-02-15 12:53:06','2015-02-15',4,5.00,NULL,0),(50,'150250',12,1,'2015-02-15 13:11:18','2015-02-15',4,344.00,NULL,0),(51,'150251',9,1,'2015-02-15 13:19:10','2015-02-15',4,333.00,NULL,0),(52,'150252',1,2,'2015-02-15 13:20:13','2015-02-15',4,44.00,NULL,0),(53,'150253',3,1,'2015-02-15 13:24:30','2015-02-15',4,7.00,NULL,0),(54,'150254',2,1,'2015-02-15 13:47:12','2015-02-15',4,4.00,NULL,0),(55,'150255',3,2,'2015-02-15 18:43:35','2015-02-15',4,671.00,NULL,0),(56,'150256',22,2,'2015-02-15 20:34:42','2015-02-16',4,4560.00,NULL,0),(57,'150247',1,0,'2015-02-17 12:51:07','2015-02-17',4,0.00,NULL,0),(59,'150259',3,0,'2015-02-24 20:53:14','2015-02-24',4,0.00,NULL,0),(60,'150260',2,0,'2015-02-24 20:54:45','2015-02-24',4,0.00,NULL,0),(61,'150261',3,0,'2015-02-24 21:03:20','2015-02-25',4,0.00,NULL,0),(62,'150262',12,0,'2015-02-24 21:04:20','2015-02-25',4,0.00,NULL,0),(63,'150363',9,0,'2015-03-11 22:51:30','2015-03-12',NULL,0.00,NULL,0),(64,'150364',2,0,'2015-03-16 22:45:33','2015-03-17',NULL,0.00,NULL,0);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_aud`
--

DROP TABLE IF EXISTS `orders_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `REVEND` int(11) DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL,
  `orderId` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `timeCreated` datetime DEFAULT NULL,
  `clientId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_aud`
--

LOCK TABLES `orders_aud` WRITE;
/*!40000 ALTER TABLE `orders_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revinfo`
--

DROP TABLE IF EXISTS `revinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revinfo` (
  `REV` int(11) NOT NULL AUTO_INCREMENT,
  `REVTSTMP` bigint(20) DEFAULT NULL,
  `user` int(10) unsigned NOT NULL,
  PRIMARY KEY (`REV`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revinfo`
--

LOCK TABLES `revinfo` WRITE;
/*!40000 ALTER TABLE `revinfo` DISABLE KEYS */;
INSERT INTO `revinfo` VALUES (1,1424805057588,1),(2,1424805077565,1),(3,1424805097372,1),(4,1424805120257,1),(5,1424805225288,1),(6,1424805330769,1),(7,1424807593638,1),(8,1424807684900,1),(9,1424808200365,1),(10,1424808260205,1),(20,1426106594795,1),(21,1426109349096,1),(23,1426110690500,1),(24,1426110697642,1),(25,1426113952526,1),(26,1426113990904,1),(27,1426113993879,1);
/*!40000 ALTER TABLE `revinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_8sewwnpamngi6b1dwaa88askk` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ROLE_ANONYMOUS'),(4,'ROLE_MANAGER'),(7,'ROLE_POSTPRESS'),(5,'ROLE_PREPRESS'),(6,'ROLE_PRESS'),(3,'ROLE_SA');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'Блокнот','NOTEBOOK'),(6,'Баннер','BANNER'),(7,'Визитка','VISITING_CARD'),(8,'Брошюра','BROCHURE'),(9,'Листовка','LEAFLET'),(10,'Буклет','BOOKLET'),(11,'Календарь','CALENDAR'),(13,'Наклейка','STICKER');
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_attribute`
--

DROP TABLE IF EXISTS `service_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `code` varchar(255) NOT NULL,
  `isRequired` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_attribute`
--

LOCK TABLES `service_attribute` WRITE;
/*!40000 ALTER TABLE `service_attribute` DISABLE KEYS */;
INSERT INTO `service_attribute` VALUES (1,'формат','format',1),(2,'материал','material',1),(3,'цвет пружины','',0),(4,'материал пружины','',0),(8,'количество страниц','pageCount',1),(9,'разрешение','',0),(10,'тираж','amount',1);
/*!40000 ALTER TABLE `service_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_service_attribute`
--

DROP TABLE IF EXISTS `service_service_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_service_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service` int(10) unsigned NOT NULL,
  `attribute` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service` (`service`,`attribute`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_service_attribute`
--

LOCK TABLES `service_service_attribute` WRITE;
/*!40000 ALTER TABLE `service_service_attribute` DISABLE KEYS */;
INSERT INTO `service_service_attribute` VALUES (1,1,1),(8,1,10),(2,3,2),(3,4,2),(5,4,8),(4,5,2),(9,6,1),(10,6,2),(11,6,10),(6,7,1),(7,7,2),(14,7,10),(15,8,1),(16,8,10),(17,14,2),(18,15,2),(19,15,8),(20,16,2);
/*!40000 ALTER TABLE `service_service_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_service_work`
--

DROP TABLE IF EXISTS `service_service_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_service_work` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service` int(10) unsigned NOT NULL,
  `work` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service` (`service`,`work`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_service_work`
--

LOCK TABLES `service_service_work` WRITE;
/*!40000 ALTER TABLE `service_service_work` DISABLE KEYS */;
INSERT INTO `service_service_work` VALUES (1,3,1),(4,3,3),(3,5,1),(6,5,3),(7,6,4),(8,7,1),(9,7,2),(10,7,3),(15,8,1),(16,8,3),(17,9,1),(11,14,1),(12,14,3),(13,16,1),(14,16,3);
/*!40000 ALTER TABLE `service_service_work` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_work`
--

DROP TABLE IF EXISTS `service_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_work` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_work`
--

LOCK TABLES `service_work` WRITE;
/*!40000 ALTER TABLE `service_work` DISABLE KEYS */;
INSERT INTO `service_work` VALUES (1,'Печать','print'),(2,'Лак','varnish'),(3,'Ламинация','lamination'),(4,'Монтаж','mount');
/*!40000 ALTER TABLE `service_work` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_work_price`
--

DROP TABLE IF EXISTS `service_work_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_work_price` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `work` int(10) unsigned NOT NULL,
  `amountFrom` int(10) unsigned NOT NULL,
  `amountTo` int(10) unsigned NOT NULL,
  `price` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_work_price`
--

LOCK TABLES `service_work_price` WRITE;
/*!40000 ALTER TABLE `service_work_price` DISABLE KEYS */;
INSERT INTO `service_work_price` VALUES (1,1,1,100,20);
/*!40000 ALTER TABLE `service_work_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_gj2fy3dcix7ph7k8684gka40c` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'igor','$2a$10$1sxNgoClFGZT64cnWWwb7uiq7zZCK6ZVv5fjO2hX5SSNMc5qbRCge',1,NULL,NULL,NULL),(2,'manager','$2a$10$1sxNgoClFGZT64cnWWwb7uiq7zZCK6ZVv5fjO2hX5SSNMc5qbRCge',1,NULL,NULL,NULL),(3,'prepress','$2a$10$1sxNgoClFGZT64cnWWwb7uiq7zZCK6ZVv5fjO2hX5SSNMc5qbRCge',1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `roleId` int(10) unsigned NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `roleId` (`roleId`),
  KEY `FK_oj50qpdthexxxmvur61ggy2fb` (`userId`),
  CONSTRAINT `FK_oj50qpdthexxxmvur61ggy2fb` FOREIGN KEY (`userId`) REFERENCES `user` (`id`),
  CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (3,1,3,NULL,NULL),(4,2,4,NULL,NULL),(5,3,5,NULL,NULL);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-23  2:37:03
