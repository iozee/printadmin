SELECT
  order0_.id                                                                                                    AS id1_34_,
  order0_.clientId                                                                                              AS clientId5_34_,
  order0_.dateCreated                                                                                           AS dateCrea2_34_,
  order0_.orderId                                                                                               AS orderId3_34_,
  order0_.timeCreated                                                                                           AS timeCrea4_34_,
  (SELECT IFNULL((SELECT SUM(oi.cost)
                  FROM order_item oi
                  WHERE oi.orderId = order0_.id AND oi.deleted = 0),
                 0))                                                                                            AS formula1_,
  (SELECT COUNT(*)
   FROM order_item oi
   WHERE oi.orderId = order0_.id AND oi.deleted =
                                     0)                                                                         AS formula2_,
  (SELECT IFNULL((SELECT CEILING(((SELECT                                  COUNT(*)
                                   FROM order_item
                                     INNER JOIN order_item_status STATUS ON order_item.status = status.id
                                   WHERE
                                     order_item.orderId = order0_.id AND status.code IN ('COMPLETE', 'GIVEN_TO_CLIENT')
                                     AND order_item.deleted = 0) / (SELECT COUNT(*)
                                                                    FROM order_item
                                                                    WHERE order_item.orderId = order0_.id AND
                                                                          order_item.deleted = 0)) * 100)),
                 0))                                                                                            AS formula3_
FROM orders order0_ INNER JOIN order_item orderitems1_ ON order0_.id = orderitems1_.orderId
WHERE orderitems1_.serviceId = ?