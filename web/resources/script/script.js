var formEdited = false;

jQuery(document).ready(function () {

    jQuery(".ui-state-highlight").removeClass("ui-state-highlight");

    jQuery("form.editMonitor").on("keyup", "input", function (event) {
        if (event.which != 13 && event.which != 27) {
            //formEdited = true;
        }
    });

    var bodyElement = jQuery("body");

    bodyElement.on("mouseup", "li.ui-selectonemenu-item:not(.ui-state-highlight)", function (event) {
        //formEdited = true;
    });

    bodyElement.on("mouseup", ".ui-widget-overlay", function () {
        closeEditDialog();
    }).keyup(function (event) {
        // close on escape
        if (event.which == 27) {
            closeEditDialog();
        }
    });
    /*
     jQuery("form").keypress(function (event) {
     if (event.which == 13) {
     event.preventDefault();
     }
     }); */

});

/*broken id*/
function resetOrdersFilterByStatus() {
    jQuery('#orderTableForm\\:orderTable\\:orderStatusMenu_input').val('');
    jQuery('#orderTableForm\\:orderTable\\:orderStatusMenu_label').html('').append('все');
    PF('ordersTable').filter()
}

/*broken id*/
function filterOrdersByStatus(status, statusName) {
    jQuery('#orderListLayout\\:orderTable\\:orderStatusMenu_input').val(status);
    jQuery('#orderListLayout\\:orderTable\\:orderStatusMenu_label').html('').append(statusName);
    PF('ordersTable').filter()
}

Array.max = function (array) {
    return Math.max.apply(Math, array);
};

function closeConfirm() {
    PF('editMonitorAlert').hide();
    formEdited = false;
    closeClosestDialog();
}

function closeEditDialog() {
    if (formEdited) {
        PF("editMonitorAlert").show();
    } else {
        closeClosestDialog();
    }
}

function closeClosestDialog() {
    var dialogs = jQuery('div.ui-dialog.ui-overlay-visible');
    var zIndexes = dialogs.map(function () {
        return jQuery(this).zIndex();
    }).get();

    dialogs.filter(function () {
        return jQuery(this).css('z-index') == Array.max(zIndexes);
    }).each(function () {
        jQuery(this).find(".ui-dialog-titlebar-close").click();
    });
    formEdited = false;
}

PrimeFaces.locales ['ru'] = {
    closeText: 'Закрыть',
    prevText: 'Назад',
    nextText: 'Вперёд',
    monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
    dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Субота'],
    dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    dayNamesMin: ['В', 'П', 'Вт', 'С ', 'Ч', 'П ', 'Сб'],
    weekHeader: 'Неделя',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: '',
    timeOnlyTitle: 'Только время',
    timeText: 'Время',
    hourText: 'Час',
    minuteText: 'Минута',
    secondText: 'Секунда',
    currentText: 'Сегодня',
    ampm: false,
    month: 'Месяц',
    week: 'неделя',
    day: 'День',
    allDayText: 'Весь день'
};


PrimeFacesExt.locales.Timeline['ru'] = {
    'MONTHS': ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
    'MONTHS_SHORT': ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
    'DAYS': ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
    'DAYS_SHORT': ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
    'ZOOM_IN': "Увeличить",
    'ZOOM_OUT': "Умeньшить",
    'MOVE_LEFT': "Сдвинуть налeво",
    'MOVE_RIGHT': "Сдвинуть направо",
    'NEW': "Новый",
    'CREATE_NEW_EVENT': "Создать новоe событиe"
};

function hideError() {
    $('.ui-state-error').removeClass('ui-state-error');
}

function closeEditable(dialogId) {
    PF('\'' + orderSearchDialog + '\'').hide();
    formEdited = false;
}

// init PF SelectOneMenu width if not visible at page load
PrimeFaces.widget.SelectOneMenu.prototype.refresh = function (cfg) {
    var _self = this;
    setTimeout(function () {
        _self.init(cfg);
    }, 10);
};

function myShow(id) {
    jQuery('#' + id).show();
}