-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Мар 06 2015 г., 15:12
-- Версия сервера: 5.6.12-log
-- Версия PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `eaprint`
--
CREATE DATABASE IF NOT EXISTS `eaprint` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `eaprint`;

-- --------------------------------------------------------

--
-- Структура таблицы `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='аккаунты' AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `account`
--

INSERT INTO `account` (`id`, `name`, `enabled`) VALUES
(1, 'starter', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `phoneFirst` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `phoneSecond` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `mobileFirst` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `mobileSecond` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ordersCount` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=43 ;

--
-- Дамп данных таблицы `client`
--

INSERT INTO `client` (`id`, `name`, `phoneFirst`, `phoneSecond`, `mobileFirst`, `mobileSecond`, `email`, `ordersCount`) VALUES
(1, 'Карлос', '8-888-8888888', NULL, '8-888-844', NULL, 'sdfsdfsdf@ma.ru', 2),
(2, 'Тоже Валерка', '', NULL, '+7 (904) 663-22-81', NULL, '', 2),
(3, 'Тот еще Федор', '', NULL, '', NULL, '', 2),
(9, 'Павлик', '', NULL, '', NULL, '', 1),
(11, 'Генацвали', '8 (342) 423-32-33', NULL, '+7 (882) 233-12-21', NULL, '', 0),
(12, 'Перчинка', '', NULL, '+7 (878) 362-25-53', NULL, '', 1),
(14, 'Петров', '', NULL, '', NULL, '', 0),
(15, 'Васечкин', '', NULL, '', NULL, '', 0),
(16, 'Валентин Федорович', '', NULL, '', NULL, '', 0),
(17, 'Генка', '', NULL, '+7 (445) 555-54-33', NULL, '', 0),
(18, 'Винтыч', '', NULL, '', NULL, '', 0),
(19, 'Базилио', '', NULL, '', NULL, '', 0),
(20, 'Мухомор', '8 (455) 734-75-46', NULL, '+7 (546) 456-45-54', NULL, 'gdfg@rwerw.ru', 0),
(21, 'Чумачечий', '', NULL, '', NULL, '', 0),
(22, 'Вазелинчиков', '', NULL, '', NULL, '', 1),
(23, 'Валентин', '', NULL, '+7 (553) 581-31-89', NULL, '', 0),
(26, 'Тереб', '', NULL, '', NULL, '', 0),
(27, 'Акакий', '8 (534) 534-64-36', NULL, '+7 (345) 332-64-57', NULL, '34sdf@fghgfh.ru', 0),
(29, 'Жозефина', '8 (234) 355-64-56', NULL, '+7 (402) 321-23-21', NULL, 'doe3243rer@f44f.ru', 0),
(41, 'Филармон', '8 (324) 322-34-55', NULL, '+7 (989) 223-43-24', NULL, 'doerer@ff.ru', 0),
(42, 'Тетка с бородой', '', NULL, '', NULL, '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `client_source`
--

CREATE TABLE IF NOT EXISTS `client_source` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='источник, откуда клиент узнал о типографии' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `phoneFirst` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `phoneSecond` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `phoneThird` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='сотрудник типографии' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `lamination`
--

CREATE TABLE IF NOT EXISTS `lamination` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `price` decimal(10,2) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='виды ламинации' AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `lamination`
--

INSERT INTO `lamination` (`id`, `name`, `price`) VALUES
(1, 'глянец', '3.00'),
(2, 'мат', '4.00');

-- --------------------------------------------------------

--
-- Структура таблицы `machine`
--

CREATE TABLE IF NOT EXISTS `machine` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `work` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='оборудование' AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `machine`
--

INSERT INTO `machine` (`id`, `name`, `work`) VALUES
(1, 'принтер', 1),
(2, 'резчик', NULL),
(3, 'ламинатор', 3),
(4, 'лаковщик', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `material`
--

CREATE TABLE IF NOT EXISTS `material` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='все материалы' AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `material`
--

INSERT INTO `material` (`id`, `name`, `price`, `width`, `height`) VALUES
(1, 'бумажка 5х3', '1.00', 5, 3),
(2, 'бумажка 4х3', '2.00', 4, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderId` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'идентификатор заказа',
  `clientId` int(10) unsigned NOT NULL,
  `itemCount` int(10) unsigned NOT NULL DEFAULT '0',
  `timeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dateCreated` date NOT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `cost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `comment` text COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `FK_6lkg0gij43h4fuca4mx56q2lh` (`clientId`),
  KEY `FK_qro50btxtakk2eg9v13c1se48` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=63 ;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `orderId`, `clientId`, `itemCount`, `timeCreated`, `dateCreated`, `status`, `cost`, `comment`) VALUES
(46, '150246', 9, 1, '2015-02-11 12:25:56', '2015-02-11', 4, '2.00', NULL),
(47, '150247', 9, 2, '2015-02-11 12:36:49', '2015-02-11', 4, '451.00', NULL),
(49, '150248', 2, 1, '2015-02-15 12:53:06', '2015-02-15', 4, '5.00', NULL),
(50, '150250', 12, 1, '2015-02-15 13:11:18', '2015-02-15', 4, '344.00', NULL),
(51, '150251', 9, 1, '2015-02-15 13:19:10', '2015-02-15', 4, '333.00', NULL),
(52, '150252', 1, 2, '2015-02-15 13:20:13', '2015-02-15', 4, '44.00', NULL),
(53, '150253', 3, 1, '2015-02-15 13:24:30', '2015-02-15', 4, '7.00', NULL),
(54, '150254', 2, 1, '2015-02-15 13:47:12', '2015-02-15', 4, '4.00', NULL),
(55, '150255', 3, 2, '2015-02-15 18:43:35', '2015-02-15', 4, '671.00', NULL),
(56, '150256', 22, 2, '2015-02-15 20:34:42', '2015-02-16', 4, '4560.00', NULL),
(57, '150247', 1, 0, '2015-02-17 12:51:07', '2015-02-17', 4, '0.00', NULL),
(59, '150259', 3, 0, '2015-02-24 20:53:14', '2015-02-24', 4, '0.00', NULL),
(60, '150260', 2, 0, '2015-02-24 20:54:45', '2015-02-24', 4, '0.00', NULL),
(61, '150261', 3, 0, '2015-02-24 21:03:20', '2015-02-25', 4, '0.00', NULL),
(62, '150262', 12, 0, '2015-02-24 21:04:20', '2015-02-25', 4, '0.00', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `order_amount_level`
--

CREATE TABLE IF NOT EXISTS `order_amount_level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service` int(10) unsigned NOT NULL,
  `amount` int(10) unsigned NOT NULL,
  `price` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `service` (`service`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `order_banner`
--

CREATE TABLE IF NOT EXISTS `order_banner` (
  `id` int(10) unsigned NOT NULL,
  `material` int(10) unsigned NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `print` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_5tscd3m0yxqcb6wqg9cktdy5o` (`material`),
  KEY `FK_qoe6dhffak6lj3kfv5ay5jgf9` (`print`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `order_banner`
--

INSERT INTO `order_banner` (`id`, `material`, `width`, `height`, `print`) VALUES
(45, 2, 44, 43, 41),
(46, 2, 4, 4, 42),
(47, 2, 4, 4, 43),
(48, 2, 4, 4, 44),
(49, 2, 7, 7, 45),
(77, 1, 1, 1, 97);

-- --------------------------------------------------------

--
-- Структура таблицы `order_banner_aud`
--

CREATE TABLE IF NOT EXISTS `order_banner_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `material` int(11) DEFAULT NULL,
  `print` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `order_booklet`
--

CREATE TABLE IF NOT EXISTS `order_booklet` (
  `id` int(10) unsigned NOT NULL,
  `material` int(10) unsigned NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `facePrint` int(10) unsigned NOT NULL,
  `backPrint` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_nss4c6cvhtsvh3cr2f76qac0g` (`backPrint`),
  KEY `FK_n5591xyadh2d00s9tt6tlxtgc` (`facePrint`),
  KEY `FK_8kgo5j8mct9ft0i5usnpvdkx6` (`material`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `order_booklet_aud`
--

CREATE TABLE IF NOT EXISTS `order_booklet_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `backPrint` int(11) DEFAULT NULL,
  `facePrint` int(11) DEFAULT NULL,
  `material` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `order_brochure`
--

CREATE TABLE IF NOT EXISTS `order_brochure` (
  `id` int(10) unsigned NOT NULL,
  `width` int(10) unsigned DEFAULT NULL,
  `height` int(10) unsigned DEFAULT NULL,
  `coverMaterial` int(10) unsigned NOT NULL,
  `coverFacePrint` int(10) unsigned DEFAULT NULL,
  `coverBackPrint` int(10) unsigned DEFAULT NULL,
  `coverLamination` int(10) unsigned DEFAULT NULL,
  `blockMaterial` int(10) unsigned NOT NULL,
  `pageCount` int(10) unsigned NOT NULL,
  `coverBackMaterial` int(10) unsigned NOT NULL,
  `coverBackFacePrint` int(10) unsigned DEFAULT NULL,
  `coverBackBackPrint` int(10) unsigned DEFAULT NULL,
  `coverBackLamination` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_6unlie0wfcp6tke151c6x8a24` (`blockMaterial`),
  KEY `FK_su8krcdg77ayh74g1v2ik6djg` (`coverBackBackPrint`),
  KEY `FK_mcac35w08fr2sroijhl0eadv1` (`coverBackFacePrint`),
  KEY `FK_f2fy60t95sj6s4kktmfk68jih` (`coverBackLamination`),
  KEY `FK_sckj81qm00ul4l9p23vscrtib` (`coverBackMaterial`),
  KEY `FK_7253tn9h3ci8neovixcef0p0l` (`coverBackPrint`),
  KEY `FK_tq3kxmdyw52agg3stys4g87m0` (`coverFacePrint`),
  KEY `FK_jqju0r05ljdum564oh8fn1qfn` (`coverLamination`),
  KEY `FK_b7b4tloc1cs16huwiiux0a4kr` (`coverMaterial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `order_brochure`
--

INSERT INTO `order_brochure` (`id`, `width`, `height`, `coverMaterial`, `coverFacePrint`, `coverBackPrint`, `coverLamination`, `blockMaterial`, `pageCount`, `coverBackMaterial`, `coverBackFacePrint`, `coverBackBackPrint`, `coverBackLamination`) VALUES
(51, 6, 6, 2, 51, 50, NULL, 1, 66, 1, 49, 48, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `order_brochure_aud`
--

CREATE TABLE IF NOT EXISTS `order_brochure_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `pageCount` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `blockMaterial` int(11) DEFAULT NULL,
  `coverBackBackPrint` int(11) DEFAULT NULL,
  `coverBackFacePrint` int(11) DEFAULT NULL,
  `coverBackLamination` int(11) DEFAULT NULL,
  `coverBackMaterial` int(11) DEFAULT NULL,
  `coverBackPrint` int(11) DEFAULT NULL,
  `coverFacePrint` int(11) DEFAULT NULL,
  `coverLamination` int(11) DEFAULT NULL,
  `coverMaterial` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `order_calendar`
--

CREATE TABLE IF NOT EXISTS `order_calendar` (
  `id` int(10) unsigned NOT NULL COMMENT 'ссылка на позицию заказа',
  `width` int(10) unsigned DEFAULT NULL,
  `height` int(10) unsigned DEFAULT NULL,
  `coverMaterial` int(10) unsigned NOT NULL,
  `coverFacePrint` int(10) unsigned NOT NULL,
  `coverBackPrint` int(10) unsigned NOT NULL,
  `coverLamination` int(10) unsigned DEFAULT NULL,
  `blockMaterial` int(10) unsigned NOT NULL,
  `pageCount` int(10) unsigned DEFAULT NULL,
  `coverBackMaterial` int(10) unsigned NOT NULL,
  `coverBackFacePrint` int(10) unsigned NOT NULL,
  `coverBackBackPrint` int(10) unsigned NOT NULL,
  `coverBackLamination` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_fcqtqwc2csghlf756wffrcqnx` (`blockMaterial`),
  KEY `FK_9b8xh8j989vxygag4nawil5r5` (`coverBackBackPrint`),
  KEY `FK_px08kqkr496gk9pgrk7jk8ndt` (`coverBackFacePrint`),
  KEY `FK_fjgc4h9wh9w629mg3mm0ts6r9` (`coverBackLamination`),
  KEY `FK_njkj1rxexp115t5mt2viy42v0` (`coverBackMaterial`),
  KEY `FK_mdrm2tecapmkr7dr8c9uwlhh6` (`coverBackPrint`),
  KEY `FK_4p1nuxp2g7aaov3sij0uqv69m` (`coverFacePrint`),
  KEY `FK_pkimnpgan87rfnriheky1ixhj` (`coverLamination`),
  KEY `FK_pc7f8gi6klh5fycj3rc7a8x1f` (`coverMaterial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `order_calendar_aud`
--

CREATE TABLE IF NOT EXISTS `order_calendar_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `pageCount` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `blockMaterial` int(11) DEFAULT NULL,
  `coverBackBackPrint` int(11) DEFAULT NULL,
  `coverBackFacePrint` int(11) DEFAULT NULL,
  `coverBackLamination` int(11) DEFAULT NULL,
  `coverBackMaterial` int(11) DEFAULT NULL,
  `coverBackPrint` int(11) DEFAULT NULL,
  `coverFacePrint` int(11) DEFAULT NULL,
  `coverLamination` int(11) DEFAULT NULL,
  `coverMaterial` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `order_format`
--

CREATE TABLE IF NOT EXISTS `order_format` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderItem` int(10) unsigned NOT NULL COMMENT 'ссылка на позицию заказа',
  `service` int(10) unsigned NOT NULL COMMENT 'ссылка на услугу',
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sshf64otngtfbin1fkahjcmvg` (`service`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='данные о форматах изделий в заказах' AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `order_format`
--

INSERT INTO `order_format` (`id`, `orderItem`, `service`, `width`, `height`) VALUES
(1, 111, 1, 3, 4),
(2, 112, 1, 4, 4),
(3, 113, 1, 3, 3),
(4, 114, 1, 4, 7),
(5, 115, 1, 2, 2),
(6, 116, 1, 2, 2),
(7, 117, 1, 2, 2),
(8, 118, 1, 3, 3),
(9, 119, 1, 3, 3),
(10, 120, 1, 3, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `order_item`
--

CREATE TABLE IF NOT EXISTS `order_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderId` int(10) unsigned NOT NULL,
  `serviceId` int(10) unsigned NOT NULL COMMENT 'ссылка на услугу',
  `status` int(10) unsigned NOT NULL,
  `amount` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'тираж',
  `cost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `comment` text COLLATE utf8_bin,
  `timeCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dateCreated` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_knuoabgwovh7grap9bvoflfgc` (`orderId`),
  KEY `FK_3otbcf36co1l7evsd31rdy23v` (`serviceId`),
  KEY `FK_ph7ljaxde0kitm63h8q9orw8r` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=86 ;

--
-- Дамп данных таблицы `order_item`
--

INSERT INTO `order_item` (`id`, `orderId`, `serviceId`, `status`, `amount`, `cost`, `comment`, `timeCreated`, `dateCreated`) VALUES
(41, 46, 7, 1, 2, '2.00', NULL, '2015-02-21 13:55:21', '2015-02-03'),
(42, 47, 7, 1, 34, '445.00', NULL, '2015-02-21 13:55:21', '2015-02-02'),
(43, 47, 1, 1, 5, '6.00', NULL, '2015-02-21 13:55:21', '2015-02-04'),
(44, 49, 7, 1, 5, '5.00', NULL, '2015-02-21 13:55:21', '2015-02-02'),
(45, 50, 6, 1, 3443, '344.00', NULL, '2015-02-21 13:55:21', '2015-02-02'),
(46, 51, 6, 1, 34, '333.00', NULL, '2015-02-21 13:55:21', '2015-02-02'),
(47, 52, 6, 1, 4, '44.00', NULL, '2015-02-21 13:55:21', '2015-02-02'),
(48, 52, 6, 1, 4, '44.00', NULL, '2015-02-21 13:55:21', '2015-02-02'),
(49, 53, 6, 1, 7, '7.00', NULL, '2015-02-21 13:55:21', '2015-02-02'),
(50, 54, 9, 1, 4, '4.00', NULL, '2015-02-21 13:55:21', '2015-02-02'),
(51, 55, 8, 1, 56, '666.00', NULL, '2015-02-21 13:55:21', '2015-02-02'),
(52, 55, 7, 1, 5, '5.00', NULL, '2015-02-21 13:55:21', '2015-02-02'),
(65, 56, 13, 3, 51, '57.00', NULL, '2015-02-21 13:55:21', '2015-02-02'),
(69, 56, 13, 10, 64, '4.00', NULL, '2015-02-21 13:55:21', '2015-02-01'),
(70, 56, 13, 5, 41, '4.00', NULL, '2015-02-21 13:55:21', '2015-02-02'),
(71, 57, 7, 2, 1, '1.00', 'что тут сказать, пиздец, сделайте визитку нахуй!', '2015-02-21 13:55:21', '2015-02-02'),
(72, 57, 13, 2, 7, '7.00', NULL, '2015-02-21 16:57:47', '2015-02-21'),
(73, 57, 13, 2, 7, '7.00', NULL, '2015-02-21 16:58:38', '2015-02-21'),
(74, 57, 13, 2, 4, '4.00', NULL, '2015-02-21 17:00:00', '2015-02-21'),
(75, 57, 13, 2, 6, '555.00', NULL, '2015-02-21 17:01:27', '2015-02-21'),
(76, 57, 13, 1, 7, '7.00', NULL, '2015-02-21 17:32:09', '2015-02-21'),
(77, 57, 6, 1, 1, '11.00', NULL, '2015-02-21 22:08:45', '2015-02-22'),
(78, 55, 1, 1, 1, '1.00', '21323', '2015-02-24 13:24:33', '2015-02-24'),
(79, 57, 13, 1, 411, '4.00', NULL, '2015-02-24 17:14:56', '2015-02-24'),
(82, 59, 13, 1, 2, '2.00', NULL, '2015-02-24 20:52:13', '2015-02-24'),
(83, 60, 13, 1, 4, '4.00', NULL, '2015-02-24 20:54:26', '2015-02-24'),
(84, 61, 13, 1, 3, '3.00', NULL, '2015-02-24 21:03:13', '2015-02-25'),
(85, 62, 13, 1, 6, '6.00', NULL, '2015-02-24 21:04:12', '2015-02-25');

-- --------------------------------------------------------

--
-- Структура таблицы `order_item_aud`
--

CREATE TABLE IF NOT EXISTS `order_item_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `REVEND` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `cost` decimal(19,2) DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL,
  `timeCreated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FK_2gp3lyv19e4cech3qcjv3myuf` (`REV`),
  KEY `FK_7vi371241geas0l5gg9r8u50o` (`REVEND`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `order_item_aud`
--

INSERT INTO `order_item_aud` (`id`, `REV`, `REVTYPE`, `REVEND`, `amount`, `comment`, `cost`, `dateCreated`, `timeCreated`, `status`) VALUES
(80, 1, 0, 2, 3, NULL, '3.00', '2015-02-24 23:10:43', '2015-02-24 23:10:43', 1),
(80, 2, 1, 3, 13, NULL, '3.00', '2015-02-24 00:00:00', '2015-02-24 23:10:43', 1),
(80, 3, 1, 4, 13, NULL, '3.00', '2015-02-24 00:00:00', '2015-02-24 23:10:43', 1),
(80, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(81, 5, 0, 6, 5, NULL, '5.00', '2015-02-24 23:13:39', '2015-02-24 23:13:39', 1),
(81, 6, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, 7, 0, NULL, 2, NULL, '2.00', '2015-02-24 23:52:13', '2015-02-24 23:52:13', 1),
(83, 8, 0, NULL, 4, NULL, '4.00', '2015-02-24 23:54:26', '2015-02-24 23:54:26', 1),
(84, 9, 0, NULL, 3, NULL, '3.00', '2015-02-25 00:03:13', '2015-02-25 00:03:13', 1),
(85, 10, 0, NULL, 6, NULL, '6.00', '2015-02-25 00:04:12', '2015-02-25 00:04:12', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `order_item_status`
--

CREATE TABLE IF NOT EXISTS `order_item_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_bin NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=17 ;

--
-- Дамп данных таблицы `order_item_status`
--

INSERT INTO `order_item_status` (`id`, `code`, `name`) VALUES
(1, 'NEW', 'Новый'),
(2, 'COMPLETE', 'Выполнен'),
(3, 'GIVEN_TO_CLIENT', 'Выдан клиенту'),
(5, 'ON_AGREEMENT', 'На согласовании'),
(6, 'DESIGN_APPROVED', 'Дизайн утвержден'),
(7, 'DESIGN_NOT_APPROVED', 'Дизайн не утвержден'),
(8, 'INVOICE_TO_PAY', 'Счет к оплате'),
(9, 'TO_PRE_PRESS', 'Передан в допечатную обработку'),
(10, 'IN_PRE_PRESS', 'Принято - допечатная обработка'),
(11, 'READY_PRE_PRESS', 'Готово - допечатная обработка'),
(12, 'IN_PRESS', 'Принято - печать'),
(13, 'REDY_PRESS', 'Готово - печать'),
(14, 'IN_POST_PRESS', 'Принято - постпечатная обработка'),
(15, 'REAY_POST_PRESS', 'Готово - постпечатная обработка'),
(16, 'CANCEL', 'Отмена');

-- --------------------------------------------------------

--
-- Структура таблицы `order_item_status_link`
--

CREATE TABLE IF NOT EXISTS `order_item_status_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderItemStatusId` int(10) unsigned DEFAULT NULL,
  `nextOrderItemStatusId` int(10) unsigned DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_6gacphmy4uf6hcgxja3vd22n6` (`nextOrderItemStatusId`),
  KEY `FK_aye3ynrw2mmo31qqyl361stx1` (`orderItemStatusId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='иерархия статусов заказа' AUTO_INCREMENT=24 ;

--
-- Дамп данных таблицы `order_item_status_link`
--

INSERT INTO `order_item_status_link` (`id`, `orderItemStatusId`, `nextOrderItemStatusId`, `comment`, `name`) VALUES
(1, 1, 5, 'новый - на согласовании', ''),
(2, 5, 6, 'на согласовании - дизайн утвержден', ''),
(3, 5, 7, 'на согласовании - дизайн не утвержден', ''),
(4, 6, 8, 'Дизайн утвержден - счет к оплате', ''),
(5, 7, 6, 'диз не утв - диз утв', ''),
(15, 8, 9, 'счет - передано в prepress', ''),
(16, 9, 10, 'передано prepress - prepress accept', ''),
(17, 10, 11, 'prepress accept - prepress ready', ''),
(18, 11, 12, 'prepress ready - press accept', ''),
(19, 12, 13, 'press accept - press ready', ''),
(20, 13, 14, 'press ready - postpress accept', ''),
(21, 15, 2, 'postpress ready - complete', ''),
(22, 2, 3, 'complete - client', ''),
(23, 14, 15, 'postpress accept - pstpress ready', '');

-- --------------------------------------------------------

--
-- Структура таблицы `order_item_status_link_security`
--

CREATE TABLE IF NOT EXISTS `order_item_status_link_security` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderItemStatusLinkId` int(10) unsigned DEFAULT NULL,
  `roleId` int(10) unsigned NOT NULL COMMENT 'роль, которой доступен этот переход',
  PRIMARY KEY (`id`),
  KEY `FK_k2l8pbnpy7shyh7r0ej7odix` (`orderItemStatusLinkId`),
  KEY `FK_jty86tsq6c5uhfv4fan9umx48` (`roleId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='какой роли доступен переход из статуса в статус' AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `order_item_status_link_security`
--

INSERT INTO `order_item_status_link_security` (`id`, `orderItemStatusLinkId`, `roleId`) VALUES
(1, 1, 4),
(2, 2, 5),
(3, 3, 5),
(4, 4, 4),
(5, 5, 5),
(6, 15, 4),
(7, 16, 5),
(8, 17, 5),
(9, 18, 6),
(10, 19, 6),
(11, 20, 7),
(12, 22, 7),
(13, 22, 4),
(14, 23, 7);

-- --------------------------------------------------------

--
-- Структура таблицы `order_lamination_work`
--

CREATE TABLE IF NOT EXISTS `order_lamination_work` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lamination` int(10) unsigned NOT NULL COMMENT 'ссылка на тип ламинации',
  `isBothSide` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_ai0upv5a0cl5pu7htjcrir570` (`lamination`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='данные о работах по ламинации' AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `order_lamination_work`
--

INSERT INTO `order_lamination_work` (`id`, `lamination`, `isBothSide`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `order_lamination_work_aud`
--

CREATE TABLE IF NOT EXISTS `order_lamination_work_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `REVEND` int(11) DEFAULT NULL,
  `isBothSide` bit(1) DEFAULT NULL,
  `lamination` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FK_ngt63eqv0o87qrnau7y8waj4c` (`REV`),
  KEY `FK_bblq40c4mhg5af32e7folmpvg` (`REVEND`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `order_leaflet`
--

CREATE TABLE IF NOT EXISTS `order_leaflet` (
  `id` int(10) unsigned NOT NULL,
  `material` int(10) unsigned NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `facePrint` int(10) unsigned NOT NULL,
  `backPrint` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_13ffu6lxagj2peipjb4agbxho` (`backPrint`),
  KEY `FK_j3g561nfp2298fqjj3ehl1k9u` (`facePrint`),
  KEY `FK_q8tho2a4mn79co4kdn17gqjhh` (`material`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `order_leaflet`
--

INSERT INTO `order_leaflet` (`id`, `material`, `width`, `height`, `facePrint`, `backPrint`) VALUES
(50, 1, 5, 5, 47, 46);

-- --------------------------------------------------------

--
-- Структура таблицы `order_leaflet_aud`
--

CREATE TABLE IF NOT EXISTS `order_leaflet_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `backPrint` int(11) DEFAULT NULL,
  `facePrint` int(11) DEFAULT NULL,
  `material` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `order_material`
--

CREATE TABLE IF NOT EXISTS `order_material` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderItem` int(10) unsigned NOT NULL COMMENT 'ссылка на позицию заказа',
  `service` int(10) unsigned NOT NULL COMMENT 'ссылка на услугу',
  `material` int(10) unsigned NOT NULL COMMENT 'ссылка на материал',
  PRIMARY KEY (`id`),
  KEY `FK_o9c0sejobk03k7fl0uc9ftvdr` (`material`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='данные о материале в заказе для услуги' AUTO_INCREMENT=22 ;

--
-- Дамп данных таблицы `order_material`
--

INSERT INTO `order_material` (`id`, `orderItem`, `service`, `material`) VALUES
(1, 114, 3, 1),
(2, 114, 4, 1),
(3, 114, 5, 2),
(4, 115, 3, 2),
(5, 115, 4, 2),
(6, 115, 5, 2),
(7, 116, 3, 2),
(8, 116, 4, 1),
(9, 116, 5, 1),
(10, 117, 3, 2),
(11, 117, 4, 2),
(12, 117, 5, 2),
(13, 118, 3, 2),
(14, 118, 4, 2),
(15, 118, 5, 2),
(16, 119, 3, 1),
(17, 119, 4, 2),
(18, 119, 5, 2),
(19, 120, 3, 1),
(20, 120, 4, 1),
(21, 120, 5, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `order_notebook`
--

CREATE TABLE IF NOT EXISTS `order_notebook` (
  `id` int(10) unsigned NOT NULL COMMENT 'ссылка на позицию заказа',
  `width` int(10) unsigned DEFAULT NULL,
  `height` int(10) unsigned DEFAULT NULL,
  `coverMaterial` int(10) unsigned NOT NULL,
  `coverFacePrint` int(10) unsigned NOT NULL,
  `coverBackPrint` int(10) unsigned NOT NULL,
  `coverLamination` int(10) unsigned DEFAULT NULL,
  `blockMaterial` int(10) unsigned NOT NULL,
  `pageCount` int(10) unsigned DEFAULT NULL,
  `coverBackMaterial` int(10) unsigned NOT NULL,
  `coverBackFacePrint` int(10) unsigned NOT NULL,
  `coverBackBackPrint` int(10) unsigned NOT NULL,
  `coverBackLamination` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_mxvf874pkjq90u7egfq0cdqcu` (`blockMaterial`),
  KEY `FK_dqfle07xokek0b8l0yb34pvay` (`coverBackLamination`),
  KEY `FK_fs43vgch59tg4a9n0bwimwljo` (`coverBackMaterial`),
  KEY `FK_4fc3abpwiylniv48xrdewxndu` (`coverLamination`),
  KEY `FK_he37omgcidxtkpkkno3b5xnxq` (`coverMaterial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `order_notebook`
--

INSERT INTO `order_notebook` (`id`, `width`, `height`, `coverMaterial`, `coverFacePrint`, `coverBackPrint`, `coverLamination`, `blockMaterial`, `pageCount`, `coverBackMaterial`, `coverBackFacePrint`, `coverBackBackPrint`, `coverBackLamination`) VALUES
(43, 6, 6, 1, 38, 37, NULL, 1, 5, 1, 36, 35, NULL),
(78, 1, 1, 1, 101, 100, NULL, 1, 11, 1, 99, 98, NULL),
(133, 455, 455, 2, 0, 0, NULL, 1, 455, 1, 0, 0, NULL),
(141, 455, 45, 2, 0, 0, NULL, 2, 455, 2, 0, 0, NULL),
(148, 233, 33, 2, 0, 0, NULL, 1, 344, 1, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `order_notebook_aud`
--

CREATE TABLE IF NOT EXISTS `order_notebook_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `pageCount` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `blockMaterial` int(11) DEFAULT NULL,
  `coverBackBackPrint` int(11) DEFAULT NULL,
  `coverBackFacePrint` int(11) DEFAULT NULL,
  `coverBackLamination` int(11) DEFAULT NULL,
  `coverBackMaterial` int(11) DEFAULT NULL,
  `coverBackPrint` int(11) DEFAULT NULL,
  `coverFacePrint` int(11) DEFAULT NULL,
  `coverLamination` int(11) DEFAULT NULL,
  `coverMaterial` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `order_page_count`
--

CREATE TABLE IF NOT EXISTS `order_page_count` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderItem` int(10) unsigned NOT NULL COMMENT 'ссылка на позицию заказа',
  `service` int(10) unsigned NOT NULL COMMENT 'ссылка на услугу',
  `pageCount` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='данные о количестве страниц в заказе для услуги' AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `order_page_count`
--

INSERT INTO `order_page_count` (`id`, `orderItem`, `service`, `pageCount`) VALUES
(1, 111, 4, 3),
(2, 112, 4, 2),
(3, 113, 4, 2),
(4, 114, 4, 2),
(5, 115, 4, 1),
(6, 116, 4, 2),
(7, 117, 4, 1),
(8, 118, 4, 2),
(9, 119, 4, 2),
(10, 120, 4, 324);

-- --------------------------------------------------------

--
-- Структура таблицы `order_print_work`
--

CREATE TABLE IF NOT EXISTS `order_print_work` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `noPrint` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'без печати',
  `isFullColor` tinyint(4) DEFAULT NULL,
  `bw` tinyint(1) NOT NULL DEFAULT '0',
  `c` tinyint(1) NOT NULL DEFAULT '0',
  `m` tinyint(1) NOT NULL DEFAULT '0',
  `y` tinyint(1) NOT NULL DEFAULT '0',
  `k` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='данные о печати для услуг заказа' AUTO_INCREMENT=109 ;

--
-- Дамп данных таблицы `order_print_work`
--

INSERT INTO `order_print_work` (`id`, `noPrint`, `isFullColor`, `bw`, `c`, `m`, `y`, `k`) VALUES
(1, 0, 0, 0, 0, 0, 0, 0),
(2, 0, 0, 0, 1, 1, 0, 0),
(3, 0, 0, 0, 1, 0, 1, 0),
(4, 0, 0, 0, 0, 1, 0, 1),
(5, 0, 0, 1, 0, 0, 0, 0),
(9, 0, 0, 0, 0, 0, 0, 0),
(10, 0, 1, 0, 0, 0, 0, 0),
(11, 0, 0, 0, 0, 0, 0, 0),
(12, 0, 1, 0, 0, 0, 0, 0),
(13, 0, 0, 0, 0, 0, 0, 0),
(14, 0, 1, 0, 0, 0, 0, 0),
(15, 0, 0, 0, 0, 0, 0, 0),
(16, 0, 1, 0, 0, 0, 0, 0),
(17, 0, 0, 0, 0, 0, 0, 0),
(18, 0, 0, 1, 0, 0, 0, 0),
(19, 0, 0, 0, 0, 0, 0, 0),
(20, 0, 1, 0, 1, 1, 0, 0),
(25, 0, 0, 0, 0, 0, 0, 1),
(26, 0, 0, 0, 1, 0, 0, 0),
(27, 0, 0, 0, 0, 1, 1, 0),
(28, 0, 0, 0, 1, 0, 0, 1),
(29, 0, 1, 0, 0, 0, 0, 0),
(30, 0, 0, 0, 1, 0, 0, 1),
(31, 0, 0, 0, 0, 0, 0, 0),
(32, 0, 0, 0, 0, 0, 0, 0),
(33, 0, 0, 0, 0, 0, 0, 0),
(34, 0, 0, 0, 0, 0, 0, 0),
(35, 0, 0, 0, 0, 0, 0, 0),
(36, 0, 0, 0, 0, 0, 0, 0),
(37, 0, 0, 0, 0, 0, 0, 0),
(38, 0, 0, 0, 0, 0, 0, 0),
(39, 0, 0, 0, 0, 0, 0, 0),
(40, 0, 0, 0, 0, 0, 0, 0),
(41, 0, 0, 0, 1, 0, 1, 0),
(42, 0, 0, 0, 1, 1, 0, 1),
(43, 0, 0, 0, 1, 1, 1, 1),
(44, 0, 0, 0, 1, 1, 0, 1),
(45, 0, 0, 0, 1, 1, 1, 1),
(46, 0, 0, 0, 1, 0, 1, 0),
(47, 0, 1, 0, 0, 0, 0, 0),
(48, 1, 0, 0, 0, 0, 0, 0),
(49, 1, 0, 0, 0, 0, 0, 0),
(50, 1, 0, 0, 0, 0, 0, 0),
(51, 1, 0, 0, 0, 0, 0, 0),
(52, 1, 0, 0, 0, 0, 0, 0),
(53, 1, 0, 0, 0, 0, 0, 0),
(54, 1, 0, 0, 0, 0, 0, 0),
(55, 1, 0, 0, 0, 0, 0, 0),
(56, 1, 0, 0, 0, 0, 0, 0),
(57, 1, 0, 0, 0, 0, 0, 0),
(58, 1, 0, 0, 0, 0, 0, 0),
(59, 1, 0, 0, 0, 0, 0, 0),
(60, 1, 0, 0, 0, 0, 0, 0),
(61, 1, 0, 0, 0, 0, 0, 0),
(62, 1, 0, 0, 0, 0, 0, 0),
(63, 1, 0, 0, 0, 0, 0, 0),
(64, 1, 0, 0, 0, 0, 0, 0),
(65, 1, 0, 0, 0, 0, 0, 0),
(66, 1, 0, 0, 0, 0, 0, 0),
(67, 1, 0, 0, 0, 0, 0, 0),
(68, 1, 0, 0, 0, 0, 0, 0),
(69, 1, 0, 0, 0, 0, 0, 0),
(70, 1, 0, 0, 0, 0, 0, 0),
(71, 1, 0, 0, 0, 0, 0, 0),
(72, 1, 0, 0, 0, 0, 0, 0),
(73, 1, 0, 0, 0, 0, 0, 0),
(74, 1, 0, 0, 0, 0, 0, 0),
(75, 1, 0, 0, 0, 0, 0, 0),
(76, 1, 0, 0, 0, 0, 0, 0),
(77, 1, 0, 0, 0, 0, 0, 0),
(78, 1, 0, 0, 0, 0, 0, 0),
(79, 1, 0, 0, 0, 0, 0, 0),
(80, 1, 0, 0, 0, 0, 0, 0),
(81, 1, 0, 0, 0, 0, 0, 0),
(82, 1, 0, 0, 0, 0, 0, 0),
(83, 1, 0, 0, 0, 0, 0, 0),
(84, 1, 0, 0, 0, 0, 0, 0),
(85, 1, 0, 0, 0, 0, 0, 0),
(86, 1, 0, 0, 0, 0, 0, 0),
(87, 1, 0, 0, 0, 0, 0, 0),
(92, 1, 0, 0, 0, 0, 0, 0),
(93, 1, 0, 0, 0, 0, 0, 0),
(94, 1, 0, 0, 0, 0, 0, 0),
(95, 0, 1, 0, 0, 0, 0, 0),
(96, 1, 0, 0, 0, 0, 0, 0),
(97, 1, 0, 0, 0, 0, 0, 0),
(98, 0, 0, 0, 1, 0, 1, 0),
(99, 0, 1, 0, 0, 0, 0, 0),
(100, 0, 0, 0, 1, 0, 1, 0),
(101, 0, 1, 0, 0, 0, 0, 0),
(102, 0, 0, 0, 1, 1, 0, 0),
(103, 1, 0, 0, 0, 0, 0, 0),
(104, 1, 0, 0, 0, 0, 0, 0),
(105, 1, 0, 0, 0, 0, 0, 0),
(106, 1, 0, 0, 0, 0, 0, 0),
(107, 1, 0, 0, 0, 0, 0, 0),
(108, 1, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `order_print_work_aud`
--

CREATE TABLE IF NOT EXISTS `order_print_work_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `REVTYPE` tinyint(4) DEFAULT NULL,
  `REVEND` int(11) DEFAULT NULL,
  `bw` bit(1) DEFAULT NULL,
  `c` bit(1) DEFAULT NULL,
  `isFullColor` bit(1) DEFAULT NULL,
  `k` bit(1) DEFAULT NULL,
  `m` bit(1) DEFAULT NULL,
  `noPrint` bit(1) DEFAULT NULL,
  `y` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`),
  KEY `FK_avcsc1gynhe5xu3ihtkh8479e` (`REV`),
  KEY `FK_jtpomnii8y2ajqd3dbbcj7t6a` (`REVEND`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `order_print_work_aud`
--

INSERT INTO `order_print_work_aud` (`id`, `REV`, `REVTYPE`, `REVEND`, `bw`, `c`, `isFullColor`, `k`, `m`, `noPrint`, `y`) VALUES
(103, 1, 0, NULL, b'0', b'0', b'0', b'0', b'0', b'1', b'0'),
(104, 5, 0, NULL, b'0', b'0', b'0', b'0', b'0', b'1', b'0'),
(105, 7, 0, NULL, b'0', b'0', b'0', b'0', b'0', b'1', b'0'),
(106, 8, 0, NULL, b'0', b'0', b'0', b'0', b'0', b'1', b'0'),
(107, 9, 0, NULL, b'0', b'0', b'0', b'0', b'0', b'1', b'0'),
(108, 10, 0, NULL, b'0', b'0', b'0', b'0', b'0', b'1', b'0');

-- --------------------------------------------------------

--
-- Структура таблицы `order_status`
--

CREATE TABLE IF NOT EXISTS `order_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_bin NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `order_status`
--

INSERT INTO `order_status` (`id`, `code`, `name`) VALUES
(4, 'NEW', 'Новый'),
(5, 'IN_WORK', 'В работе'),
(6, 'COMPLETE', 'Выполнен');

-- --------------------------------------------------------

--
-- Структура таблицы `order_sticker`
--

CREATE TABLE IF NOT EXISTS `order_sticker` (
  `id` int(10) unsigned NOT NULL,
  `material` int(10) unsigned NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `print` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pd1wxrupkfm9dwre3yqxc970x` (`print`),
  KEY `FK_h83c6p5g5ivxp5ekaf8txgu34` (`material`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `order_sticker`
--

INSERT INTO `order_sticker` (`id`, `material`, `width`, `height`, `print`) VALUES
(65, 1, 55, 55, 75),
(69, 1, 4, 4, 79),
(70, 1, 4, 4, 80),
(72, 1, 7, 7, 92),
(73, 1, 7, 7, 93),
(74, 1, 4, 4, 94),
(75, 1, 6, 6, 95),
(76, 1, 7, 7, 96),
(79, 1, 411, 4, 102),
(82, 2, 2, 2, 105),
(83, 1, 4, 4, 106),
(84, 2, 3, 3, 107),
(85, 2, 6, 6, 108);

-- --------------------------------------------------------

--
-- Структура таблицы `order_sticker_aud`
--

CREATE TABLE IF NOT EXISTS `order_sticker_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `material` int(11) DEFAULT NULL,
  `print` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `order_sticker_aud`
--

INSERT INTO `order_sticker_aud` (`id`, `REV`, `height`, `width`, `material`, `print`) VALUES
(80, 1, 3, 3, 1, 103),
(80, 2, 3, 3, 1, 103),
(80, 3, 3, 3, 2, 103),
(80, 4, NULL, NULL, NULL, NULL),
(81, 5, 5, 5, 1, 104),
(81, 6, NULL, NULL, NULL, NULL),
(82, 7, 2, 2, 2, 105),
(83, 8, 4, 4, 1, 106),
(84, 9, 3, 3, 2, 107),
(85, 10, 6, 6, 2, 108);

-- --------------------------------------------------------

--
-- Структура таблицы `order_visiting_card`
--

CREATE TABLE IF NOT EXISTS `order_visiting_card` (
  `id` int(10) unsigned NOT NULL,
  `material` int(10) unsigned NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `facePrint` int(10) unsigned NOT NULL,
  `backPrint` int(10) unsigned NOT NULL,
  `lamination` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_nvg1v8hbodhydsuyu6au7ftm5` (`backPrint`),
  KEY `FK_1ys0dtc75ymbg0kibs22jysjs` (`facePrint`),
  KEY `FK_nw9o2mbatdtpi3cgoae247t3f` (`material`),
  KEY `FK_flf6y6kd1haa8aw29qk5m2rpt` (`lamination`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `order_visiting_card`
--

INSERT INTO `order_visiting_card` (`id`, `material`, `width`, `height`, `facePrint`, `backPrint`, `lamination`) VALUES
(41, 2, 2, 2, 32, 31, 1),
(42, 2, 4, 4, 34, 33, 1),
(44, 1, 5, 5, 40, 39, 1),
(52, 2, 5, 5, 53, 52, 1),
(71, 1, 1, 1, 87, 86, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `order_visiting_card_aud`
--

CREATE TABLE IF NOT EXISTS `order_visiting_card_aud` (
  `id` int(11) NOT NULL,
  `REV` int(11) NOT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `backPrint` int(11) DEFAULT NULL,
  `facePrint` int(11) DEFAULT NULL,
  `lamination` int(11) DEFAULT NULL,
  `material` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `revinfo`
--

CREATE TABLE IF NOT EXISTS `revinfo` (
  `REV` int(11) NOT NULL AUTO_INCREMENT,
  `REVTSTMP` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`REV`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `revinfo`
--

INSERT INTO `revinfo` (`REV`, `REVTSTMP`) VALUES
(1, 1424805057588),
(2, 1424805077565),
(3, 1424805097372),
(4, 1424805120257),
(5, 1424805225288),
(6, 1424805330769),
(7, 1424807593638),
(8, 1424807684900),
(9, 1424808200365),
(10, 1424808260205);

-- --------------------------------------------------------

--
-- Структура таблицы `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_8sewwnpamngi6b1dwaa88askk` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'ROLE_ANONYMOUS'),
(4, 'ROLE_MANAGER'),
(7, 'ROLE_POSTPRESS'),
(5, 'ROLE_PREPRESS'),
(6, 'ROLE_PRESS'),
(3, 'ROLE_SA');

-- --------------------------------------------------------

--
-- Структура таблицы `service`
--

CREATE TABLE IF NOT EXISTS `service` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `service`
--

INSERT INTO `service` (`id`, `name`, `code`) VALUES
(1, 'Блокнот', 'NOTEBOOK'),
(6, 'Баннер', 'BANNER'),
(7, 'Визитка', 'VISITING_CARD'),
(8, 'Брошюра', 'BROCHURE'),
(9, 'Листовка', 'LEAFLET'),
(10, 'Буклет', 'BOOKLET'),
(11, 'Календарь', 'CALENDAR'),
(13, 'Наклейка', 'STICKER');

-- --------------------------------------------------------

--
-- Структура таблицы `service_attribute`
--

CREATE TABLE IF NOT EXISTS `service_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `code` varchar(255) NOT NULL,
  `isRequired` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `service_attribute`
--

INSERT INTO `service_attribute` (`id`, `name`, `code`, `isRequired`) VALUES
(1, 'формат', 'format', 1),
(2, 'материал', 'material', 1),
(3, 'цвет пружины', '', 0),
(4, 'материал пружины', '', 0),
(8, 'количество страниц', 'pageCount', 1),
(9, 'разрешение', '', 0),
(10, 'тираж', 'amount', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `service_service_attribute`
--

CREATE TABLE IF NOT EXISTS `service_service_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service` int(10) unsigned NOT NULL,
  `attribute` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service` (`service`,`attribute`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `service_service_attribute`
--

INSERT INTO `service_service_attribute` (`id`, `service`, `attribute`) VALUES
(1, 1, 1),
(8, 1, 10),
(2, 3, 2),
(3, 4, 2),
(5, 4, 8),
(4, 5, 2),
(9, 6, 1),
(10, 6, 2),
(11, 6, 10),
(6, 7, 1),
(7, 7, 2),
(14, 7, 10),
(15, 8, 1),
(16, 8, 10),
(17, 14, 2),
(18, 15, 2),
(19, 15, 8),
(20, 16, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `service_service_work`
--

CREATE TABLE IF NOT EXISTS `service_service_work` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service` int(10) unsigned NOT NULL,
  `work` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service` (`service`,`work`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `service_service_work`
--

INSERT INTO `service_service_work` (`id`, `service`, `work`) VALUES
(1, 3, 1),
(4, 3, 3),
(3, 5, 1),
(6, 5, 3),
(7, 6, 4),
(8, 7, 1),
(9, 7, 2),
(10, 7, 3),
(15, 8, 1),
(16, 8, 3),
(17, 9, 1),
(11, 14, 1),
(12, 14, 3),
(13, 16, 1),
(14, 16, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `service_work`
--

CREATE TABLE IF NOT EXISTS `service_work` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `service_work`
--

INSERT INTO `service_work` (`id`, `name`, `code`) VALUES
(1, 'Печать', 'print'),
(2, 'Лак', 'varnish'),
(3, 'Ламинация', 'lamination'),
(4, 'Монтаж', 'mount');

-- --------------------------------------------------------

--
-- Структура таблицы `service_work_price`
--

CREATE TABLE IF NOT EXISTS `service_work_price` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `work` int(10) unsigned NOT NULL,
  `amountFrom` int(10) unsigned NOT NULL,
  `amountTo` int(10) unsigned NOT NULL,
  `price` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `service_work_price`
--

INSERT INTO `service_work_price` (`id`, `work`, `amountFrom`, `amountTo`, `price`) VALUES
(1, 1, 1, 100, 20);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `created` date DEFAULT NULL,
  `modified` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_gj2fy3dcix7ph7k8684gka40c` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `name`, `password`, `enabled`, `email`, `created`, `modified`) VALUES
(1, 'igor', '$2a$10$1sxNgoClFGZT64cnWWwb7uiq7zZCK6ZVv5fjO2hX5SSNMc5qbRCge', 1, NULL, NULL, NULL),
(2, 'manager', '$2a$10$1sxNgoClFGZT64cnWWwb7uiq7zZCK6ZVv5fjO2hX5SSNMc5qbRCge', 1, NULL, NULL, NULL),
(3, 'prepress', '$2a$10$1sxNgoClFGZT64cnWWwb7uiq7zZCK6ZVv5fjO2hX5SSNMc5qbRCge', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user_role`
--

CREATE TABLE IF NOT EXISTS `user_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `roleId` int(10) unsigned NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `roleId` (`roleId`),
  KEY `FK_oj50qpdthexxxmvur61ggy2fb` (`userId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `user_role`
--

INSERT INTO `user_role` (`id`, `userId`, `roleId`, `role_id`, `user_id`) VALUES
(3, 1, 3, NULL, NULL),
(4, 2, 4, NULL, NULL),
(5, 3, 5, NULL, NULL);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FK_6lkg0gij43h4fuca4mx56q2lh` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `FK_qro50btxtakk2eg9v13c1se48` FOREIGN KEY (`status`) REFERENCES `order_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_amount_level`
--
ALTER TABLE `order_amount_level`
  ADD CONSTRAINT `FK_49c4fl0xtp1ag13xoieapkv07` FOREIGN KEY (`service`) REFERENCES `service` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_banner`
--
ALTER TABLE `order_banner`
  ADD CONSTRAINT `FK_5tscd3m0yxqcb6wqg9cktdy5o` FOREIGN KEY (`material`) REFERENCES `material` (`id`),
  ADD CONSTRAINT `FK_aw3j65l7x3rpcaid3qieu3omi` FOREIGN KEY (`id`) REFERENCES `order_item` (`id`),
  ADD CONSTRAINT `FK_qoe6dhffak6lj3kfv5ay5jgf9` FOREIGN KEY (`print`) REFERENCES `order_print_work` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_banner_aud`
--
ALTER TABLE `order_banner_aud`
  ADD CONSTRAINT `FK_cekxcsk5atgqu6rdqa4mr431u` FOREIGN KEY (`id`, `REV`) REFERENCES `order_item_aud` (`id`, `REV`);

--
-- Ограничения внешнего ключа таблицы `order_booklet`
--
ALTER TABLE `order_booklet`
  ADD CONSTRAINT `FK_8kgo5j8mct9ft0i5usnpvdkx6` FOREIGN KEY (`material`) REFERENCES `material` (`id`),
  ADD CONSTRAINT `FK_k1o9gdfqk3oemkxmi3l4cnsf5` FOREIGN KEY (`id`) REFERENCES `order_item` (`id`),
  ADD CONSTRAINT `FK_n5591xyadh2d00s9tt6tlxtgc` FOREIGN KEY (`facePrint`) REFERENCES `order_print_work` (`id`),
  ADD CONSTRAINT `FK_nss4c6cvhtsvh3cr2f76qac0g` FOREIGN KEY (`backPrint`) REFERENCES `order_print_work` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_booklet_aud`
--
ALTER TABLE `order_booklet_aud`
  ADD CONSTRAINT `FK_pk1syvcf6yd4a2jsuoghopffy` FOREIGN KEY (`id`, `REV`) REFERENCES `order_item_aud` (`id`, `REV`);

--
-- Ограничения внешнего ключа таблицы `order_brochure`
--
ALTER TABLE `order_brochure`
  ADD CONSTRAINT `FK_6unlie0wfcp6tke151c6x8a24` FOREIGN KEY (`blockMaterial`) REFERENCES `material` (`id`),
  ADD CONSTRAINT `FK_7253tn9h3ci8neovixcef0p0l` FOREIGN KEY (`coverBackPrint`) REFERENCES `order_print_work` (`id`),
  ADD CONSTRAINT `FK_b7b4tloc1cs16huwiiux0a4kr` FOREIGN KEY (`coverMaterial`) REFERENCES `material` (`id`),
  ADD CONSTRAINT `FK_f2fy60t95sj6s4kktmfk68jih` FOREIGN KEY (`coverBackLamination`) REFERENCES `lamination` (`id`),
  ADD CONSTRAINT `FK_jqju0r05ljdum564oh8fn1qfn` FOREIGN KEY (`coverLamination`) REFERENCES `lamination` (`id`),
  ADD CONSTRAINT `FK_mcac35w08fr2sroijhl0eadv1` FOREIGN KEY (`coverBackFacePrint`) REFERENCES `order_print_work` (`id`),
  ADD CONSTRAINT `FK_moodbto8imvby78fpcs4n3jj8` FOREIGN KEY (`id`) REFERENCES `order_item` (`id`),
  ADD CONSTRAINT `FK_sckj81qm00ul4l9p23vscrtib` FOREIGN KEY (`coverBackMaterial`) REFERENCES `material` (`id`),
  ADD CONSTRAINT `FK_su8krcdg77ayh74g1v2ik6djg` FOREIGN KEY (`coverBackBackPrint`) REFERENCES `order_print_work` (`id`),
  ADD CONSTRAINT `FK_tq3kxmdyw52agg3stys4g87m0` FOREIGN KEY (`coverFacePrint`) REFERENCES `order_print_work` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_brochure_aud`
--
ALTER TABLE `order_brochure_aud`
  ADD CONSTRAINT `FK_ek2gk3e7gtqxs1h6mopenui33` FOREIGN KEY (`id`, `REV`) REFERENCES `order_item_aud` (`id`, `REV`);

--
-- Ограничения внешнего ключа таблицы `order_calendar`
--
ALTER TABLE `order_calendar`
  ADD CONSTRAINT `FK_4p1nuxp2g7aaov3sij0uqv69m` FOREIGN KEY (`coverFacePrint`) REFERENCES `order_print_work` (`id`),
  ADD CONSTRAINT `FK_9b8xh8j989vxygag4nawil5r5` FOREIGN KEY (`coverBackBackPrint`) REFERENCES `order_print_work` (`id`),
  ADD CONSTRAINT `FK_fcqtqwc2csghlf756wffrcqnx` FOREIGN KEY (`blockMaterial`) REFERENCES `material` (`id`),
  ADD CONSTRAINT `FK_fjgc4h9wh9w629mg3mm0ts6r9` FOREIGN KEY (`coverBackLamination`) REFERENCES `lamination` (`id`),
  ADD CONSTRAINT `FK_mdrm2tecapmkr7dr8c9uwlhh6` FOREIGN KEY (`coverBackPrint`) REFERENCES `order_print_work` (`id`),
  ADD CONSTRAINT `FK_njkj1rxexp115t5mt2viy42v0` FOREIGN KEY (`coverBackMaterial`) REFERENCES `material` (`id`),
  ADD CONSTRAINT `FK_pc7f8gi6klh5fycj3rc7a8x1f` FOREIGN KEY (`coverMaterial`) REFERENCES `material` (`id`),
  ADD CONSTRAINT `FK_pkimnpgan87rfnriheky1ixhj` FOREIGN KEY (`coverLamination`) REFERENCES `lamination` (`id`),
  ADD CONSTRAINT `FK_px08kqkr496gk9pgrk7jk8ndt` FOREIGN KEY (`coverBackFacePrint`) REFERENCES `order_print_work` (`id`),
  ADD CONSTRAINT `FK_s3e5iwxfkwd9xv1sbj0igqvw7` FOREIGN KEY (`id`) REFERENCES `order_item` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_calendar_aud`
--
ALTER TABLE `order_calendar_aud`
  ADD CONSTRAINT `FK_f63a3077wqiv32o66u08fy6so` FOREIGN KEY (`id`, `REV`) REFERENCES `order_item_aud` (`id`, `REV`);

--
-- Ограничения внешнего ключа таблицы `order_format`
--
ALTER TABLE `order_format`
  ADD CONSTRAINT `FK_sshf64otngtfbin1fkahjcmvg` FOREIGN KEY (`service`) REFERENCES `service` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_item`
--
ALTER TABLE `order_item`
  ADD CONSTRAINT `FK_3otbcf36co1l7evsd31rdy23v` FOREIGN KEY (`serviceId`) REFERENCES `service` (`id`),
  ADD CONSTRAINT `FK_knuoabgwovh7grap9bvoflfgc` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `FK_ph7ljaxde0kitm63h8q9orw8r` FOREIGN KEY (`status`) REFERENCES `order_item_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_item_aud`
--
ALTER TABLE `order_item_aud`
  ADD CONSTRAINT `FK_2gp3lyv19e4cech3qcjv3myuf` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`),
  ADD CONSTRAINT `FK_7vi371241geas0l5gg9r8u50o` FOREIGN KEY (`REVEND`) REFERENCES `revinfo` (`REV`);

--
-- Ограничения внешнего ключа таблицы `order_item_status_link`
--
ALTER TABLE `order_item_status_link`
  ADD CONSTRAINT `FK_6gacphmy4uf6hcgxja3vd22n6` FOREIGN KEY (`nextOrderItemStatusId`) REFERENCES `order_item_status` (`id`),
  ADD CONSTRAINT `FK_aye3ynrw2mmo31qqyl361stx1` FOREIGN KEY (`orderItemStatusId`) REFERENCES `order_item_status` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_item_status_link_security`
--
ALTER TABLE `order_item_status_link_security`
  ADD CONSTRAINT `FK_jty86tsq6c5uhfv4fan9umx48` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `FK_k2l8pbnpy7shyh7r0ej7odix` FOREIGN KEY (`orderItemStatusLinkId`) REFERENCES `order_item_status_link` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_lamination_work`
--
ALTER TABLE `order_lamination_work`
  ADD CONSTRAINT `FK_ai0upv5a0cl5pu7htjcrir570` FOREIGN KEY (`lamination`) REFERENCES `lamination` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_lamination_work_aud`
--
ALTER TABLE `order_lamination_work_aud`
  ADD CONSTRAINT `FK_bblq40c4mhg5af32e7folmpvg` FOREIGN KEY (`REVEND`) REFERENCES `revinfo` (`REV`),
  ADD CONSTRAINT `FK_ngt63eqv0o87qrnau7y8waj4c` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`);

--
-- Ограничения внешнего ключа таблицы `order_leaflet`
--
ALTER TABLE `order_leaflet`
  ADD CONSTRAINT `FK_13ffu6lxagj2peipjb4agbxho` FOREIGN KEY (`backPrint`) REFERENCES `order_print_work` (`id`),
  ADD CONSTRAINT `FK_j3g561nfp2298fqjj3ehl1k9u` FOREIGN KEY (`facePrint`) REFERENCES `order_print_work` (`id`),
  ADD CONSTRAINT `FK_q8tho2a4mn79co4kdn17gqjhh` FOREIGN KEY (`material`) REFERENCES `material` (`id`),
  ADD CONSTRAINT `FK_sbt806dmclbm4yqb0d9vvuwib` FOREIGN KEY (`id`) REFERENCES `order_item` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_leaflet_aud`
--
ALTER TABLE `order_leaflet_aud`
  ADD CONSTRAINT `FK_5my251434m6mjuo4pkpnxdvuw` FOREIGN KEY (`id`, `REV`) REFERENCES `order_item_aud` (`id`, `REV`);

--
-- Ограничения внешнего ключа таблицы `order_material`
--
ALTER TABLE `order_material`
  ADD CONSTRAINT `FK_o9c0sejobk03k7fl0uc9ftvdr` FOREIGN KEY (`material`) REFERENCES `material` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_notebook`
--
ALTER TABLE `order_notebook`
  ADD CONSTRAINT `FK_4fc3abpwiylniv48xrdewxndu` FOREIGN KEY (`coverLamination`) REFERENCES `lamination` (`id`),
  ADD CONSTRAINT `FK_dqfle07xokek0b8l0yb34pvay` FOREIGN KEY (`coverBackLamination`) REFERENCES `lamination` (`id`),
  ADD CONSTRAINT `FK_fs43vgch59tg4a9n0bwimwljo` FOREIGN KEY (`coverBackMaterial`) REFERENCES `material` (`id`),
  ADD CONSTRAINT `FK_he37omgcidxtkpkkno3b5xnxq` FOREIGN KEY (`coverMaterial`) REFERENCES `material` (`id`),
  ADD CONSTRAINT `FK_mxvf874pkjq90u7egfq0cdqcu` FOREIGN KEY (`blockMaterial`) REFERENCES `material` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_notebook_aud`
--
ALTER TABLE `order_notebook_aud`
  ADD CONSTRAINT `FK_bgl3hom300shq9an5cqrb3qm7` FOREIGN KEY (`id`, `REV`) REFERENCES `order_item_aud` (`id`, `REV`);

--
-- Ограничения внешнего ключа таблицы `order_print_work_aud`
--
ALTER TABLE `order_print_work_aud`
  ADD CONSTRAINT `FK_avcsc1gynhe5xu3ihtkh8479e` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`),
  ADD CONSTRAINT `FK_jtpomnii8y2ajqd3dbbcj7t6a` FOREIGN KEY (`REVEND`) REFERENCES `revinfo` (`REV`);

--
-- Ограничения внешнего ключа таблицы `order_sticker`
--
ALTER TABLE `order_sticker`
  ADD CONSTRAINT `FK_ew1re2lyaou4drgkc7054a8uf` FOREIGN KEY (`id`) REFERENCES `order_item` (`id`),
  ADD CONSTRAINT `FK_h83c6p5g5ivxp5ekaf8txgu34` FOREIGN KEY (`material`) REFERENCES `material` (`id`),
  ADD CONSTRAINT `FK_pd1wxrupkfm9dwre3yqxc970x` FOREIGN KEY (`print`) REFERENCES `order_print_work` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_sticker_aud`
--
ALTER TABLE `order_sticker_aud`
  ADD CONSTRAINT `FK_nc53kv8sris53jvwbiru5ucls` FOREIGN KEY (`id`, `REV`) REFERENCES `order_item_aud` (`id`, `REV`);

--
-- Ограничения внешнего ключа таблицы `order_visiting_card`
--
ALTER TABLE `order_visiting_card`
  ADD CONSTRAINT `FK_1ys0dtc75ymbg0kibs22jysjs` FOREIGN KEY (`facePrint`) REFERENCES `order_print_work` (`id`),
  ADD CONSTRAINT `FK_as1guqpfa5n4g9v0iw1m7ewqq` FOREIGN KEY (`id`) REFERENCES `order_item` (`id`),
  ADD CONSTRAINT `FK_flf6y6kd1haa8aw29qk5m2rpt` FOREIGN KEY (`lamination`) REFERENCES `order_lamination_work` (`id`),
  ADD CONSTRAINT `FK_nvg1v8hbodhydsuyu6au7ftm5` FOREIGN KEY (`backPrint`) REFERENCES `order_print_work` (`id`),
  ADD CONSTRAINT `FK_nw9o2mbatdtpi3cgoae247t3f` FOREIGN KEY (`material`) REFERENCES `material` (`id`);

--
-- Ограничения внешнего ключа таблицы `order_visiting_card_aud`
--
ALTER TABLE `order_visiting_card_aud`
  ADD CONSTRAINT `FK_jhmwwvjhe44ac8ifwa753ttbm` FOREIGN KEY (`id`, `REV`) REFERENCES `order_item_aud` (`id`, `REV`);

--
-- Ограничения внешнего ключа таблицы `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `FK_oj50qpdthexxxmvur61ggy2fb` FOREIGN KEY (`userId`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
